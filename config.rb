# Require any additional compass plugins here.

asset_cache_buster :none

# Set this to the root of your project when deployed:
project_path = "html/"

css_dir = "assets/css"
css_path = "html/assets/css"

images_dir = "assets/img"
images_path = "html/assets/img"

sass_dir = "src/scss"
sass_path = "src/scss"

javascripts_dir = "assets/js"
javascripts_path = "html/assets/js"

# sprite_load_path = "src/sprite"


http_generated_images_path = "../img"

# You can select your preferred output style here (can be overridden via the command line):
# output_style = :expanded or :nested or :compact or :compressed
output_style = :compressed

# To enable relative paths to assets via compass helper functions. Uncomment:
# relative_assets = true

# To disable debugging comments that display the original location of your selectors. Uncomment:
line_comments = false


# If you prefer the indented syntax, you might want to regenerate this
# project again passing --syntax sass, or you can uncomment this:
# preferred_syntax = :sass
# and then run:
# sass-convert -R --from scss --to sass sass scss && rm -rf sass && mv scss sass
#  Make a copy of sprites with a name that has no uniqueness of the hash.

#  on_sprite_saved do |filename|
#    if File.exists?(filename)
#      FileUtils.cp filename, filename.gsub(%r{-s[a-z0-9]{10}\.png$}, '.png')
#    end
#  end

# Replace in stylesheets generated references to sprites
# by their counterparts without the hash uniqueness.
#  on_stylesheet_saved do |filename|
#    if File.exists?(filename)
#      css = File.read filename
#      File.open(filename, 'w+') do |f|
#        f << css.gsub(%r{-s[a-z0-9]{10}\.png}, '.png')
#      end
#    end
#  end
#


require 'autoprefixer-rails'

on_stylesheet_saved do |file|
  css = File.read(file)
  map = file + '.map'

  if File.exists? map
    result = AutoprefixerRails.process(css,
      browsers: ["last 1 version", "< 1%", "ie 8"],
      from: file,
      to:   file,
      map:  { prev: File.read(map), inline: false })
    File.open(file, 'w') { |io| io << result.css }
    File.open(map,  'w') { |io| io << result.map }
  else
    File.open(file, 'w') { |io| io << AutoprefixerRails.process(css) }
  end
end
