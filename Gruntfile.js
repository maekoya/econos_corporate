module.exports = function(grunt) {
  'use strict';

  grunt.initConfig({
    // Metadata.
    pkg: grunt.file.readJSON('package.json'),

    // Compile Compass to CSS.
    compass: {
      dist: {
        options: {
          config: 'config.rb'
        }
      }
    },
    concat: {
      js: {
        src: [
           'src/js/script.js'
        ],
        dest: 'html/assets/js/script.js'
      }
    },
    watch: {
      dist: {
        files: ['src/**/*.scss','htdocs/*.html'],
        tasks: ['compass']
      },
      concatjs: {
        files: 'src/js/*.js',
        tasks: ['concat:js']
      }
    }
  });

  // Load the plugins.
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-compass');
  grunt.loadNpmTasks('grunt-contrib-watch');

  // Default task.
  grunt.registerTask('default', ['']);

  // Indivisual Tasks.
  grunt.registerTask('develop', ['watch']);
};
