'use strict';

;(function (window, $){
  var document = window.document;
  $(document).ready(function(){

    /* DOM ready */
        currentGlobalNav();
    smoothScrollForAncorTag();

  });
}(this, jQuery));


/* --------------------------------------------------------
 SMOOTH SCROLL
-------------------------------------------------------- */
function smoothScrollForAncorTag(){
  var eventListener = 'click';
  var scrollAncors = $('a[href^="#"], a[href^="' + location.pathname + '#"]');

  scrollAncors.each (function(){
    var hash = this.hash;
    $(this).on(eventListener, function(){
      smoothScroll(hash);
      return false;
    });
  });
}

function smoothScroll(hash) {
  var target = $(hash).offset().top;

  $('body, html').stop().animate({scrollTop: target >= 50 ? target - 50  : target}, 600, 'swing', function(){
    $(this).unbind("mousewheel DOMMouseScroll");
  }).bind("mousewheel DOMMouseScroll",function(){
    $(this).queue([]).stop();
    $(this).unbind("mousewheel DOMMouseScroll");
  });

}

/* --------------------------------------------------------
 CURRENT GLOBAL NAVIGATION added ACLTIVE CLASS
-------------------------------------------------------- */
function currentGlobalNav() {
    var dir = (location.protocol + "//" + location.host + location.pathname).split("/");
    dir = dir[3];

    var $target = $(".mod-header-nav");
    var $target = dir === "" ? $(".mod-header-logo") : $target.find(".nav-" + dir);

    $target.addClass("active");
}
