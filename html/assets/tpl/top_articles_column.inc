<?php
$is_wordpress = true; // if local, set false

if ($is_wordpress) :
    require_once( $_SERVER['DOCUMENT_ROOT'] .'/cms/wp-load.php');
    print_articles();
endif;

function print_articles(){

    $html = "";
    $set_post_type = array(
        'post_type' => 'topics',
        'showposts' => 3
        );
    query_posts($set_post_type);

    if (have_posts()) : while(have_posts()) : the_post();
        $html .= '<li><a href="' . get_the_permalink() . '"><date class="top-sec-topics-date">' . get_the_time('Y.m.d') . '</date><span class="top-sec-topics-title">' . get_the_title() . '</span></a></li>' . "\n";
    endwhile; else:
        $html = '現在、表示可能な記事はありません。';
    endif;
    wp_reset_postdata();
    wp_reset_query();
    echo $html;
}





