
<footer class="mod-footer">
    <div class="mod-footer-in">
        <p class="mod-footer-txt-1"><a href="/">株式会社エコノス</a></p>
        <p class="mod-footer-pagetop"><a href="#top" class="cssRoll"><img src="/assets/img/base/footer-pagetop-1.png" width="57" height="30" alt="このページのトップへ"></a></p>
        <div class="mod-footer-nav">
            <ul class="w25per">
                <li><a href="/">TOP</a></li>
                <li><a href="/contact/">お問い合わせ</a></li>
                <li><a href="/topics/">トピックス</a></li>
            </ul>
            <ul class="w25per">
                <li><a href="/company/">会社紹介</a>
                	<br>
                	<ul class="mod-footer-nav-list-low">
	                    <li><a href="/company/#page-message">ご挨拶</a></li>
	                    <li><a href="/company/outline/">会社概要・沿革</a></li>
                    </ul>
                </li>
                <li><a href="/ir/">IR情報</a></li>
                <li><a href="/recruit/">採用情報</a></li>
            </ul>
            <ul class="w50per">
                <li><a href="/reuse/">事業内容</a>
                	<br>
                	<ul class="mod-footer-nav-list-low w50per">
	                    <li class="mod-footer-strong">＜リユース事業＞</li>
	                    <li><a href="/reuse/">リユースショップ</a></li>
	                    <li><a href="/reuse/#page-ec">EC販売</a></li>
	                    <li><a href="/reuse/shop/">運営店舗一覧</a></li>
                    </ul>
                	<ul class="mod-footer-nav-list-low w50per">
	                    <li class="mod-footer-strong">＜低炭素事業＞</li>
	                    <li><a href="/carbonoffset/">カーボンオフセット</a></li>
	                    <li><a href="/eco/">エコプロダクツ</a></li>
	                    <li>　　<a href="/eco/products.html">取り扱い商品</a></li>
	                    <li>　　<a href="/eco/#page-point">ポイント交換商品</a></li>
	                    <li>　　<a href="/eco/send.html">交換方法・申請書類のご案内</a></li>
                    </ul>
                </li>
            </ul>
        </div><!-- /.mod-footer-nav -->
    </div><!-- /.mod-footer-in -->
    <div class="mod-footer-privacy">
    	<p>Copyright © ECONOS Co.,Ltd. All Rights Reserved.</p>
    </div>
</footer>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-63174178-1', 'auto');
  ga('send', 'pageview');

</script>
