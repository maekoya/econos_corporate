
<header class="mod-header">
    <p class="mod-header-logo"><a href="/"><img src="/assets/img/base/header-logo-1.png" width="126" height="60" alt="株式会社エコノス"></a></p>
    <nav class="mod-header-nav">
        <ul>
            <li class="nav-topics"><a href="/topics/">トピックス</a></li>
            <li class="nav-company"><a href="/company/">会社紹介</a></li>
            <li class="nav-carbonoffset nav-eco nav-reuse nav-business"><a href="/reuse/">事業内容</a></li>
            <li class="nav-ir"><a href="/ir/">IR情報</a></li>
            <li class="nav-recruit"><a href="/recruit/">採用情報</a></li>
        </ul>
    </nav>
</header>
