<?php
/**
 * ECONOS TOPICS
 */
?>
<!DOCTYPE html>
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
<meta charset="UTF-8">
<title><?php if (is_single()){ the_title('', false, 'right'); echo '｜';} ?>トピックス｜北海道からエコで新たな価値を｜エコノス</title>
<?php
print_description();
//no index
if (is_404() || is_archive()):
    $output = '<meta name="robots" content="noindex">'."\n";
    echo $output;
endif;
?>
<meta name="viewport" content="width=device-width,user-scalable=1">
<meta name="format-detection" content="telephone=yes,address=no,email=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="copyright" content="Copytight © ECONOS Co.,Ltd. All Rights Reserved.">
<meta property="og:site_name" content="株式会社エコノス">
<meta property="og:image" content="">
<meta property="og:locale" content="ja_JP">
<link rel="alternate" href="http://eco-nos.com/topics/feed/" type="application/rss+xml" title="RSS 2.0">
<link rel="alternate" href="http://eco-nos.com/topics/feed/atom/" type="application/atom+xml" title="Atom 1.0">
<?php include($_SERVER['DOCUMENT_ROOT']."/assets/tpl/mod_head.inc"); ?>
</head>



<body id="top" class="">
<?php include($_SERVER['DOCUMENT_ROOT']."/assets/tpl/mod_header.inc"); ?>

<div class="mod-pankuzu">
    <ul>
        <?php print_pankuzu(); ?>
    </ul>
</div><!-- /.mod-pankuzu -->

<div class="mod-tit type-topics">
    <h1><img src="/assets/img/covers/topics-tit-main.png" width="205" height="48" alt="トピックス"></h1>
</div><!-- /.mod-tit -->

<div class="mod-container type-two-col">
    <section class="mod-body type-left">

        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        <article class="mod-article">
            <div class="mod-article-head">
                <date class="mod-article-head-date"><?php the_time('Y.m.d'); ?></date>
                <ul class="mod-article-head-tags">
                <?php the_terms($post->ID, 'topics_cat', '<li>', '</li><li>', '</li>'); ?>
                </ul>
                <h2 class="mod-article-head-tit"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
            </div><!-- /.mod-article-head -->
            <div class="mod-article-body">
            <?php the_content(__('(続きを見る)')); ?>
            </div><!-- /.mod-article-body -->
        </article>
        <?php endwhile; ?>

        <div class="mod-pager">
        <?php
        //ページャー
        if (is_single()){
            $prev_post = get_previous_post();
            if (!empty( $prev_post)):
                echo '<p class="mod-pager-prev"><a href="'.get_permalink( $prev_post->ID ).'" class="mod-btn type-ico-arrow-l cssRoll">前の記事へ</a></p>';
            endif;

            $next_post = get_next_post();
            if (!empty( $next_post)):
            echo '<p class="mod-pager-next"><a href="'.get_permalink( $next_post->ID ).'" class="mod-btn type-ico-arrow-r cssRoll">次の記事へ</a></p>';
            endif;
        }else{
            $prev_page = get_previous_posts_page_link();
            if (get_previous_posts_link()):
            echo '<p class="mod-pager-prev"><a href="'.$prev_page.'" class="mod-btn type-ico-arrow-l cssRoll">前のページへ</a></p>';
            endif;

            $next_page = get_next_posts_page_link();
            if (get_next_posts_link()):
            echo '<p class="mod-pager-next"><a href="'.$next_page.'" class="mod-btn type-ico-arrow-r cssRoll">次のページへ</a></p>';
            endif;
        }
        ?>
        </div>
        <?php else: ?>
        <p><?php _e('申し訳ございません。該当ページは見つかりませんでした。'); ?></p>
        <?php endif; ?>
    </section><!-- /.mod-body -->

    <div class="mod-side">
        <nav class="mod-side-nav">
            <p class="mod-side-nav-heading">最近の記事</p>
            <ul>
                <?php
                    $set_post_type = array(
                        'post_type' => array('topics'),
                        'showposts' => 5
                        );
                    query_posts($set_post_type);
                    if (have_posts()):while(have_posts()):the_post();
                ?>
                <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
                <?php
                    endwhile; endif;
                    wp_reset_postdata();
                    //wp_reset_query();
                ?>
            </ul>
            <p class="mod-side-nav-heading">カテゴリー</p>
            <ul>
                <?php wp_list_categories('title_li=&taxonomy=topics_cat'); ?>
            </ul>
            <p class="mod-side-nav-heading">アーカイブ</p>
            <ul>
                <?php wp_get_archives('type=monthly&post_type=topics&show_post_count=0&format=custom&before=<li>&after=</li>'); ?>
            </ul>
        </nav>
    </div><!-- /.mod-side -->
</div><!-- /.mod-container -->

<?php include($_SERVER['DOCUMENT_ROOT']."/assets/tpl/mod_footer.inc"); ?>
</body>
</html>