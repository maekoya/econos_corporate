<?php
/**
 * @package WordPress
 *
 */


//Description出力
function print_description(){
    $dir_name = explode( '/', $_SERVER[REQUEST_URI]);
    $output = '';
    if ($dir_name[1] == "topics" && !$dir_name[2]):
        $output = 'エコロジー（環境）とエコノミー（経済）を両立させてリユース事業と低炭素事業を通じて北海道から新しい価値を発信する株式会社エコノスのトピックスです。';
    elseif (is_single()):
        if (have_posts()) : while (have_posts()) : the_post();
        $output = get_the_excerpt();
        endwhile; endif;
    elseif (is_404()):
        $output = 'エコロジー（環境）とエコノミー（経済）を両立させてリユース事業と低炭素事業を通じて北海道から新しい価値を発信する株式会社エコノスのトピックスです。';
    else:
        $output = 'エコロジー（環境）とエコノミー（経済）を両立させてリユース事業と低炭素事業を通じて北海道から新しい価値を発信する株式会社エコノスのトピックスです。' . wp_title('', false, '') .'の記事一覧です。';
    endif;
    echo '<meta name="description" content="' . $output . '">'."\n";
}

//パンくず出力
function print_pankuzu(){
    $dir_name = explode( '/', $_SERVER[REQUEST_URI]);
    $output = '';
    if ($dir_name[1] == "topics" && !$dir_name[2]):
        $output = '<li><a href="/">トップページ</a></li><li>トピックス</li>';
    elseif (is_single()):
        if (mb_strlen( get_the_title() ) > 66 ) :
            $output = '<li><a href="/">トップページ</a></li><li><a href="/topics/">トピックス</a></li><li>' . mb_substr( get_the_title(), 0, 66 ) . '...</li>';
        else :
            $output = '<li><a href="/">トップページ</a></li><li><a href="/topics/">トピックス</a></li><li>' . get_the_title() . '</li>';
        endif;
    elseif (is_404()):
        $output = '<li><a href="/">トップページ</a></li><li><a href="/topics/">トピックス</a></li><li>' . wp_title('', false, '') . '</li>';
    else:
        $output = '<li><a href="/">トップページ</a></li><li><a href="/topics/">トピックス</a></li><li>' . wp_title('', false, '') . 'の記事</li>';
    endif;
    echo $output;
}

//"wp_list_categories"関数の表示カスタマイズ
function span_before_link_list_categories( $list ) {
    $list = str_replace('<li><a href=','<dd><a href=',$list);
    $list = str_replace('</a></li>','</a></dd>',$list);
    return $list;
}
add_filter ( 'wp_list_categories', 'span_before_link_list_categories' );


//"wp_title"関数の日本語日付表記カスタマイズ
function jp_date_wp_title( $title, $sep, $seplocation ) {
    if ( is_date() ) {
        $m = get_query_var('m');
        if ( $m ) {
            $year = substr($m, 0, 4);
            $month = intval(substr($m, 4, 2));
            $day = intval(substr($m, 6, 2));
        } else {
            $year = get_query_var('year');
            $month = get_query_var('monthnum');
            $day = get_query_var('day');
        }
        $title = ($seplocation != 'right' ? " $sep " : '') . ($year ? $year . '年' : '') . ($month ? $month . '月' : '') . ($day ? $day . '日' : '') . ($seplocation == 'right' ? " $sep " : '');
    }
    return $title;
}
add_filter( 'wp_title', 'jp_date_wp_title', 10, 3 );


?>