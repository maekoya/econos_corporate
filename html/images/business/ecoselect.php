<?php include("../../manager/include/dbconn.php") ?>
<?php include("../../manager/include/func.php") ?>
<?php include("../../manager/include/settings.php") ?>

<?php
// ニュース・インフォメーション情報取得
$row_ecopoints = Exec_Sql_ToA($sql_ecopoints);
?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<meta name="keywords" content="株式会社エコノス,カーボン・オフセット,カーボンオフセット,排出権,J-VER,国内クレジット,myclimate,あんしんプロバイダー,オフセットプロバイダー">
<meta name="description" content="株式会社エコノスのカーボンオフセット、カーボン・マネジメント、クレジット創出支援、リユース、ブックオフ、ハードオフ、オフハウス、ホビーオフ、ガレージオフ、e-コマース、e-コミュニケーションの各事業内容を紹介">
<meta name="viewport" content="width=device-width">
<title>エコノスセレクト | 環境商品提供事業 | 事業内容 | 株式会社エコノス</title>
<link type="text/css" rel="stylesheet" href="../../css/common/index.css">
<link type="text/css" rel="stylesheet" href="../../css/business.css">
<script src="../../js/jquery.js" type="text/javascript"></script>
<script src="../../js/jquery.innerfade.js" type="text/javascript"></script>
<script src="../../js/jquery-ui.custom.js" type="text/javascript"></script>
<script src="../../js/jquery.flatheights.js" type="text/javascript"></script>
<script src="../../js/script.js" type="text/javascript"></script>
<script src="../../js/google-analytics.js" type="text/javascript"></script>
<!--[if lt IE 9]>
<script src="../../js/html5.js"></script>
<![endif]-->
</head>
<body>
<!-- header -->
<header>
	<div id="header">
		<h1><a href="/"><img src="../../images/common/header_logo.gif" alt="株式会社エコノス" width="117" height="79"></a></h1>
		<div id="header_navi_area">
			<nav id="header_navi">
				<ul>
					<li><a href="../../sitemap/index.html"><img src="../../images/common/header_navi01.gif" alt="サイトマップ" width="89" height="12" class="hoverImg"></a></li>
					<li><a href="../../link/index.html"><img src="../../images/common/header_navi02.gif" alt="リンク" width="50" height="12" class="hoverImg"></a></li>
				</ul>
			</nav>
			<nav id="globalnavi">
				<ul>
					<li><a href="/"><img src="../../images/common/navi01_n.gif" alt="ホーム" width="121" height="50"></a></li>
					<li><a href="../../business/index.html"><img src="../../images/common/navi02_o.gif" alt="事業内容" width="121" height="50"></a></li>
					<li><a href="../../company/index.html"><img src="../../images/common/navi03_n.gif" alt="会社情報" width="121" height="50"></a></li>
					<li><a href="../../ir/index.php"><img src="../../images/common/navi04_n.gif" alt="IR情報" width="121" height="50"></a></li>
					<li><a href="../../recruit/index.php"><img src="../../images/common/navi05_n.gif" alt="採用情報" width="121" height="50"></a></li>
					<li><a href="../../contact/index.php"><img src="../../images/common/navi06_n.gif" alt="お問い合わせ" width="121" height="50"></a></li>
				</ul>
			</nav>
		</div>
	</div>
</header>
<!-- /#header -->
<!-- top_title-->
<div id="top_title">
	<h2><img src="../../images/business/top_title02.gif" alt="エコノスセレクト　エコノスが選んだ環境配慮型商品" width="600" height="32"></h2>
</div>
<!-- /#top_title -->
<!-- topic_path -->
<div class="topic_path">
	<ul>
		<li><a href="/">ホーム</a></li>
		<li><a href="../index.html">事業内容</a></li>
		<li><a href="index.html">環境商品提供事業</a></li>
		<li>エコノスセレクト</li>
	</ul>
</div>
<!-- /.topic_path -->
<!-- contents -->
<div id="contents">
<!-- column_1 -->
	<div class="column_1">
		<div class="inner">
<!-- section h3 -->
			<section class="section01">
				<h3 class="eco_title"><img src="../../images/business/eco_main_title.gif" alt="エコノスセレクト 復興支援・住宅エコポイント交換商品のご案内。エコ住宅の新築や、エコリフォームにより取得したエコポイントを、エコノス取扱いの環境配慮型商品と交換いただけます。下記リストよりお好みの商品をお選びください。事業者コード V004" width="776" height="98"></h3>
				<div class="eco_sinsei">
					<dl class="sinsei01">
						<dt><img src="../../images/business/eco_sinsei_title01.gif" alt="WEBからの申請 " width="134" height="18"></dt>
						<dd>事務局のホームページ内の<a href="http://jutaku-eco-points.force.com/fukko">マイページ</a>に、ポイント通知ハガキに記載されている個人IDとパスコードを入力してログインください。</dd>
					</dl>
					<dl class="sinsei02">
						<dt><img src="../../images/business/eco_sinsei_title02.gif" alt="書類提出による申請 " width="171" height="18"></dt>
						<dd>申請書類を<a href="http://fukko-jutaku.eco-points.jp/download/">こちらよりダウンロード</a>していただき、弊社事業者コード <img src="../../images/business/eco_sinsei_code.gif" alt="V004" width="39" height="12"> と下記リストに記載の商品コードをメモ帳などに控えて頂きまして<a href="http://fukko-jutaku.eco-points.jp">エコポイント事務局</a>にご申請ください。<br>
							申請書類の記入方法については<a href="http://fukko-jutaku.eco-points.jp/common/file/user/apply_point_how01.pdf">こちら</a>をご参照ください。</dd>
					</dl>
				</div>
				<!-- eco_btn_area -->
				<div class="eco_btn_area">
					<ul class="eco_btn01">
						<li><a href="http://fukko-jutaku.eco-points.jp/mypage/"><img src="../../images/business/eco_btn01.jpg" alt="WEBからの申請方法" width="182" height="79" class="hoverImg"></a></li>
						<li><a href="http://fukko-jutaku.eco-points.jp/user/send/"><img src="../../images/business/eco_btn02.jpg" alt="書類提出による申請方法" width="213" height="79" class="hoverImg"></a></li>
						<li><a href="http://fukko-jutaku.eco-points.jp/download/"><img src="../../images/business/eco_btn03.jpg" alt="書類提出用申請書ダウンロード" width="232" height="79" class="hoverImg"></a></li>
					</ul>
					<div class="eco_btn02"><a href="http://fukko-jutaku.eco-points.jp"><img src="../../images/business/eco_btn04.jpg" alt="エコポイント事務局" width="121" height="76" class="hoverImg"></a></div>
				</div>
				<!-- /.eco_btn_area -->
				<!-- eco_product_area -->
				<section class="eco_product_area">
					<h4 class="eco_cattitle">カテゴリーから探す</h4>
					<div id="ecoselect_category">
						<ul>
							<li><a href="ecoselect19.php#ecopointItemTop"><img src="../../images/business/cat01.png" alt="エコポイントで掃除機と交換" class="hoverImg"></a></li>
							<li><a href="ecoselect01.php#ecopointItemTop"><img src="../../images/business/cat05.png" alt="エコポイントでロボット掃除機と交換" class="hoverImg"></a></li>
							<li><a href="ecoselect02.php#ecopointItemTop"><img src="../../images/business/cat02.png" alt="エコポイントで高圧洗浄機と交換" class="hoverImg"></a></li>
							<li><a href="ecoselect03.php#ecopointItemTop"><img src="../../images/business/cat03.png" alt="エコポイントでガジェットと交換" class="hoverImg"></a></li>
							<li><a href="ecoselect04.php#ecopointItemTop"><img src="../../images/business/cat04.png" alt="エコポイントでデジタルカメラと交換" class="hoverImg"></a></li>
							<li><a href="ecoselect05.php#ecopointItemTop"><img src="../../images/business/cat15.png" alt="エコポイントでプリンター・スキャナ・インクと交換" class="hoverImg"></a></li>
							<li><a href="ecoselect06.php#ecopointItemTop"><img src="../../images/business/cat06.png" alt="エコポイントで空気清浄機と交換" class="hoverImg"></a></li>
							<li><a href="ecoselect07.php#ecopointItemTop"><img src="../../images/business/cat10.png" alt="エコポイントでホームベーカリーと交換" class="hoverImg"></a></li>
							<li><a href="ecoselect08.php#ecopointItemTop"><img src="../../images/business/cat13.png" alt="エコポイントで電気ケトル・ポットと交換" class="hoverImg"></a></li>
							<li><a href="ecoselect09.php#ecopointItemTop"><img src="../../images/business/cat16.png" alt="エコポイントでコーヒーメーカーと交換" class="hoverImg"></a></li>
							<li><a href="ecoselect10.php#ecopointItemTop"><img src="../../images/business/cat17.png" alt="エコポイントで調理器具と交換" class="hoverImg"></a></li>
							<li><a href="ecoselect11.php#ecopointItemTop"><img src="../../images/business/cat18.png" alt="エコポイントで生活用品と交換" class="hoverImg"></a></li>
							<li><a href="ecoselect12.php#ecopointItemTop"><img src="../../images/business/cat12.png" alt="エコポイントで音響機器と交換" class="hoverImg"></a></li>
							<li><a href="ecoselect13.php#ecopointItemTop"><img src="../../images/business/cat20.png" alt="エコポイントでゲーム機器と交換" class="hoverImg"></a></li>
							<li><a href="ecoselect14.php#ecopointItemTop"><img src="../../images/business/cat22.png" alt="エコポイントでヒーターと交換" class="hoverImg"></a></li>
							<li><a href="ecoselect16.php#ecopointItemTop"><img src="../../images/business/cat07.png" alt="エコポイントで扇風機・サーキュレーターと交換" class="hoverImg"></a></li>
							<li><a href="ecoselect17.php#ecopointItemTop"><img src="../../images/business/cat08.png" alt="エコポイントで美容家電・電動歯ブラシ・シェーバーと交換" class="hoverImg"></a></li>
					</ul>
					</div>
					<h4 id="ecopointItemTop"><img src="../../images/business/eco_title01.gif" alt="商品一覧" width="778" height="42"></h4>
					<!-- eco_product -->
<?php foreach ($row_ecopoints as $key=>$val) { ?>
<?php $link = getImageLink($val); ?>
					<div class="eco_product">
						<div class="product_img">
							<?php if (!empty($link)) { echo '<img src="'.$link.'" alt="" width="200" height="150">'; } ?>
						</div>
						<div class="product_txt">
							<div class="code">
							<table class="goodscd">
									<tr>
										<td class="td-title" nowrap>
											商品コード
										</td>
										<td class="td-code" nowrap>
											<?php echo $val['goodscd']; ?>
										</td>
									</tr>
								</table>
							</div>
							<h5><?php echo $val['goodsname'] ?></h5>
							<dl class="point">
								<dt>交換必要ポイント：</dt>
								<dd><?php echo number_format($val['point']); ?>pt</dd>
							</dl>
							<p>
								<?php echo hbr($val['explaination']); ?>
							</p>
						</div>
					</div>
<?php } ?>
					<!-- /.eco_product -->
					<!-- eco_btn_area -->
					<div class="eco_btn_area">
						<ul class="eco_btn01">
							<li><a href="http://fukko-jutaku.eco-points.jp/mypage/"><img src="../../images/business/eco_btn01.jpg" alt="WEBからの申請方法" width="182" height="79" class="hoverImg"></a></li>
							<li><a href="http://fukko-jutaku.eco-points.jp/user/send/"><img src="../../images/business/eco_btn02.jpg" alt="書類提出による申請方法" width="213" height="79" class="hoverImg"></a></li>
							<li><a href="http://fukko-jutaku.eco-points.jp/download/"><img src="../../images/business/eco_btn03.jpg" alt="書類提出用申請書ダウンロード" width="232" height="79" class="hoverImg"></a></li>
						</ul>
						<div class="eco_btn02"><a href="http://fukko-jutaku.eco-points.jp"><img src="../../images/business/eco_btn04.jpg" alt="エコポイント事務局" width="121" height="76" class="hoverImg"></a></div>
					</div>
					<!-- /.eco_btn_area -->
					<h4 class="eco_cattitle">カテゴリーから探す</h4>
					<div id="ecoselect_category">
						<ul>
							<li><a href="ecoselect19.php#ecopointItemTop"><img src="../../images/business/cat01.png" alt="エコポイントで掃除機と交換" class="hoverImg"></a></li>
							<li><a href="ecoselect01.php#ecopointItemTop"><img src="../../images/business/cat05.png" alt="エコポイントでロボット掃除機と交換" class="hoverImg"></a></li>
							<li><a href="ecoselect02.php#ecopointItemTop"><img src="../../images/business/cat02.png" alt="エコポイントで高圧洗浄機と交換" class="hoverImg"></a></li>
							<li><a href="ecoselect03.php#ecopointItemTop"><img src="../../images/business/cat03.png" alt="エコポイントでガジェットと交換" class="hoverImg"></a></li>
							<li><a href="ecoselect04.php#ecopointItemTop"><img src="../../images/business/cat04.png" alt="エコポイントでデジタルカメラと交換" class="hoverImg"></a></li>
							<li><a href="ecoselect05.php#ecopointItemTop"><img src="../../images/business/cat15.png" alt="エコポイントでプリンター・スキャナ・インクと交換" class="hoverImg"></a></li>
							<li><a href="ecoselect06.php#ecopointItemTop"><img src="../../images/business/cat06.png" alt="エコポイントで空気清浄機と交換" class="hoverImg"></a></li>
							<li><a href="ecoselect07.php#ecopointItemTop"><img src="../../images/business/cat10.png" alt="エコポイントでホームベーカリーと交換" class="hoverImg"></a></li>
							<li><a href="ecoselect08.php#ecopointItemTop"><img src="../../images/business/cat13.png" alt="エコポイントで電気ケトル・ポットと交換" class="hoverImg"></a></li>
							<li><a href="ecoselect09.php#ecopointItemTop"><img src="../../images/business/cat16.png" alt="エコポイントでコーヒーメーカーと交換" class="hoverImg"></a></li>
							<li><a href="ecoselect10.php#ecopointItemTop"><img src="../../images/business/cat17.png" alt="エコポイントで調理器具と交換" class="hoverImg"></a></li>
							<li><a href="ecoselect11.php#ecopointItemTop"><img src="../../images/business/cat18.png" alt="エコポイントで生活用品と交換" class="hoverImg"></a></li>
							<li><a href="ecoselect12.php#ecopointItemTop"><img src="../../images/business/cat12.png" alt="エコポイントで音響機器と交換" class="hoverImg"></a></li>
							<li><a href="ecoselect13.php#ecopointItemTop"><img src="../../images/business/cat20.png" alt="エコポイントでゲーム機器と交換" class="hoverImg"></a></li>
							<li><a href="ecoselect14.php#ecopointItemTop"><img src="../../images/business/cat22.png" alt="エコポイントでヒーターと交換" class="hoverImg"></a></li>
							<li><a href="ecoselect16.php#ecopointItemTop"><img src="../../images/business/cat07.png" alt="エコポイントで扇風機・サーキュレーターと交換" class="hoverImg"></a></li>
							<li><a href="ecoselect17.php#ecopointItemTop"><img src="../../images/business/cat08.png" alt="エコポイントで美容家電・電動歯ブラシ・シェーバーと交換" class="hoverImg"></a></li>
						</ul>
					</div>
				</section>
				<!-- /eco_product_area -->
			
			</section>
<!-- /section h3 -->
		
		</div>
	</div>
<!-- /.column_1 -->

</div>
<!-- /#contents -->
<!-- pagetop -->
<div class="pagetop"><a href="#header"><img src="../../images/common/pagetop_n.gif" alt="ページトップへ" width="106" height="26"></a></div>
<!-- /.pagetop -->
<!-- footer -->
<footer>
	<div id="footer">
		<div id="footer_address">
			<div><img src="../../images/common/footer_logo.gif" alt="株式会社エコノス Relationship witheconomy and ecology" width="230" height="56"></div>
			<p>【札幌本社】 〒003-0834<br>
							北海道札幌市白石区北郷四条13-3-25<br>
							TEL 011-875-1996</p>
		</div>
		<div id="footer_navi">
			<ul class="f_navi">
				<li><a href="/">ホーム</a></li>
				<li><a href="../../business/index.html">事業内容</a>
					<ul>
						<li><a href="../../business/reuse/index.html">リユース事業</a></li>
						<li><a href="../../business/environmental/index.html">環境商品提供事業</a></li>
						<li><a href="../../business/environmental/ecoselect.php">エコノスセレクト</a></li>
					</ul>
				</li>
			</ul>
			<ul class="f_navi">
				<li><a href="../../company/index.html">会社概要</a>
					<ul>
						<li><a href="../../company/catchword.html">コーポレートスローガン</a></li>
						<li><a href="../../company/outline.html">企業概要</a></li>
						<li><a href="../../company/exchange.html">社長挨拶</a></li>
						<li><a href="../../company/organigram.html">組織図</a></li>
						<li><a href="../../company/history.html">沿革</a></li>
					</ul>
				</li>
			</ul>
			<ul class="f_navi">
				<li><a href="../../ir/index.php">IR情報</a></li>
				<li><a href="../../recruit/index.php">採用情報</a></li>
				<li><a href="../../link/index.html">リンク</a></li>
				<li><a href="../../sitemap/index.html">サイトマップ</a></li>
				<li><a href="../../policy/index.html">プライバシーポリシー</a></li>
			</ul>
			<ul class="f_navi last">
				<li><a href="../../contact/index.php">お問い合わせ</a></li>
			</ul>
		</div>
	</div>
	<div id="copyright">
		<small>Copyright (c) ECONOS Co., Ltd. All Rights Reserved.</small>
	</div>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-37101611-1']);
  _gaq.push(['_setDomainName', 'eco-nos.com']);
  _gaq.push(['_setAllowLinker', true]);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<a title="Web Analytics" href="http://getclicky.com/66412114"><img alt="Web Analytics" src="//static.getclicky.com/media/links/badge.gif" border="0" /></a>
<script src="//static.getclicky.com/js" type="text/javascript"></script>
<script type="text/javascript">try{ clicky.init(66412114); }catch(e){}</script>
<noscript><p><img alt="Clicky" width="1" height="1" src="//in.getclicky.com/66412114ns.gif" /></p></noscript>
</footer>
<!-- /#footer -->
</body>
</html>