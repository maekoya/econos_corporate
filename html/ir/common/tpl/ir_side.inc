
    <div class="mod-side">
        <nav class="mod-side-nav">
	        <p class="mod-side-nav-heading"><a href="/ir/">IR情報</a></p>
            <ul>
				<li class="ir-news"><a href="/ir/news.html">IRニュース</a></li>
				<li class="ir-message"><a href="/ir/message.html">株主・投資家の皆様へ</a></li>
				<li><a href="/ir/governance.html">経営情報</a>
				<ul class="type-governance">
					<li class="ir-governance"><a href="/ir/governance.html">コーポレート・ガバナンス</a></li>
					<li><a href="/company/">経営理念</a></li>
				</ul>
				</li>
				<li class="ir-highlight"><a href="/ir/highlight.html">財務ハイライト</a></li>
				<li><a href="/ir/library.html">IRライブラリー</a>
				<ul class="type-library">
					<li class="ir-library"><a href="/ir/library.html">決算短信</a></li>
					<li class="ir-securities"><a href="/ir/securities.html">有価証券報告書</a></li>
					<li class="ir-materials"><a href="/ir/materials.html">IR資料</a></li>
				</ul>
				</li>
				<li><a href="/ir/stock.html">株式について</a>
				<ul class="type-stock">
					<li class="ir-stock"><a href="/ir/stock.html">株式情報</a></li>
					<li class="ir-meeting"><a href="/ir/meeting.html">株主総会</a></li>
					<li><a href="http://stocks.finance.yahoo.co.jp/stocks/detail/?code=3136" target="_blank">株価情報</a></li>
				</ul>
				</li>
				<li class="ir-calendar"><a href="/ir/calendar.html">IRカレンダー</a></li>
				<li class="ir-investor"><a href="/ir/investor.html">個人投資家の皆様へ</a></li>
				<li class="ir-faq"><a href="/ir/faq.html">よくあるご質問</a></li>
				<li><a href="&#109;&#97;i&#108;&#116;&#111;&#58;info&#45;&#105;r&#64;&#101;&#99;o-no&#115;.co&#109;">IRお問い合わせ</a></li>
				<li class="ir-notice"><a href="/ir/notice.html">電子公告</a></li>
				<li class="ir-disclaimer"><a href="/ir/disclaimer.html">免責事項</a></li>
            </ul>
        </nav>
    </div><!-- /.mod-side -->
