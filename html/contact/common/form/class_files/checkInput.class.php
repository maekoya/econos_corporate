<?php

class checkInput {
	
	/**
	 * This file is part of Cypher Point Framework
	 *
	 * Copyright(c) 2007-2008 Cypher point Co.,Ltd. All Rights Reserved.
	 *
	 * http://www.cypher-point.co.jp/
	 */

	/*
	 * 
	 * ・入力必須チェック　Blank()
	 * ・メールアドレスチェック　MailAddress()
	 * ・半角英数チェック　Alnum()
	 * ・半角数字チェック　Num()
	 * ・日付チェック　Date( $year, $month, $day )
	 * ・郵便番号チェック　ZipCode() ※引数は、1or2個 ex) "214-0032" or "214 & 0032"
	 * ・電話番号チェック　PhoneNo( $phone_no ) ※引数は、090-1111-2222,03-444-5555等、-(ハイフン)2つで区切られるもの
	 * ・英数文字チェック　×
	 * ・ローマ字チェック　×
	 * ・ひらがなチェッック　×
	 * ・全角カナチェック　×
	 * ・漢字チェック　×
	 * ・URL チェック　URL($url)
	 * ・ファイルアップロードチェック　×
	 * ・半角英数、-(ハイフン)、_(アンダーバー)チェック　Custom1()
	 * ・都道府県チェック　Prefs()
	 * 
	 */
	
	// -------------------------------------------------------------------------------------
	// コンストラクタ
	// -------------------------------------------------------------------------------------
	function checkInput() {
		
	}
	
	// -------------------------------------------------------------------------------------
	// 各種エラーチェック
	// -------------------------------------------------------------------------------------

	// 入力必須チェック（空かどうか）
	function Blank() {
		
		$is_error = false;
		$str = func_get_args();
	
		foreach( $str as $value ) {
			
			// $valueが配列の場合
			if ( is_array($value) ) {
				
			
				for( $i=0; $i<count($value); $i++ ) {
			
					if ( $value[$i] == "" ) {
						$is_error = true;
					}
				
				}
			
			// その他
			} else {
			
				if ( $value == "" ) {
					$is_error = true;
				}
				
			}
			
		}
		
	   	return $is_error;
	
	}
	
	// メールアドレスチェック
	function MailAddress() {
		
		$is_error = false;
		$str = func_get_args();
	
		foreach( $str as $value ) {
			if( !preg_match( '/^[a-zA-Z0-9_\.\-]+?@[A-Za-z0-9_\.\-]+$/',$value ) ) {
			$is_error = true;
			}	
		}
		
	   	return $is_error;
	
	}
	
	// 半角英数チェック
	function Alnum() {
		
		$is_error = false;
		$str = func_get_args();
	
		foreach( $str as $value ) {
			
			// $valueが配列の場合
			if ( is_array($value) ) {
			
				for( $i=0; $i<count($value); $i++ ) {
			
					if( !preg_match("/^[a-zA-Z0-9]+$/", $value) ) {
						$is_error = true;
					}
				
				}
			
			// その他
			} else {
		
				if( !preg_match("/^[a-zA-Z0-9]+$/", $value) ) {
					$is_error = true;
				}
				
			}
			
		}
		
	   	return $is_error;
	
	}
	
	// 半角数字チェック
	function Num() {
		
		$is_error = false;
		$str = func_get_args();
	
		foreach( $str as $value ) {
			
			// $valueが配列の場合
			if ( is_array($value) ) {
			
				for( $i=0; $i<count($value); $i++ ) {
			
					if( !preg_match("/^[0-9]+$/", $value) ) {
						$is_error = true;
					}	
				
				}
			
			// その他
			} else {
			
				if( !preg_match("/^[0-9]+$/", $value) ) {
					$is_error = true;
				}	
				
			}
			
		}
		
	   	return $is_error;
	
	}
	
	// 日付チェック
	function Date( $year, $month, $day ) {
		
		$is_error = false;
		
		$date = getdate();
		
		// 年のチェック	
		if( $year > $date['year'] ) {
			$is_error = true;
		} else if ( $this->Num($year) ) {
			$is_error = true;
		}
		
		// 月チェック
		if( 1 > $month || $month > 12  ) {
			$is_error = true;
		} else if ( $this->Num($month) ) {
			$is_error = true;
		}
		
		// 日チェック
		switch($month) {
		
			case '1':
				$is_error = ( 0 > $day || $day > 31 ) ? true : $is_error;
			break;
		
			case '2':
			
				// うるう年の判定
				if( $year % 4 == 0 && $year % 100 != 0 ) {
					$is_error = ( 0 > $day || $day > 29 ) ? true : $is_error;
				} else if( $year % 400 == 0) {
					$is_error = ( 0 > $day || $day > 29 ) ? true : $is_error;
				} else {
					$is_error = ( 0 > $day || $day > 28 ) ? true : $is_error;
				}
	
			break;
		
			case '3':
				$is_error = ( 0 > $day || $day > 31 ) ? true : $is_error;
			break;
		
			case '4':
				$is_error = ( 0 > $day || $day > 30 ) ? true : $is_error;
			break;
		
			case '5':
				$is_error = ( 0 > $day || $day > 31 ) ? true : $is_error;
			break;
		
			case '6':
				$is_error = ( 0 > $day || $day > 30 ) ? true : $is_error;
			break;
		
			case '7':
				$is_error = ( 0 > $day || $day > 31 ) ? true : $is_error;
			break;
		
			case '8':
				$is_error = ( 0 > $day || $day > 31 ) ? true : $is_error;
			break;
		
			case '9':
				$is_error = ( 0 > $day || $day > 30 ) ? true : $is_error;
			break;
		
			case '10':
				$is_error = ( 0 > $day || $day > 31 ) ? true : $is_error;
			break;
		
			case '11':
				$is_error = ( 0 > $day || $day > 30 ) ? true : $is_error;
			break;
		
			case '12':
				$is_error = ( 0 > $day || $day > 31 ) ? true : $is_error;
			break;
			
			default:
				$is_error = true;
			break;
			
		}
		
	   	return $is_error;
	
	}
	
	// 郵便番号チェック
	function ZipCode( $zip_code ) {
	
		$is_error = false;
		$zip_str = $zip_code;
		
		// 変数が配列の場合
		if ( is_array($zip_code) ) {
		
			if ( count($zip_code) != 2 ) {
				return false;
			}
			
			$zip_str = join('-', $zip_code);
			
		}
		
		if ( !preg_match( "/^\d{3}\-\d{4}$/", $zip_str )) {
			
			$is_error = true;
			
		}
		
		return $is_error;
		
	}
	
	// 電話番号チェック
	function PhoneNo( $phone_no ){
	
		$is_error = false;
		$phone_str = $phone_no;
		
		// 変数が配列の場合
		if ( is_array($phone_no) ) {
		
			if ( count($phone_no) != 3 ) {
				return false;
			}
			
			$phone_str = join('-', $phone_no);
			
		}
		
		$phone_str = str_replace("-", "", $phone_str);
		
		// 正規表現で電話番号をチェック
		if ( !preg_match('/\d{6,}$/', $phone_str) ) {
			$is_error = true;
		}
		
		return $is_error;
		
	}
	
	// URLチェック
	function URL( $url ){
	
		$is_error = false;
	
		if ( !preg_match('/^(https?|ftp)(:\/\/[-_.!~*\'()a-zA-Z0-9;\/?:\@&=+\$,%#]+)$/', $url)) {
			$is_error = true;
		}
		
		return $is_error;
		
	}	
	
	// 半角英数、-(ハイフン)、_(アンダーバー)チェック
	function Custom1() {
		
		$is_error = false;
		$str = func_get_args();
	
		foreach( $str as $value ) {
			if( !preg_match( '/^[a-zA-Z0-9_\-]+$/',$value ) ) {
			$is_error = true;
			}	
		}
		
	   	return $is_error;
	
	}

	// 都道府県チェック
	function Prefs( $pref ) {
    static $prefs_data = array(
      '北海道', '青森県', '岩手県', '宮城県', '秋田県', '山形県', '福島県',
      '茨城県', '栃木県', '群馬県', '埼玉県', '千葉県', '東京都', '神奈川県',
      '新潟県', '富山県', '石川県', '福井県', '山梨県', '長野県', '岐阜県',
      '静岡県', '愛知県', '三重県', '滋賀県', '京都府', '大阪府', '兵庫県',
      '奈良県', '和歌山県', '鳥取県', '島根県', '岡山県', '広島県', '山口県',
      '徳島県', '香川県', '愛媛県', '高知県', '福岡県', '佐賀県', '長崎県',
      '熊本県', '大分県', '宮崎県', '鹿児島県', '沖縄県',
    );
		return !in_array($pref, $prefs_data);
	}
	
	
	// カタカナチェック
	function Kana() {
		
		$is_error = false;
		$str = func_get_args();
	
		foreach( $str as $value ) {
			
			// $valueが配列の場合
			if ( is_array($value) ) {
			
				for( $i=0; $i<count($value); $i++ ) {
			
					if( !preg_match("/^[ァ-ヾ\s　]+$/u", $value) ) {
						$is_error = true;
					}
				
				}
			
			// その他
			} else {
		
				if( !preg_match("/^[ァ-ヾ\s　]+$/u", $value) ) {
					$is_error = true;
				}
				
			}
			
		}
		
	   	return $is_error;
	
	}

}

?>