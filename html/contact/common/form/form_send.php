<?php
require_once("./config.php");

/* ----------------------------------------------------------------------------
ページ遷移のチェック
---------------------------------------------------------------------------- */
// 戻るボタンが押されていたら戻る
if ( isset($_GET['submit']) && $_GET['submit'] == 'back' ) {
	$_SESSION['back'] = true;
	header("Location: ".$checkPage);
	exit();
}

// ワンタイムキーに誤りがある場合
if ( !isset($_POST['p']) || $_POST['p'] != $_SESSION['p'] ) {
	$_SESSION = array();
	header( "Location: ".$indexPage );
	exit();

// ワンタイムキーが正しい場合
} else if( $_POST['p'] == $_SESSION['p'] ) {
	// 戻るボタンが押されていたら戻る
	if ( $_POST['submit'] == $returnBtn ) {
		$_SESSION['back'] = true;
		header("Location: ".$checkPage);
		exit();
	}

// その他
} else {
	// セッション変数を初期化
	$_SESSION = array();
	header( "Location: ".$indexPage );
	exit();
}


/* ----------------------------------------------------------------------------
メールの設定 - 管理者メール -
---------------------------------------------------------------------------- */
$mailSender = new mailClass();

// デバッグフラグ　リリース時にはfalseにしてください
$debug = false;

// メール情報の設定
$mailSetting = new stdClass();

if($debug){
	$mailSetting->to = $mailToAdmin;
	//$mailSetting->from = $_SESSION['name']." <".$_SESSION['mail'].">";
	//$mailSetting->from = $_SESSION['email'];
	$mailSetting->from = $mailFrom;
	$mailSetting->subject = $mailSubjectAdmin;
}else{
	$mailSetting->to = $mailToAdmin;
	//$mailSetting->from = $_SESSION['email'];
	$mailSetting->from = $mailFrom;
	$mailSetting->subject = $mailSubjectAdmin;
}
$mailSetting->cc = $mailToCc;
$mailSetting->bcc = $mailToBcc;


/* ----------------------------------------------------------------------------
メール本文の設定
---------------------------------------------------------------------------- */
$nowDate = date("Y-m-d(D) H：i");

// IPアドレスの取得
if(isset($_SERVER["REMOTE_ADDR"])) {
	$ip_address = $_SERVER["REMOTE_ADDR"];
} else {
	$ip_address = "不明";
}

// ホスト名
if(isset($_SERVER["REMOTE_ADDR"])) {
	// ローカルでテストする場合重くなるので下を使う
	$host_name = gethostbyaddr($_SERVER["REMOTE_ADDR"]);
	//$host_name = $_SERVER["REMOTE_ADDR"];
} else {
	$host_name = "不明";
}


/* ----------------------------------------------------------------------------
文字列置換 （メール表示用）
※配列は空だと{変数名}というふうに表示されるので必須
---------------------------------------------------------------------------- */
if ( $_SESSION['company']=="" ) {
	$_SESSION['company'] = "未入力";
}
if ( $_SESSION['busyo']=="" ) {
	$_SESSION['busyo'] = "未入力";
}
if ( $_SESSION['message']=="" ) {
	$_SESSION['message'] = "未入力";
}



$mailSetting->body = $mailBodyAdmin;

// SESSIONの値に置換
foreach($_SESSION as $key => $val){
	if ( is_array($val) ) {
		$val = implode("\r", $val);
	}
	$mailSetting->body = str_replace("{" . $key . "}", $val, $mailSetting->body );
}

//固定で文字列置換
$mailSetting->body = str_replace("{nowDate}", $nowDate, $mailSetting->body );
$mailSetting->body = str_replace("{ip_address}", $ip_address, $mailSetting->body );
$mailSetting->body = str_replace("{host_name}", $host_name, $mailSetting->body );

// \r\nを\rに変換
$mailSetting->body = str_replace("\r\n","\r", $mailSetting->body);
// \rを\nに変換
$mailSetting->body = str_replace("\r","\n", $mailSetting->body);


/* ----------------------------------------------------------------------------
メールの送信
---------------------------------------------------------------------------- */
$mailSender->setTo($mailSetting->to);
$mailSender->setBcc($mailSetting->bcc);
$mailSender->setCc($mailSetting->cc);
$mailSender->setFrom($mailSetting->from);
$mailSender->setSubject($mailSetting->subject);
$mailSender->setBody($mailSetting->body);
$mailSender->sendMail();


/* ----------------------------------------------------------------------------
メールの設定 - 訪問者 -
---------------------------------------------------------------------------- */
$mailSender2 = new mailClass();

// デバッグフラグ　リリース時にはfalseにしてください
$debug2 = true;

// メール情報の設定
$mailSetting2 = new stdClass();
if($debug2){
	$mailSetting2->to = $_SESSION['email'];
	$mailSetting2->from = $mailFromUser;
	$mailSetting2->subject = $mailSubjectUser;
}else{
	$mailSetting2->to = $_SESSION['email'];
	$mailSetting2->from = $mailFromUser;
	$mailSetting2->subject = $mailSubjectUser;
}
$mailSetting2->cc = "";
$mailSetting2->bcc = "";


/* ----------------------------------------------------------------------------
メール本文の設定
---------------------------------------------------------------------------- */
$nowDate = date("Y-m-d H：i");

$mailSetting2->body = $mailBodyUser;

// SESSIONの値に置換
foreach($_SESSION as $key => $val){
	if ( is_array($val) ) {
		$val = implode("\r" , $val);
	}
	$mailSetting2->body = str_replace("{" . $key . "}", $val, $mailSetting2->body );
}

$mailSetting2->body = str_replace("{nowDate}", $nowDate, $mailSetting2->body );

// \r\nを\rに変換
$mailSetting2->body = str_replace("\r\n","\r", $mailSetting2->body);
// \rを\nに変換
$mailSetting2->body = str_replace("\r","\n", $mailSetting2->body);


/* ----------------------------------------------------------------------------
メールの送信
---------------------------------------------------------------------------- */
$mailSender2->setTo($mailSetting2->to);
$mailSender2->setBcc($mailSetting2->bcc);
$mailSender2->setCc($mailSetting2->cc);
$mailSender2->setFrom($mailSetting2->from);
$mailSender2->setSubject($mailSetting2->subject);
$mailSender2->setBody($mailSetting2->body);
$mailSender2->sendMail();


/* ----------------------------------------------------------------------------
ページの遷移
---------------------------------------------------------------------------- */
header( "Location: ".$endPage );