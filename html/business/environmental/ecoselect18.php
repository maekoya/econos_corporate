<?php include("../../manager/include/dbconn.php") ?>
<?php include("../../manager/include/func.php") ?>
<?php include("../../manager/include/settings.php") ?>
<?php
// ニュース・インフォメーション情報取得
$row_ecopoints = Exec_Sql_ToA($sql_ecopoints);

// 表示ID
$goodscds = array(
	154,
);
// ID確認用
//echo "<!--";
//print_r($row_ecopoints);
//echo "-->";
?>
<!DOCTYPE html>
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
<meta charset="UTF-8">
<title>エコノスセレクト｜環境商品提供事業｜事業内容｜株式会社エコノス</title>
<meta name="description" content="株式会社エコノスのカーボンオフセット、カーボン・マネジメント、クレジット創出支援、リユース、ブックオフ、ハードオフ、オフハウス、ホビーオフ、ガレージオフ、e-コマース、e-コミュニケーションの各事業内容を紹介">
<meta name="viewport" content="width=device-width,user-scalable=1">
<meta name="format-detection" content="telephone=yes,address=no,email=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="copyright" content="Copytight © ECONOS Co.,Ltd. All Rights Reserved.">
<meta property="og:site_name" content="株式会社エコノス">
<meta property="og:image" content="">
<meta property="og:locale" content="ja_JP">
<?php include($_SERVER['DOCUMENT_ROOT']."/assets/tpl/mod_head.inc"); ?>
<link type="text/css" rel="stylesheet" href="/business/common/css/base.css">
<link type="text/css" rel="stylesheet" href="/business/common/css/business.css">
</head>



<body id="top">
<div style="background:#fff;">
<?php include($_SERVER['DOCUMENT_ROOT']."/assets/tpl/mod_header.inc"); ?>
</div>

<div class="mod-pankuzu">
    <ul>
        <li><a href="/">トップページ</a></li>
				<li>事業内容</li>
				<li><a href="/eco/">低炭素事業｜エコプロダクツ</a></li>
				<li>環境商品提供事業｜エコノスセレクト</li>
    </ul>
</div><!-- /.mod-pankuzu -->

<!-- top_title-->
<div id="top_title">
	<h2><img src="../../images/business/top_title02.gif" alt="エコノスセレクト　エコノスが選んだ環境配慮型商品" width="600" height="32"></h2>
</div>
<!-- /#top_title -->

<!-- contents -->
<div id="contents">
<!-- column_1 -->
	<div class="column_1">
		<div class="inner">
<!-- section h3 -->
			<section class="section01">
				<h3 class="eco_title"><img src="../../images/business/eco_main_title.gif" alt="エコノスセレクト 復興支援・住宅エコポイント交換商品のご案内。エコ住宅の新築や、エコリフォームにより取得したエコポイントを、エコノス取扱いの環境配慮型商品と交換いただけます。下記リストよりお好みの商品をお選びください。事業者コード V004" width="776" height="98"></h3>
				<div class="eco_sinsei">
					<dl class="sinsei01">
						<dt><img src="../../images/business/eco_sinsei_title01.gif" alt="WEBからの申請 " width="134" height="18"></dt>
						<dd>事務局のホームページ内の<a href="http://jutaku-eco-points.force.com/fukko">マイページ</a>に、ポイント通知ハガキに記載されている個人IDとパスコードを入力してログインください。</dd>
					</dl>
					<dl class="sinsei02">
						<dt><img src="../../images/business/eco_sinsei_title02.gif" alt="書類提出による申請 " width="171" height="18"></dt>
						<dd>申請書類を<a href="http://fukko-jutaku.eco-points.jp/download/">こちらよりダウンロード</a>していただき、弊社事業者コード <img src="../../images/business/eco_sinsei_code.gif" alt="V004" width="39" height="12"> と下記リストに記載の商品コードをメモ帳などに控えて頂きまして<a href="http://fukko-jutaku.eco-points.jp">エコポイント事務局</a>にご申請ください。<br>
							申請書類の記入方法については<a href="http://fukko-jutaku.eco-points.jp/common/file/user/apply_point_how01.pdf">こちら</a>をご参照ください。</dd>
					</dl>
				</div>
				<!-- eco_btn_area -->
				<div class="eco_btn_area">
					<ul class="eco_btn01">
						<li><a href="http://fukko-jutaku.eco-points.jp/mypage/"><img src="../../images/business/eco_btn01.jpg" alt="WEBからの申請方法" width="182" height="79" class="hoverImg"></a></li>
						<li><a href="http://fukko-jutaku.eco-points.jp/user/send/"><img src="../../images/business/eco_btn02.jpg" alt="書類提出による申請方法" width="213" height="79" class="hoverImg"></a></li>
						<li><a href="http://fukko-jutaku.eco-points.jp/download/"><img src="../../images/business/eco_btn03.jpg" alt="書類提出用申請書ダウンロード" width="232" height="79" class="hoverImg"></a></li>
					</ul>
					<div class="eco_btn02"><a href="http://fukko-jutaku.eco-points.jp"><img src="../../images/business/eco_btn04.jpg" alt="エコポイント事務局" width="121" height="76" class="hoverImg"></a></div>
				</div>
				<!-- /.eco_btn_area -->
				<!-- eco_product_area -->
				<section class="eco_product_area">
					<h4 id="ecopointItemTop"><img src="../../images/business/eco_title01.gif" alt="商品一覧" width="778" height="42"></h4>
					<div id="ecoselect_category">
						<ul>
							<li><a href="ecoselect19.php#ecopointItemTop"><img src="../../images/business/cat01.png" alt="エコポイントで掃除機と交換" class="hoverImg"></a></li>
							<li><a href="ecoselect01.php#ecopointItemTop"><img src="../../images/business/cat05.png" alt="エコポイントでロボット掃除機と交換" class="hoverImg"></a></li>
							<li><a href="ecoselect02.php#ecopointItemTop"><img src="../../images/business/cat02.png" alt="エコポイントで高圧洗浄機と交換" class="hoverImg"></a></li>
							<li><a href="ecoselect03.php#ecopointItemTop"><img src="../../images/business/cat03.png" alt="エコポイントでガジェットと交換" class="hoverImg"></a></li>
							<li><a href="ecoselect04.php#ecopointItemTop"><img src="../../images/business/cat04.png" alt="エコポイントでデジタルカメラと交換" class="hoverImg"></a></li>
							<li><a href="ecoselect05.php#ecopointItemTop"><img src="../../images/business/cat15.png" alt="エコポイントでプリンター・スキャナ・インクと交換" class="hoverImg"></a></li>
							<li><a href="ecoselect06.php#ecopointItemTop"><img src="../../images/business/cat06.png" alt="エコポイントで空気清浄機と交換" class="hoverImg"></a></li>
							<li><a href="ecoselect07.php#ecopointItemTop"><img src="../../images/business/cat10.png" alt="エコポイントでホームベーカリーと交換" class="hoverImg"></a></li>
							<li><a href="ecoselect08.php#ecopointItemTop"><img src="../../images/business/cat13.png" alt="エコポイントで電気ケトル・ポットと交換" class="hoverImg"></a></li>
							<li><a href="ecoselect09.php#ecopointItemTop"><img src="../../images/business/cat16.png" alt="エコポイントでコーヒーメーカーと交換" class="hoverImg"></a></li>
							<li><a href="ecoselect10.php#ecopointItemTop"><img src="../../images/business/cat17.png" alt="エコポイントで調理器具と交換" class="hoverImg"></a></li>
							<li><a href="ecoselect11.php#ecopointItemTop"><img src="../../images/business/cat18.png" alt="エコポイントで生活用品と交換" class="hoverImg"></a></li>
							<li><a href="ecoselect12.php#ecopointItemTop"><img src="../../images/business/cat12.png" alt="エコポイントで音響機器と交換" class="hoverImg"></a></li>
							<li><a href="ecoselect13.php#ecopointItemTop"><img src="../../images/business/cat20.png" alt="エコポイントでゲーム機器と交換" class="hoverImg"></a></li>
							<li><a href="ecoselect14.php#ecopointItemTop"><img src="../../images/business/cat22.png" alt="エコポイントでヒーターと交換" class="hoverImg"></a></li>
							<li><a href="ecoselect16.php#ecopointItemTop"><img src="../../images/business/cat07.png" alt="エコポイントで扇風機・サーキュレーターと交換" class="hoverImg"></a></li>
							<li><a href="ecoselect17.php#ecopointItemTop"><img src="../../images/business/cat08.png" alt="エコポイントで美容家電・電動歯ブラシ・シェーバーと交換" class="hoverImg"></a></li>
						</ul>
					</div>
					<h4 class="eco_cattitle">自転車</h4>
					<div class="eco_catlead">
						<p>
						コンパクトに収納可能な折り畳み自転車をエコポイント交換で。<br>
						新居の周辺で気持ち良いサイクリングを。
						</p>
					</div>
					<!-- eco_product -->
<?php foreach ($row_ecopoints as $key=>$val) { ?>
					<?php if(in_array($val['goodscd'], $goodscds)):?>
					<?php $link = getImageLink($val); ?>
					<div class="eco_product">
						<div class="product_img">
							<?php if (!empty($link)) { echo '<img src="'.$link.'" alt="" width="200" height="150">'; } ?>
						</div>
						<div class="product_txt">
							<div class="code">
							<table class="goodscd">
									<tr>
										<td class="td-title" nowrap>
											商品コード
										</td>
										<td class="td-code" nowrap>
											<?php echo $val['goodscd']; ?>
										</td>
									</tr>
								</table>
							</div>
							<h5><?php echo $val['goodsname'] ?></h5>
							<dl class="point">
								<dt>交換必要ポイント：</dt>
								<dd><?php echo number_format($val['point']); ?>pt</dd>
							</dl>
							<p>
								<?php echo hbr($val['explaination']); ?>
							</p>
						</div>
					</div>
					<?php endif;?>
<?php } ?>
					<!-- /.eco_product -->
					<!-- eco_btn_area -->
					<div class="eco_btn_area">
						<ul class="eco_btn01">
							<li><a href="http://fukko-jutaku.eco-points.jp/mypage/"><img src="../../images/business/eco_btn01.jpg" alt="WEBからの申請方法" width="182" height="79" class="hoverImg"></a></li>
							<li><a href="http://fukko-jutaku.eco-points.jp/user/send/"><img src="../../images/business/eco_btn02.jpg" alt="書類提出による申請方法" width="213" height="79" class="hoverImg"></a></li>
							<li><a href="http://fukko-jutaku.eco-points.jp/download/"><img src="../../images/business/eco_btn03.jpg" alt="書類提出用申請書ダウンロード" width="232" height="79" class="hoverImg"></a></li>
						</ul>
						<div class="eco_btn02"><a href="http://fukko-jutaku.eco-points.jp"><img src="../../images/business/eco_btn04.jpg" alt="エコポイント事務局" width="121" height="76" class="hoverImg"></a></div>
					</div>
					<!-- /.eco_btn_area -->
					<h4 class="eco_cattitle">その他のカテゴリーから探す</h4>
					<div id="ecoselect_category">
						<ul>
							<li><a href="ecoselect19.php#ecopointItemTop"><img src="../../images/business/cat01.png" alt="エコポイントで掃除機と交換" class="hoverImg"></a></li>
							<li><a href="ecoselect01.php#ecopointItemTop"><img src="../../images/business/cat05.png" alt="エコポイントでロボット掃除機と交換" class="hoverImg"></a></li>
							<li><a href="ecoselect02.php#ecopointItemTop"><img src="../../images/business/cat02.png" alt="エコポイントで高圧洗浄機と交換" class="hoverImg"></a></li>
							<li><a href="ecoselect03.php#ecopointItemTop"><img src="../../images/business/cat03.png" alt="エコポイントでガジェットと交換" class="hoverImg"></a></li>
							<li><a href="ecoselect04.php#ecopointItemTop"><img src="../../images/business/cat04.png" alt="エコポイントでデジタルカメラと交換" class="hoverImg"></a></li>
							<li><a href="ecoselect05.php#ecopointItemTop"><img src="../../images/business/cat15.png" alt="エコポイントでプリンター・スキャナ・インクと交換" class="hoverImg"></a></li>
							<li><a href="ecoselect06.php#ecopointItemTop"><img src="../../images/business/cat06.png" alt="エコポイントで空気清浄機と交換" class="hoverImg"></a></li>
							<li><a href="ecoselect07.php#ecopointItemTop"><img src="../../images/business/cat10.png" alt="エコポイントでホームベーカリーと交換" class="hoverImg"></a></li>
							<li><a href="ecoselect08.php#ecopointItemTop"><img src="../../images/business/cat13.png" alt="エコポイントで電気ケトル・ポットと交換" class="hoverImg"></a></li>
							<li><a href="ecoselect09.php#ecopointItemTop"><img src="../../images/business/cat16.png" alt="エコポイントでコーヒーメーカーと交換" class="hoverImg"></a></li>
							<li><a href="ecoselect10.php#ecopointItemTop"><img src="../../images/business/cat17.png" alt="エコポイントで調理器具と交換" class="hoverImg"></a></li>
							<li><a href="ecoselect11.php#ecopointItemTop"><img src="../../images/business/cat18.png" alt="エコポイントで生活用品と交換" class="hoverImg"></a></li>
							<li><a href="ecoselect12.php#ecopointItemTop"><img src="../../images/business/cat12.png" alt="エコポイントで音響機器と交換" class="hoverImg"></a></li>
							<li><a href="ecoselect13.php#ecopointItemTop"><img src="../../images/business/cat20.png" alt="エコポイントでゲーム機器と交換" class="hoverImg"></a></li>
							<li><a href="ecoselect14.php#ecopointItemTop"><img src="../../images/business/cat22.png" alt="エコポイントでヒーターと交換" class="hoverImg"></a></li>
							<li><a href="ecoselect16.php#ecopointItemTop"><img src="../../images/business/cat07.png" alt="エコポイントで扇風機・サーキュレーターと交換" class="hoverImg"></a></li>
							<li><a href="ecoselect17.php#ecopointItemTop"><img src="../../images/business/cat08.png" alt="エコポイントで美容家電・電動歯ブラシ・シェーバーと交換" class="hoverImg"></a></li>
						</ul>
					</div>
				</section>
				<!-- /eco_product_area -->

			</section>
<!-- /section h3 -->

		</div>
	</div>
<!-- /.column_1 -->

</div>
<!-- /#contents -->

<?php include($_SERVER['DOCUMENT_ROOT']."/assets/tpl/mod_footer.inc"); ?>
<?php include($_SERVER['DOCUMENT_ROOT']."/business/environmental/analyticstracking.php"); ?>
<script type="text/javascript">
function gaConvn2(){
   jQuery('body').append('<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/994400969/?value=1.00&amp;currency_code=JPY&amp;label=szqICMfXmBgQybWV2gM&amp;guid=ON&amp;script=0"/>');
}
function gaConvn3(){
   jQuery('body').append('<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/994400969/?label=dHKKCJqOhVcQybWV2gM&amp;guid=ON&amp;script=0"/>');
}
</script>
</body>
</html>