<?php
class InvestorrellistsController extends AppController {
    var $name       = 'Investorrellists';
    var $uses       = array('Investorrel');
    var $components = array();
    var $helpers    = array('Paginator');
    var $paginate;

    var $loginuser;

    function beforeFilter() {
        parent::beforeFilter();
        $auth = $this->Auth->user();
        if (!$auth) {
            // ログインしていない
            $this->redirect('/admin/logins/index');
        }
        $this->loginuser = $auth;
        $this->set('loginuser', $this->loginuser);
    }

    /**
     * ＩＲ一覧
     * @return void
     */
    function admin_index($hotelcd = null){

    	$searchValue = array();
    	
    	// 検索情報をセッションに保存
    	$this->Session->write('Investorrellist.page', 1);
    	$this->Session->write('Investorrellist.conditions', $searchValue);
    	 
        // ＩＲ一覧の取得
        $this->_searchInvestorrellists();

        return $this->_admin_index_rendering();
    }

    /**
     * ＩＲ一覧(検索)
     * @return void
     */
    function admin_search(){
        $searchValue = $this->Investorrel->searchValueExcerpt($this->data);

        // 検索情報をセッションに保存
        $this->Session->write('Investorrellist.page', 1);
        $this->Session->write('Investorrellist.conditions', $searchValue);

        // ＩＲ一覧の取得
        $this->_searchInvestorrellists();

        return $this->_admin_index_rendering();
    }

    /**
     * ＩＲ一覧(ページング)
     * @return void
     */
    function admin_paging(){
        if (isset($this->params['named']['page'])) {
            $this->Session->write('Investorrellist.page', $this->params['named']['page']);
        } else {
            $this->Session->write('Investorrellist.page', 1);
         }

         // ホテルコードセット
        $searchValue = $this->Session->read('Investorrellist.conditions');
        if (is_null($searchValue)) {
            $this->redirect('/admin/investorrellists');
         }
        $this->data  = Set::merge($this->data, $searchValue);

        // ＩＲ一覧の取得
        $this->_searchInvestorrellists();

        return $this->_admin_index_rendering();
    }

    function admin_delete($id=0){

    	// DB登録
    	$this->Investorrel->begin();
    	if ($this->Investorrel->delete($id)) {
    		$this->Session->setFlash('データの削除に成功しました。', 'default', array('class' => 'message_success'));
    	} else {
    		$this->Investorrel->rollback();
    		$this->Session->setFlash('データの削除に失敗しました。', 'default', array('class' => 'message_failed'));
    	}
    
    	// commit
    	$this->Investorrel->commit();
    
    	$this->redirect('/admin/investorrellists/index');

    }

    /**
     * ＩＲ詳細
     * @return void
     */
    function admin_detail($id = null){
        // ニュース・インフォメーション情報の取得
        $investorrel = $this->Investorrel->detailInvestorrel($id);
        // 取得できなければ一覧ページへ
        if ($investorrel === false) {
            $this->redirect('/admin/investorrellists/index');
        }
        $this->set('Investorrel', $investorrel);
        // 直前のページ番号を取得
        $this->set('prev_page_no', $this->Session->read('Investorrellist.page'));

        $this->set('title_for_layout', sprintf(C_TITLE, 'ＩＲ詳細'));
        $this->render('/investorrellists/admin_detail');
    }

    /**
     * ＩＲ一覧の取得
     * @return void
     */
    function _searchInvestorrellists(){
        // セッションから検索に関する情報を取得
        $page        = $this->Session->read('Investorrellist.page');
        $searchValue = $this->Session->read('Investorrellist.conditions');
        
        // ＩＲ一覧の取得
        $this->paginate = array(
            'limit'      => Configure::read('ListNumber.investorrellist'),
            'order'      => 'Investorrel.category, Investorrel.created'
        );
        $this->set('investorrels', $this->paginate('Investorrel'));
    }

    /**
     * ＩＲ一覧ページのレンダリング(ページングや検索などがあるので、ここだけ分けました。)
     * @return void
     */
    function _admin_index_rendering(){
        $this->set('title_for_layout', sprintf(C_TITLE, 'ＩＲ管理'));
        $this->render('/investorrellists/admin_index');
    }

    /**
     * ＩＲ PDFのレンダリング(ページングや検索などがあるので、ここだけ分けました。)
     * @return void
     */
    function _admin_show_rendering(){
    	$this->set('title_for_layout', sprintf(C_TITLE, 'URL'));
    	$this->render('/investorrellists/admin_show');
    }

}