<?php
class HiringlistsController extends AppController {
    var $name       = 'Hiringlists';
    var $uses       = array('Hiring');
    var $components = array();
    var $helpers    = array();

    var $loginuser;

    function beforeFilter() {
        parent::beforeFilter();
        $auth = $this->Auth->user();
        if (!$auth) {
            // ログインしていない
            $this->redirect('/admin/logins/index');
        }
        $this->loginuser = $auth;
        $this->set('loginuser', $this->loginuser);
    }

    /**
      * 採用条件一覧
     * @return void
     */
    function admin_index($hotelcd = null){
        $searchValue = array();

        // 検索情報をセッションに保存
//        $this->Session->write('Hiringlist.page', 1);
//        $this->Session->write('Hiringlist.conditions', $searchValue);

        // 採用条件一覧の取得
        $this->_searchHiringlists();

        return $this->_admin_index_rendering();
    }

    /**
     * 採用条件一覧(検索)
     * @return void
     */
    function admin_search(){
        $searchValue = $this->Hiring->searchValueExcerpt($this->data);

        // 採用条件一覧の取得
        $this->_searchHiringlists();

        return $this->_admin_index_rendering();
    }

    /**
     * 採用条件一覧(削除)
     * @return void
     */
    function admin_delete($id=0){

    	// DB登録
    	$this->Hiring->begin();
    	if ($this->Hiring->delete($id)) {
    		$this->Session->setFlash('データの削除に成功しました。', 'default', array('class' => 'message_success'));
    	} else {
    		$this->Hiring->rollback();
    		$this->Session->setFlash('データの削除に失敗しました。', 'default', array('class' => 'message_failed'));
    	}

    	// commit
    	$this->Hiring->commit();

    	$this->redirect('/admin/hiringlists/index');

    }

    /**
     * 採用条件一覧の取得
     * @return void
     */
    function _searchHiringlists(){
        
        // 採用条件一覧の取得
       $hiringList = $this->Hiring->hiringlistsFind();
    	$this->set('hirings', $hiringList);

    }

    /**
     * 採用条件一覧ページのレンダリング(ページングや検索などがあるので、ここだけ分けました。)
     * @return void
     */
    function _admin_index_rendering(){
        $this->set('title_for_layout', sprintf(C_TITLE, '採用条件管理'));
        $this->render('/hiringlists/admin_index');
    }

}