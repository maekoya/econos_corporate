<?php
class LoginsController extends AppController {
    var $name       = 'Logins';
    var $uses       = array('User');
    var $components = array();
    var $layout     = 'login_layout';
    var $helpers    = array();
    var $paginate;

    function beforeFilter() {
        parent::beforeFilter();
        $this->layout = 'login_layout';
    }

    /**
     * トップページ
     * @return void
     */
    function admin_index(){
        $auth = $this->Auth->user();
        if ($auth) {
            // ログイン成功
            $this->Session->renew();
            $this->redirect('/admin/infolists/index');
        }
        $this->set('title_for_layout', sprintf(C_TITLE, 'ログイン'));
        $this->render('/logins/admin_index');
    }

    /**
     * ログイン
     * @return void
     */
    function admin_login(){
        $auth = $this->Auth->user();
        if ($auth) {            
            // ログイン成功
            $this->Session->renew();
            $this->redirect('/admin/infolists/index');
        } else {
            $this->Session->setFlash('管理者IDとパスワードに誤りがあります。', 'default', array('class' => 'message_loginmiss'));
            $this->data['User']['passwd'] = $_POST['data']['User']['passwd'];
        }
        return $this->admin_index();
    }

    /**
     * ログアウト
     * @return void
     */
    function admin_logout(){
        $this->Session->destroy();
        $this->Auth->logout();
        $this->Session->setFlash('ログアウトしました。', 'default', array('class' => 'message_logout'));
        $this->redirect('/admin/logins/index');
    }

}