<?php
class InvestorrelentriesController extends AppController {
    var $name       = 'Investorrelentries';
    var $uses       = array('Investorrel');
    var $components = array ();
    var $helpers    = array();
    var $paginate;

    var $loginuser;

    function beforeFilter() {
        parent::beforeFilter();
        $auth = $this->Auth->user();
        if (!$auth) {
            // ログインしていない
            $this->redirect('/admin/logins/index');
        }
        $this->loginuser = $auth;
        $this->set('loginuser', $this->loginuser);
    }

    /**
     * ＩＲ登録(新規)
     * @return void
     */
    function admin_index(){
        // 初期アクセス用の入力値をセット
        $defaultData = $this->Investorrel->defaultInvestorrelData();
        $this->data  = Set::merge($this->data, $defaultData);

        // 前のページのページ番号をセット
        $this->set('prev_page_no', 1);

        $this->_admin_index_rendering();
    }

    /**
     * ＩＲ登録(編集)
     * @return void
     */
    function admin_edit($id = null){
        if (is_null($id)) {
            $this->redirect('/admin/investorrellists/index');
        }
        // 初期アクセス用の入力値をセット
        $editData   = $this->Investorrel->editInvestorrelData($id);
        $this->data = Set::merge($this->data, $editData);

        // 前のページのページ番号をセット
        $page = $this->Session->read('Investorrellist.page');
        $this->set('prev_page_no', $page);

        $this->_admin_index_rendering($id);
    }

    /**
     * 保存
     * @return void
     */
    function admin_save(){
        // 前のページのページ番号をセット
        $page = $this->Session->read('Investorrellist.page');
        if (is_null($page)) {
            $page = 1;
        }
        $this->set('prev_page_no', $page);

        // 削除が押下された場合
        if (isset($this->params['form']) && !empty($this->params['form']['delete'])) {
        	$this->data['Investorrel']['pdf_delete'] = 1;
        	$this->data['Investorrel']['pdf'] = 2;
        } else {
        
	        // PDFチェック
	        $pdfValidate = $this->Investorrel->uploadPdfValidate($this->data, 'pdf');
	        // 入力チェック
	        $this->Investorrel->set($this->data);
	        $valid = $this->Investorrel->validates();
	        // どちらかにエラーがあれば、入力ページを再表示
	        if (!$valid || count($pdfValidate) > 0) {
	            $validError = $this->Investorrel->validationErrors;
	            // 入力にエラーがある場合は、PDFを破棄する
	            if (!$valid && !isset($validError['pdf']) && $this->data['Investorrel']['pdf']['error'] == 0) {
	                $validError['pdf'] = '他の項目にエラーがありましたので、PDFを破棄しました。<br />再度アップロードをお願いいたします。';
	            }
	            // 画像と入力のエラーをマージする
	            $error['Investorrel'] = Set::merge($validError, $pdfValidate);
	
	            $this->set('validErrors', $error);
	            return $this->_admin_index_rendering();
	        }
        }
        // DB登録
        $this->Investorrel->begin();
        //   更新者データを埋め込む
        $this->data['User'] = $this->loginuser['User'];
        $this->data['Investorrel']['modified_user_id'] = $this->data['User']['userid'];
        //   登録データを組み立てる
        $investorrelData = $this->Investorrel->arrangeData($this->data);
        //   保存
        $this->Investorrel->create();
        if ($this->Investorrel->save($investorrelData, false)) {
            $this->Session->setFlash('データベースの更新に成功しました。', 'default', array('class' => 'message_success'));
            // idが無い場合はinsertなので、追加されたidをセット
            if (empty($investorrelData['Investorrel']['id'])) {
                $investorrelData['Investorrel']['id'] = $this->Investorrel->getLastInsertID();
            }
        } else {
            // rollaback
            $this->Investorrel->rollback();
            $this->Session->setFlash('データベースの更新に失敗しました。', 'default', array('class' => 'message_failed'));
            $this->redirect('/admin/investorrelentries/index');
        }
        // commit
        $this->Investorrel->commit();

        // PDFの保存
        if (!isset($this->data['Investorrel']['pdf_delete']) && $this->data['Investorrel']['pdf']['error'] == 0) {
            // アップロードされている
            //   アップロード先を取得
            $pdfPath = $this->Investorrel->pdfPath($investorrelData);
            
            if (move_uploaded_file($this->data['Investorrel']['pdf']['tmp_name'], $pdfPath['filepath'])) {
            	chmod($pdfPath['filepath'], 0666);
            } else {
            	$this->Session->setFlash('PDFファイルのアップロードに失敗しました。', 'default', array('class' => 'message_failed'));
              $this->redirect('/admin/investorrelentries/index');
            }
        } elseif(isset($this->data['Investorrel']['pdf_delete']) && $this->data['Investorrel']['pdf_delete'] == 1) {
            // PDFの「削除する」にチェックされているので、PDFファイルを削除する
            //   アップロード先を取得
            $pdfPath = $this->Investorrel->pdfPath($investorrelData);
            //   PDFを削除
            foreach ($pdfPath as $path) {
                if (file_exists($path)) {
                    unlink($path);
                }
            }
        }

        $this->redirect('/admin/investorrelentries/edit/'.$investorrelData['Investorrel']['id']);
    }

    function admin_delete(){
    	// 前のページのページ番号をセット
    	$page = $this->Session->read('Investorrellist.page');
    	if (is_null($page)) {
    		$page = 1;
    	}
    	$this->set('prev_page_no', $page);

    	// 画像の削除
    	$investorrelData = $this->Investorrel->arrangeData($this->data);
		$imgPath = $this->Investorrel->pdfPath($investorrelData);


    }
    /**
     * ＩＲ基本情報登録ページのレンダリング(入力エラー後に遷移したりするので、ここだけ分けました。)
     * @return void
     */
    function _admin_index_rendering($edit = null){
        if (is_null($edit)) {
            $this->set('title_for_layout', sprintf(C_TITLE, 'ＩＲ登録'));
        } else {
            $this->set('title_for_layout', sprintf(C_TITLE, 'ＩＲ更新'));
        }
        $this->render('/investorrelentries/admin_index');
    }

}