<?php
class InfolistsController extends AppController {
    var $name       = 'Infolists';
    var $uses       = array('Info');
    var $components = array();
    var $helpers    = array('Paginator');
    var $paginate;

    var $loginuser;

    function beforeFilter() {
        parent::beforeFilter();
        $auth = $this->Auth->user();
        if (!$auth) {
            // ログインしていない
            $this->redirect('/admin/logins/index');
        }
        $this->loginuser = $auth;
        $this->set('loginuser', $this->loginuser);
    }

    /**
      * ニュース・インフォメーション一覧
     * @return void
     */
    function admin_index($hotelcd = null){

        $searchValue = array();

        // 検索情報をセッションに保存
        $this->Session->write('Infolist.page', 1);
        $this->Session->write('Infolist.conditions', $searchValue);

        // ニュース・インフォメーション一覧の取得
        $this->_searchInfolists();

        return $this->_admin_index_rendering();
    }

    /**
      * ニュース・インフォメーション一覧(検索)
     * @return void
     */
    function admin_search(){
        $searchValue = $this->Info->searchValueExcerpt($this->data);

        // 検索情報をセッションに保存
        $this->Session->write('Infolist.page', 1);
        $this->Session->write('Infolist.conditions', $searchValue);

        // ニュース・インフォメーション一覧の取得
        $this->_searchInfolists();

        return $this->_admin_index_rendering();
    }

    /**
     * ニュース・インフォメーション一覧(ページング)
     * @return void
     */
    function admin_paging(){
        if (isset($this->params['named']['page'])) {
            $this->Session->write('Infolist.page', $this->params['named']['page']);
        } else {
            $this->Session->write('Infolist.page', 1);
         }

         // ホテルコードセット
        $searchValue = $this->Session->read('Infolist.conditions');
        if (is_null($searchValue)) {
            $this->redirect('/admin/infolists');
         }
        $this->data  = Set::merge($this->data, $searchValue);

        // ニュース・インフォメーション一覧の取得
        $this->_searchInfolists();

        return $this->_admin_index_rendering();
    }

    function admin_delete($id=0){

    	// DB登録
    	$this->Info->begin();
    	if ($this->Info->delete($id)) {
    		$this->Session->setFlash('データの削除に成功しました。', 'default', array('class' => 'message_success'));
    	} else {
    		$this->Info->rollback();
    		$this->Session->setFlash('データの削除に失敗しました。', 'default', array('class' => 'message_failed'));
    	}
    
    	// commit
    	$this->Info->commit();
    
    	$this->redirect('/admin/infolists/index');

    }

    /**
     * ニュース・インフォメーション詳細
     * @return void
     */
    function admin_detail($id = null){
        // ニュース・インフォメーション情報の取得
        $newsinfo = $this->Info->detailInfo($id);
        // 取得できなければ一覧ページへ
        if ($newsinfo === false) {
            $this->redirect('/admin/infolists/index');
        }
        $this->set('Info', $newsinfo);
        // 直前のページ番号を取得
        $this->set('prev_page_no', $this->Session->read('Infolist.page'));

        $this->set('title_for_layout', sprintf(C_TITLE, 'ニュース・インフォメーション詳細'));
        $this->render('/infolists/admin_detail');
    }

    /**
     * ニュース・インフォメーション一覧の取得
     * @return void
     */
    function _searchInfolists(){
        // セッションから検索に関する情報を取得
        $page        = $this->Session->read('Infolist.page');
        $searchValue = $this->Session->read('Infolist.conditions');
        
        // ニュース・インフォメーション一覧の取得
        $this->paginate = array(
            'limit'      => Configure::read('ListNumber.infolist'),
            'order'      => 'Info.ymd desc'
        );
        $this->set('infos', $this->paginate('Info'));
    }

    /**
     * ニュース・インフォメーション一覧ページのレンダリング(ページングや検索などがあるので、ここだけ分けました。)
     * @return void
     */
    function _admin_index_rendering(){
        $this->set('title_for_layout', sprintf(C_TITLE, 'ニュース・インフォメーション一覧'));
        $this->render('/infolists/admin_index');
    }

    /**
     * ニュース・インフォメーション PDFのレンダリング(ページングや検索などがあるので、ここだけ分けました。)
     * @return void
     */
    function _admin_show_rendering(){
    	$this->set('title_for_layout', sprintf(C_TITLE, 'URL'));
    	$this->render('/infolists/admin_show');
    }
    
}