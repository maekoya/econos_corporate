<?php
class HiringentriesController extends AppController {
    var $name       = 'Hiringentries';
    var $uses       = array('Hiring', 'Jobcontent');
    var $components = array ();
    var $helpers    = array();
    var $paginate;

    var $loginuser;

    function beforeFilter() {
        parent::beforeFilter();
        $auth = $this->Auth->user();
        if (!$auth) {
            // ログインしていない
            $this->redirect('/admin/logins/index');
        }
        $this->loginuser = $auth;
        $this->set('loginuser', $this->loginuser);
    }

    /**
     * 採用条件登録(新規)
     * @return void
     */
    function admin_index(){
        // 初期アクセス用の入力値をセット
        $defaultData = $this->Hiring->defaultHiringData();
        $this->data  = Set::merge($this->data, $defaultData);

        $this->_admin_index_rendering();

    }

    /**
     * 採用条件登録(編集)
     * @return void
     */
    function admin_edit($id = null){
        if (is_null($id)) {
            $this->redirect('/admin/hiringlists/index');
        }
        // 初期アクセス用の入力値をセット
        $editData   = $this->Hiring->editHiringData($id);
        $this->data = Set::merge($this->data, $editData);

        $this->_admin_index_rendering($id);
    }

    /**
     * 保存
     * @return void
     */
    function admin_save(){

    	$error = array();
        // 入力チェック
        $this->Hiring->set($this->data);
        $valid = $this->Hiring->validates();

        $chk =& Validation::getInstance();

        // 項目名・内容１?１５の入力チェック＆
        foreach ($this->data['Jobcontent'] as $key=>$val) {
        	if (empty($val['title']) && empty($val['contents'])) {
        		// 両方空はエラーなし
        		;
        	}
        	else if (!empty($val['title']) && !empty($val['contents'])) {
        		// 両方入力されていればエラーなし
        		;
        	}
        	else {
        		$this->validationErrors[$key]['title'] = '項目名・内容どちらかが入力された場合は両方とも必須です';
        	}

        	if (!$chk->maxLength($val['title'], 100) ) {
        		$this->validationErrors[$key]['title'] = '項目名は100文字以内で入力してください。';
        	}
         	if (!$chk->maxLength($val['contents'], 20000) ) {
        		$this->validationErrors[$key]['contents'] = '内容は20000文字以内で入力してください。';
        	}

        }

        // 求人内容1の必須チェック
      	if (empty($this->data['Jobcontent'][0]['title']) || empty($this->data['Jobcontent'][0]['contents'])) {
        		$this->validationErrors[0]['title'] = '求人内容1が入力されていません。';
      	}

       if (count($this->validationErrors)>0) {
        	$validError = $this->validationErrors;
        	$error['Jobcontent'] = $validError;
        }
		if (!$valid) {
		   $validError = $this->Hiring->validationErrors;
		   $error['Hiring'] = $validError;
		}

       // エラーがあれば、入力ページを再表示
		if (count($error)>0) {
			$this->set('validErrors', $error);
			return $this->_admin_index_rendering();
		}

        // DB登録
        $this->Hiring->begin();
        //   更新者データを埋め込む
        $this->data['User'] = $this->loginuser['User'];
        $this->data['Hiring']['modified_user_id'] = $this->data['User']['userid'];
        foreach ($this->data['Jobcontent'] as &$key) {
        	$id = $key['id'];
        	$key['modified_user_id'] = $this->data['User']['userid'];
         }
        //   登録データを組み立てる
        $storeData = $this->Hiring->arrangeData($this->data);

        //   保存
        $this->Hiring->create();
        if ($this->Hiring->saveAll($storeData)) {
            $this->Session->setFlash('データベースの更新に成功しました。', 'default', array('class' => 'message_success'));
            // idが無い場合はinsertなので、追加されたidをセット
            if (empty($storeData['Hiring']['id'])) {
                $storeData['Hiring']['id'] = $this->Hiring->getLastInsertID();
            }
        } else {
            // rollaback
            $this->Hiring->rollback();
            $this->Session->setFlash('データベースの更新に失敗しました。', 'default', array('class' => 'message_failed'));
            $this->redirect('/admin/hiringentries/index');
        }
        // commit
        $this->Hiring->commit();

        $this->redirect('/admin/hiringentries/edit/'.$storeData['Hiring']['id']);
    }

    /**
     * 削除
     * @return void
     */
    function admin_delete($id=0){

   	// DB登録
    	$this->Hiring->begin();
    	if ($this->Hiring->delete($id)) {
    		$this->Session->setFlash('データの削除に成功しました。', 'default', array('class' => 'message_success'));
    	} else {
    		$this->Hiring->rollback();
    		$this->Session->setFlash('データの削除に失敗しました。', 'default', array('class' => 'message_failed'));
    	}
    
    	// commit
    	$this->Hiring->commit();
    
    	$this->redirect('/admin/hiringlists/index');

    }

    /**
      * 採用条件登録ページのレンダリング(入力エラー後に遷移したりするので、ここだけ分けました。)
     * @return void
     */
    function _admin_index_rendering($edit = null){
        if (is_null($edit)) {
            $this->set('title_for_layout', sprintf(C_TITLE, '採用条件登録'));
        } else {
            $this->set('title_for_layout', sprintf(C_TITLE, '採用条件更新'));
        }
        $this->render('/hiringentries/admin_index');
    }

}