<?php
class StoresortsController extends AppController {
    var $name       = 'Storesorts';
    var $uses       = array('Store');
    var $components = array();
//    var $helpers    = array('Paginator');
    var $paginate;

    var $loginuser;

    function beforeFilter() {
        parent::beforeFilter();
        $auth = $this->Auth->user();
        if (!$auth) {
            // ログインしていない
            $this->redirect('/admin/logins/index');
        }
        $this->loginuser = $auth;
        $this->set('loginuser', $this->loginuser);
    }

    /**
      * 運営店舗一覧
     * @return void
     */
    function admin_index($storetype = null){
        $searchValue = $this->_defaultSearchParams($storetype);

        // 検索情報をセッションに保存
        $this->Session->write('Storelist.conditions', $searchValue);

        // 店舗タイプセット
        $this->data = Set::merge($this->data, $searchValue);

        // 運営店舗一覧の取得
        $this->_searchStorelists();

        return $this->_admin_index_rendering($storetype);
    }

    /**
      * 運営店舗一覧(検索)
     * @return void
     */
    function admin_search(){
        $searchValue = $this->Store->searchValueExcerpt($this->data);

        // 検索情報をセッションに保存
        $this->Session->write('Storelist.conditions', $searchValue);

        // 運営店舗一覧の取得
        $this->_searchStorelists();

        return $this->_admin_index_rendering();
    }

    /**
      * 運営店舗一覧(検索)
     * @return void
     */
    function admin_set($movetype, $id = null){
        // 運営店舗情報の取得
        $store = $this->Store->detailStore($id);
        // 取得できなければ一覧ページへ
        if ($store === false) {
            $this->redirect('/admin/storesorts/index');
        }

        // 順番入れ替え
        $this->Store->begin();
        $ret = $this->Store->orderUpdate($movetype, $id, $this->loginuser['User']['userid']);
        if ($ret) {
            $this->Session->setFlash('並び順を更新しました。', 'default', array('class' => 'message_success'));
            $this->Store->commit();
        } else {
            $this->Store->rollback();
        }

        // 店舗コードセット
        $searchValue = $this->Session->read('Storelist.conditions');
        $this->data  = Set::merge($this->data, $searchValue);

        // 運営店舗一覧の取得
        $this->_searchStorelists();

        return $this->_admin_index_rendering();
    }

    /**
      * 運営店舗一覧の取得
     * @return void
     */
    function _searchStorelists(){

        $searchValue = $this->Session->read('Storelist.conditions');
        $conditions  = $this->Store->createConditions($searchValue);

        // 運営店舗一覧の取得
        $storeList = $this->Store->storesortsFind($conditions);

        $this->set('stores', $storeList);
    }

    /**
     * デフォルトの検索条件を取得
     * @return void
     */
    function _defaultSearchParams($stype = null){

        // 店舗タイプ設定
        $storetype = 'BookOFF';
        if (is_null($stype)) {
                // 最初のホテルコードを採用
                $storetypeType = Constant::$storetype_s;
                reset($storetypeType);
                $storetype = key($storetypeType);
       } else {
            $storetype = $stype;
        }

        // 検索条件取得
        $searchParams = array(
            'Store' => array(
                'storetype'   => $storetype,
            )
        );

        return $searchParams;
    }

    /**
     * 運営店舗一覧ページのレンダリング(ページングや検索などがあるので、ここだけ分けました。)
     * @return void
     */
    function _admin_index_rendering($storetype=null){
        $this->set('title_for_layout', sprintf(C_TITLE, '運営店舗管理'));
        $this->render('/storesorts/admin_index');
    }

}