<?php
class EcopointsortsController extends AppController {
    var $name       = 'Ecopointsorts';
    var $uses       = array('Ecopoint');
    var $components = array();
    var $paginate;

    var $loginuser;

    function beforeFilter() {
        parent::beforeFilter();
        $auth = $this->Auth->user();
        if (!$auth) {
            // ログインしていない
            $this->redirect('/admin/logins/index');
        }
        $this->loginuser = $auth;
        $this->set('loginuser', $this->loginuser);
    }

    /**
      * エコポイント一覧
     * @return void
     */
    function admin_index(){
        // エコポイント一覧の取得
        $this->_searchEcopointlists();

        return $this->_admin_index_rendering();
    }

    /**
     * エコポイント一覧(検索)
     * @return void
     */
    function admin_search(){
        $searchValue = $this->Ecopoint->searchValueExcerpt($this->data);

        // エコポイント一覧の取得
        $this->_searchEcopointlists();

        return $this->_admin_index_rendering();
    }

    /**
     * エコポイント一覧(検索)
     * @return void
     */
    function admin_set($movetype, $id = null){
        // エコポイント情報の取得
        $ecopoint = $this->Ecopoint->detailEcopoint($id);
        // 取得できなければ一覧ページへ
        if ($ecopoint === false) {
            $this->redirect('/admin/ecopointsorts/index');
        }

        // 順番入れ替え
        $this->Ecopoint->begin();
        $ret = $this->Ecopoint->orderUpdate($movetype, $id, $this->loginuser['User']['userid']);
        if ($ret) {
            $this->Session->setFlash('並び順を更新しました。', 'default', array('class' => 'message_success'));
            $this->Ecopoint->commit();
        } else {
            $this->Ecopoint->rollback();
        }

        // エコポイント一覧の取得
        $this->_searchEcopointlists();

        return $this->_admin_index_rendering();
    }

    /**
     * エコポイント一覧の取得
     * @return void
     */
    function _searchEcopointlists(){

        // エコポイント一覧の取得
        $ecopointList = $this->Ecopoint->ecopointsortsFind();

        $this->set('ecopoints', $ecopointList);
    }

    function admin_delete($id=0){

    	// DB登録
    	$this->Ecopoint->begin();
    	if ($this->Ecopoint->resetDisplayOrder($id) && $this->Ecopoint->delete($id)) {
    		$this->Session->setFlash('データの削除に成功しました。', 'default', array('class' => 'message_success'));
    	} else {
    		$this->Ecopoint->rollback();
    		$this->Session->setFlash('データの削除に失敗しました。', 'default', array('class' => 'message_failed'));
    	}
    
    	// commit
    	$this->Ecopoint->commit();
    
    	$this->redirect('/admin/ecopointsorts/index');
    
    }

    /**
     * エコポイント一覧ページのレンダリング(ページングや検索などがあるので、ここだけ分けました。)
     * @return void
     */
    function _admin_index_rendering(){
        $this->set('title_for_layout', sprintf(C_TITLE, 'エコポイント管理'));
        $this->render('/ecopointsorts/admin_index');
    }

}