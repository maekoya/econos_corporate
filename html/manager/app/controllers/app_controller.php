<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       cake
 * @subpackage    cake.cake.libs.controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

/**
 * This is a placeholder class.
 * Create the same file in app/app_controller.php
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package       cake
 * @subpackage    cake.cake.libs.controller
 * @link http://book.cakephp.org/view/957/The-App-Controller
 */
class AppController extends Controller {
    var $components = array(
        'Auth', 
        'Cookie', 
        'Session',
        'Security',
        'DebugKit.Toolbar',
    );
    var $layout = 'default_layout';
	var $helpers = array('Session', 'Html', 'Form', 'Javascript');

    function beforeFilter() {
        $this->Security->validatePost = false;
        $this->Auth->allow('*');
        $this->Auth->autoRedirect = false;
        $this->Auth->userModel = 'User';
        if (isset($this->params['prefix']) && $this->params['prefix'] == 'admin') {
            $this->layout = 'admin_layout';
            $this->Auth->fields = array(
                'username' => 'userid',
                'password' => 'passwd'
            );
            $this->Auth->loginAction = array(
                'admin' => false,
                'controller' => 'admin/logins',
                'action' => 'login'
            );
            $this->Auth->logoutAction = array(
                'admin' => false,
                'controller' => 'admin/logins',
                'action' => 'logout'
            );
            //　エラーメッセージを設定
            $this->Auth->loginError = '';
            $this->Auth->authError = ' ';
        }
    }
    
    /*
     * 正しい日付かチェック
    * @param array $data 入力値
    * @param array $arg 専用引数
    * @return boolean
    */
    function isDate($data, $arg) {
    	$val = $this->data['Tradedetail'][$arg[0]];
    	$y   = substr($val, 0, 4);
    	$m   = substr($val, 4, 2);
    	$d   = substr($val, 6, 2);
    	if (checkdate($m, $d, $y)) {
    		return true;
    	} else {
    		return false;
    	}
    }
}
