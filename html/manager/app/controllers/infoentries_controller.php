<?php
class InfoentriesController extends AppController {
    var $name       = 'Infoentries';
    var $uses       = array('Info');
    var $components = array ('Thumbmake');
    var $helpers    = array();
    var $paginate;

    var $loginuser;

    function beforeFilter() {
        parent::beforeFilter();
        $auth = $this->Auth->user();
        if (!$auth) {
            // ログインしていない
            $this->redirect('/admin/logins/index');
        }
        $this->loginuser = $auth;
        $this->set('loginuser', $this->loginuser);
    }

    /**
      * ニュース・インフォメーション登録(新規)
     * @return void
     */
    function admin_index(){
        // 初期アクセス用の入力値をセット
        $defaultData = $this->Info->defaultInfoData();
        $this->data  = Set::merge($this->data, $defaultData);

        // 前のページのページ番号をセット
        $this->set('prev_page_no', 1);

        $this->_admin_index_rendering();
    }

    /**
      * ニュース・インフォメーション登録(編集)
     * @return void
     */
    function admin_edit($id = null){
        if (is_null($id)) {
            $this->redirect('/admin/infolists/index');
        }
        // 初期アクセス用の入力値をセット
        $editData   = $this->Info->editInfoData($id);
        $this->data = Set::merge($this->data, $editData);

        // 前のページのページ番号をセット
        $page = $this->Session->read('Infolist.page');
        $this->set('prev_page_no', $page);

        $this->_admin_index_rendering($id);
    }

    /**
     * 保存
     * @return void
     */
    function admin_save(){
        // 前のページのページ番号をセット
        $page = $this->Session->read('Infolist.page');
        if (is_null($page)) {
            $page = 1;
        }
        $this->set('prev_page_no', $page);

        // 削除が押下された場合
        if (isset($this->params['form']) && !empty($this->params['form']['delete'])) {
			$this->data['Info']['pdf_delete'] = 1;
			$this->data['Info']['pdf'] = 2;
        } else {

	        // PDFチェック
	        $pdfValidate = $this->Info->uploadPdfValidate($this->data, 'pdf');
	        // 入力チェック
	        $this->Info->set($this->data);
	        $valid = $this->Info->validates();
	        // どちらかにエラーがあれば、入力ページを再表示
	        if (!$valid || count($pdfValidate) > 0) {
	            $validError = $this->Info->validationErrors;
	            // 入力にエラーがある場合は、PDFを破棄する
	            if (!$valid && !isset($validError['pdf']) && $this->data['Info']['pdf']['error'] == 0) {
	                $validError['pdf'] = '他の項目にエラーがありましたので、PDFを破棄しました。<br />再度アップロードをお願いいたします。';
	            }
	            // 画像と入力のエラーをマージする
	            $error['Info'] = Set::merge($validError, $pdfValidate);
	
	            $this->set('validErrors', $error);
	            return $this->_admin_index_rendering();
	        }
        }
        // DB登録
        $this->Info->begin();
        //   更新者データを埋め込む
        $this->data['User'] = $this->loginuser['User'];
        $this->data['Info']['modified_user_id'] = $this->data['User']['userid'];
        //   登録データを組み立てる
        $storeData = $this->Info->arrangeData($this->data);
        //   保存
        $this->Info->create();
        if ($this->Info->save($storeData, false)) {
            $this->Session->setFlash('データベースの更新に成功しました。', 'default', array('class' => 'message_success'));
            // idが無い場合はinsertなので、追加されたidをセット
            if (empty($storeData['Info']['id'])) {
                $storeData['Info']['id'] = $this->Info->getLastInsertID();
            }
        } else {
            // rollaback
            $this->Info->rollback();
            $this->Session->setFlash('データベースの更新に失敗しました。', 'default', array('class' => 'message_failed'));
            $this->redirect('/admin/infoentries/index');
        }
        // commit
        $this->Info->commit();

        // 画像の保存
        if (!isset($this->data['Info']['pdf_delete']) && $this->data['Info']['pdf']['error'] == 0) {
            // アップロードされている
            //   アップロード先を取得
            $pdfPath = $this->Info->pdfPath($storeData);

            if (move_uploaded_file($this->data['Info']['pdf']['tmp_name'], $pdfPath['filepath'])) {
            	chmod($pdfPath['filepath'], 0666);
            } else {
            	$this->Session->setFlash('PDFファイルのアップロードに失敗しました。', 'default', array('class' => 'message_failed'));
              $this->redirect('/admin/infoentries/index');
            }

        } elseif(isset($this->data['Info']['pdf_delete']) && $this->data['Info']['image_delete'] == 1) {
            // 画像の「削除する」にチェックされているので、PDFファイルを削除する
            //   アップロード先を取得
            $imgPath = $this->Info->pdfPath($storeData);
            //   画像を削除
            foreach ($imgPath as $path) {
                if (file_exists($path)) {
                    unlink($path);
                }
            }
        }

        $this->redirect('/admin/infoentries/edit/'.$storeData['Info']['id']);
    }

    function admin_delete(){
    	// 前のページのページ番号をセット
    	$page = $this->Session->read('Infolist.page');
    	if (is_null($page)) {
    		$page = 1;
    	}
    	$this->set('prev_page_no', $page);

    	// 画像の削除
    	$storeData = $this->Info->arrangeData($this->data);
		$imgPath = $this->Info->pdfPath($storeData);
    }

    /**
     * ニュース・インフォメーション登録ページのレンダリング(入力エラー後に遷移したりするので、ここだけ分けました。)
     * @return void
     */
    function _admin_index_rendering($edit = null){
        if (is_null($edit)) {
            $this->set('title_for_layout', sprintf(C_TITLE, 'ニュース・インフォメーション登録'));
        } else {
            $this->set('title_for_layout', sprintf(C_TITLE, 'ニュース・インフォメーション更新'));
        }
        $this->render('/infoentries/admin_index');
    }

}