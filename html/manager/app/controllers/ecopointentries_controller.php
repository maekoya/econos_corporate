<?php
class EcopointentriesController extends AppController {
    var $name       = 'Ecopointentries';
    var $uses       = array('Ecopoint');
    var $components = array ('Thumbmake');
    var $helpers    = array();
    var $paginate;

    var $loginuser;

    function beforeFilter() {
        parent::beforeFilter();
        $auth = $this->Auth->user();
        if (!$auth) {
            // ログインしていない
            $this->redirect('/admin/logins/index');
        }
        $this->loginuser = $auth;
        $this->set('loginuser', $this->loginuser);
    }

    /**
     * エコポイント登録(新規)
     * @return void
     */
    function admin_index(){
        // 初期アクセス用の入力値をセット
        $defaultData = $this->Ecopoint->defaultEcopointData();
        $this->data  = Set::merge($this->data, $defaultData);

        $this->_admin_index_rendering();

    }

    /**
     * エコポイント基本情報登録(編集)
     * @return void
     */
    function admin_edit($id = null){
        if (is_null($id)) {
            $this->redirect('/admin/ecopointlists/index');
        }
        // 初期アクセス用の入力値をセット
        $editData   = $this->Ecopoint->editEcopointData($id);
        $this->data = Set::merge($this->data, $editData);

        $this->_admin_index_rendering($id);
    }

    /**
     * 保存
     * @return void
     */
    function admin_save(){

    	// 削除が押下された場合
    	if (isset($this->params['form']) && !empty($this->params['form']['delete'])) {
    		$this->data['Ecopoint']['image_delete'] = 1;
    		$this->data['Ecopoint']['image'] = 2;
    	} else {
	        // 画像チェック
	        $imgValidate = $this->Ecopoint->uploadImageValidate($this->data, 'image');
	    	// 入力チェック
	        $this->Ecopoint->set($this->data);
	        $valid = $this->Ecopoint->validates();
	        // どちらかにエラーがあれば、入力ページを再表示
	        if (!$valid || count($imgValidate) > 0) {
	            $validError = $this->Ecopoint->validationErrors;
	            // 入力にエラーがある場合は、画像を破棄する
	            if (!$valid && !isset($validError['image']) && $this->data['Ecopoint']['image']['error'] == 0) {
	                $validError['image'] = '他の項目にエラーがありましたので、画像を破棄しました。<br />再度アップロードをお願いいたします。';
	            }
	            // 画像と入力のエラーをマージする
	            $error['Ecopoint'] = Set::merge($validError, $imgValidate);
	
	            $this->set('validErrors', $error);
	            return $this->_admin_index_rendering();
	        }
    	 }

        // DB登録
        $this->Ecopoint->begin();
        //   更新者データを埋め込む
        $this->data['User'] = $this->loginuser['User'];
        $this->data['Ecopoint']['modified_user_id'] = $this->data['User']['userid'];
        //   登録データを組み立てる
        $storeData = $this->Ecopoint->arrangeData($this->data);
        //   保存
        $this->Ecopoint->create();
        if ($this->Ecopoint->save($storeData, false)) {
            // idが無い場合はinsertなので、追加されたidをセット
            if (empty($storeData['Ecopoint']['id'])) {
                $storeData['Ecopoint']['id'] = $this->Ecopoint->getLastInsertID();
        		  $this->Ecopoint->save($storeData, false);
            }
          $this->Session->setFlash('データベースの更新に成功しました。', 'default', array('class' => 'message_success'));
        } else {
            // rollaback
            $this->Ecopoint->rollback();
            $this->Session->setFlash('データベースの更新に失敗しました。', 'default', array('class' => 'message_failed'));
            $this->redirect('/admin/ecopointentries/index');
        }
        // commit
        $this->Ecopoint->commit();

        // 画像の保存
        if (!isset($this->data['Ecopoint']['image_delete']) && $this->data['Ecopoint']['image']['error'] == 0) {
            // アップロードされている
            //   アップロード先を取得
            $imgPath = $this->Ecopoint->imagePath($storeData);
            
            if (move_uploaded_file($this->data['Ecopoint']['image']['tmp_name'], $imgPath['filepath'])) {
            	chmod($imgPath['filepath'], 0666);
            } else {
            	$this->Session->setFlash('画像ファイルのアップロードに失敗しました。', 'default', array('class' => 'message_failed'));
              $this->redirect('/admin/ecopointentries/index');
            }
        } elseif(isset($this->data['Ecopoint']['image_delete']) && $this->data['Ecopoint']['image_delete'] == 1) {
            // 画像の「削除する」にチェックされているので、画像ファイルを削除する
            //   アップロード先を取得
            $imgPath = $this->Ecopoint->imagePath($storeData);
            //   画像を削除
            foreach ($imgPath as $path) {
                if (file_exists($path)) {
                    unlink($path);
                }
            }
        }

       $this->redirect('/admin/ecopointentries/edit/'.$storeData['Ecopoint']['id']);
    }

    function admin_delete(){

    	// 画像の削除
    	$storeData = $this->Ecopoint->arrangeData($this->data);
		$imgPath = $this->Ecopoint->imagePath($storeData);

    }
    /**
     * エコポイント基本情報登録ページのレンダリング(入力エラー後に遷移したりするので、ここだけ分けました。)
     * @return void
     */
    function _admin_index_rendering($edit = null){
        if (is_null($edit)) {
            $this->set('title_for_layout', sprintf(C_TITLE, 'エコポイント登録'));
        } else {
            $this->set('title_for_layout', sprintf(C_TITLE, 'エコポイント更新'));
        }
        $this->render('/ecopointentries/admin_index');
    }

}