<?php
class StoreentriesController extends AppController {
    var $name       = 'Storeentries';
    var $uses       = array('Store');
    var $helpers    = array();
    var $paginate;

    var $loginuser;

    function beforeFilter() {
        parent::beforeFilter();
        $auth = $this->Auth->user();
        if (!$auth) {
            // ログインしていない
            $this->redirect('/admin/logins/index');
        }
        $this->loginuser = $auth;
        $this->set('loginuser', $this->loginuser);
    }

    /**
      * 運営店舗登録(新規)
     * @return void
     */
    function admin_index($storetype=null){
        // 初期アクセス用の入力値をセット
        $defaultData = $this->Store->defaultStoreData($storetype);
        $this->data  = Set::merge($this->data, $defaultData);

        // 前のページのページ番号をセット
        $this->set('prev_page_no', 1);

        $this->_admin_index_rendering();
    }

    /**
      * 運営店舗登録(編集)
     * @return void
     */
    function admin_edit($id = null){
        if (is_null($id)) {
            $this->redirect('/admin/storesorts/index');
        }
        // 初期アクセス用の入力値をセット
        $editData   = $this->Store->editStoreData($id);
        $this->data = Set::merge($this->data, $editData);

        // 前のページのページ番号をセット
        $page = $this->Session->read('Storelist.page');
        $this->set('prev_page_no', $page);

        $this->_admin_index_rendering($id);
    }

    /**
     * 保存
     * @return void
     */
    function admin_save(){
        // 前のページのページ番号をセット
        $page = $this->Session->read('Storelist.page');
        if (is_null($page)) {
            $page = 1;
        }
        $this->set('prev_page_no', $page);

		 // 入力チェック
		$this->Store->set($this->data);
		$valid = $this->Store->validates();
		// エラーがあれば、入力ページを再表示
		if (!$valid) {
		   $validError = $this->Store->validationErrors;
		   
		   $error['Store'] = $validError;
			
		  $this->set('validErrors', $error);
		  return $this->_admin_index_rendering();
		}
		
		// DB登録
		$this->Store->begin();
		//	 更新者データを埋め込む
		$this->data['User'] = $this->loginuser['User'];
		$this->data['Store']['modified_user_id'] = $this->data['User']['userid'];
		//	 登録データを組み立てる
		$storeData = $this->Store->arrangeData($this->data);
		//	 保存
		$this->Store->create();
		if ($this->Store->save($storeData, false)) {
			$this->Session->setFlash('データベースの更新に成功しました。', 'default', array('class' => 'message_success'));
			// idが無い場合はinsertなので、追加されたidをセット
			if (empty($storeData['Store']['id'])) {
				$storeData['Store']['id'] = $this->Store->getLastInsertID();
			}
		} else {
			// rollaback
			$this->Store->rollback();
			$this->Session->setFlash('データベースの更新に失敗しました。', 'default', array('class' => 'message_failed'));
			$this->redirect('/admin/storeentries/index');
		}
		// commit
		$this->Store->commit();
		
		$this->redirect('/admin/storeentries/edit/'.$storeData['Store']['id']);

    }

    /**
      * 削除
     * @return void
     */
    function admin_delete($id=0){

    	$this->Store->begin();
        if ($this->Store->resetDisplayOrder($id) && $this->Store->delete($id)) {
        	$this->Session->setFlash('データの削除に成功しました。', 'default', array('class' => 'message_success'));
        } else {
        	$this->Store->rollback();
        	$this->Session->setFlash('データの削除に失敗しました。', 'default', array('class' => 'message_failed'));
        }

       // commit
       $this->Store->commit();

       $this->redirect('/admin/storesorts/index');
    	
    }
    /**
     * 運営店舗登録ページのレンダリング(入力エラー後に遷移したりするので、ここだけ分けました。)
     * @return void
     */
    function _admin_index_rendering($edit = null){
        if (is_null($edit)) {
            $this->set('title_for_layout', sprintf(C_TITLE, '運営店舗登録'));
        } else {
            $this->set('title_for_layout', sprintf(C_TITLE, '運営店舗更新'));
        }
        $this->render('/storeentries/admin_index');
    }

}