<?php

// PDF系
Configure::write('Pdf.pdf_url', '/econos_files/pdf');
Configure::write('Pdf.pdf_dir', dirname(dirname(dirname(dirname(__FILE__)))).DS.'econos_files'.DS.'pdf');

// PDF系 IR管理のPDFファイルフォルダ
Configure::write('Pdf.investorrel_dir', 'investorrel');
// PDF系 ニュース・インフォメーション管理のPDFファイルフォルダ
Configure::write('Pdf.info_dir', 'info');

// 画像系
Configure::write('Image.img_url', '/econos_files/img');
Configure::write('Image.img_dir', dirname(dirname(dirname(dirname(__FILE__)))).DS.'econos_files'.DS.'img');

// 画像系 エコポイントの画像ファイルフォルダ
Configure::write('Image.ecopoint_dir', 'ecopoint');

