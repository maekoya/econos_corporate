<?php
// EUCです。
function getcode($string) {
    $spl = explode(':', $string);
    return $spl[0];
}

function getname($string) {
    $spl = explode(':', $string);
    return isset($spl[1]) ? $spl[1] : $string;
}

function fixStatus($string) {
    if ($string == 'MUKO') {
        return '無効';
    }
    if ($string == 'YUKO') {
        return '';
    }
    return $string;
}

function fixInfoKbn($string) {
	if ($string == 'KEISAI') {
		return '掲載情報';
	} else if ($string == 'KAISYA') {
		return '会社情報';
	} else if ($string == 'IRKANREN') {
		return 'ＩＲ関連';
	}
	return $string;
}

function fixCategory($string) {
	if ($string == 'KEIEI') {
		return '経営の特徴と戦略';
	} else if ($string == 'GYOSEKI') {
		return '業績・財務情報';
	} else if ($string == 'IRKANREN') {
		return 'ＩＲ関連資料';
	}
	return $string;
}

