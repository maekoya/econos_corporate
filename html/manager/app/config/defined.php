<?php
// タイトル
define('C_TITLE', '<%s> | 株式会社エコノスCMS');
// author
define('C_AUTHOR', '');
// Description
define('C_DESCRIPTION', '');
// フッター
define('C_COPYRIGHT', 'Copyright &copy; 2013 econos Co.,Ltd. All rights reserved.');

// 表示件数
define('C_LIST_NUMBER_BASE', 30);
Configure::write('ListNumber', array(
    'infolist'           => C_LIST_NUMBER_BASE,
    'investorrellist'    => C_LIST_NUMBER_BASE,
));

// PDF系
Configure::write('Pdf', array(
		'upload_limitsize' => 1024*1024*5, // アップロードMAX容量
));

// 画像系
Configure::write('Image', array(
    'upload_limitsize' => 1024*1024*5, // アップロードMAX容量
    'image_w'          => 225, // リサイズ後の画像横幅(縦幅は横の縮小率にあわせる)
    'image_thumb_w'    => 98, // リサイズ後の画像横幅(縦幅は横の縮小率にあわせる)
));

// 郵便番号チェック用
define('POSTAL_REGEXP','/^[0-9\-]*$/i');
// TELチェック用
define('TEL_REGEXP','/^[0-9\-]*$/i');
