<?php

class Constant {

    // ステータス
    static public $status = array(
        'YUKO' => '有効',
        'MUKO' => '無効',
    );

    // 情報区分
    static public $infokbn = array(
    		'KEISAI' => '掲載情報',
    		'KAISHA' => '会社情報',
    		'IR'     => 'ＩＲ情報',
    );

    // リンク区分
    static public $linkkbn = array(
    		'PDF' => 'PDF',
    		'URL' => 'URL',
    		'NON' => 'なし',
    );

    // 店舗タイプ
    static public $storetype_s = array(
    		'BookOFF'    => 'BOOK OFF',
    		'HardOFF'    => 'HARD OFF',
    		'OFFHouse'   => 'OFF HOUSE',
    		'HobbyOFF'   => 'Hobby OFF',
    		'GarageOFF'  => 'Garage OFF',
    );

    // カテゴリ
    static public $category = array(
    		'01KEIEI' => '経営の特徴と戦略',
    		'02GYOSEKI' => '業績・財務情報',
    		'03IRKANREN' => 'ＩＲ関連資料',
    );

}
?>