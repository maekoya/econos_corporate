
<?php echo $javascript->link('jquery.submitconfirm.js'); ?>
<?php if (!empty($this->data['Investorrel']['id'])) { ?>
<h1>ＩＲ更新</h1>
<?php } else { ?>
<h1>ＩＲ登録</h1>
<?php } ?>
<div class="span-20 last">
	<?php echo $this->Session->flash(); ?>
	<?php echo $form->create('Investorrel', array('url'=>'/admin/investorrelentries/save', 'enctype' => 'multipart/form-data', 'id'=>'registForm')); ?>
		<?php echo $form->hidden('id'); ?>
		<table class="econosinfo">
			<tr>
<?php
// ラジオボタンの形式を変更
$tmp = $form->Html->tags['radio'];
$form->Html->tags['radio'] = '<div class="radiolist"><input type="radio" name="%1$s" id="%2$s" %3$s /><label for="%2$s">%4$s</label><span id="span%4$s" class=""></span></div>';
?>
			<th>ステータス</th>
				<td class="radio-l3o">
<?php $key = 'status'; ?>
<?php if (isset($validErrors['Investorrel'][$key])) { ?>
					<div class="valid-error">
						<p><?php echo $validErrors['Investorrel'][$key]; ?></p>
					</div>
<?php } ?>
					<?php echo $form->radio($key, Constant::$status, array('legend'=>false, 'label'=>false, 'class'=>'typeradio', '$value'=>$this->data['Investorrel']['status'])); ?>
				</td>
			</tr>

			<tr>
				<th>カテゴリ（※）</th>
				<td class="radio-l3o">
<?php $key = 'category'; ?>
<?php if (isset($validErrors['Investorrel'][$key])) { ?>
					<div class="valid-error">
						<p><?php echo $validErrors['Investorrel'][$key]; ?></p>
					</div>
<?php } ?>
				<?php echo $form->radio($key, Constant::$category, array('legend'=>false, 'label'=>false, 'class'=>'typeradio', '$value'=>$this->data['Investorrel']['category'])); ?>
				</td>
			</tr>

			<tr>
				<th>タイトル（※）</th>
				<td>
<?php $key = 'title'; ?>
<?php if (isset($validErrors['Investorrel'][$key])) { ?>
					<div class="valid-error">
						<p><?php echo $validErrors['Investorrel'][$key]; ?></p>
					</div>
<?php } ?>
					<?php echo $form->text($key); ?>
				</td>
			</tr>

			<tr>
				<th>リンク</th>
				<td>
<?php $key = 'linkkbn'; ?>
<?php if (isset($validErrors['Investorrel'][$key])) { ?>
					<div class="valid-error">
						<p><?php echo $validErrors['Investorrel'][$key]; ?></p>
					</div>
<?php } ?>
<?php $key = 'pdf'; ?>
<?php if (isset($validErrors['Investorrel'][$key])) { ?>
					<div class="valid-error">
						<p><?php echo $validErrors['Investorrel'][$key]; ?></p>
					</div>
<?php } ?>
<?php echo $form->hidden('pdf_tmp'); ?>
<?php $key = 'url'; ?>
<?php if (isset($validErrors['Investorrel'][$key])) { ?>
					<div class="valid-error">
						<p><?php echo $validErrors['Investorrel'][$key]; ?></p>
					</div>
<?php } ?>
				<table class="econoslinkkbn">
				<tr>
				<td class="td-linkkbn" rowspan="3" nowrap>
<?php $key = 'linkkbn'; ?>
<?php echo $form->radio($key, Constant::$linkkbn, array('legend'=>false, 'label'=>false, 'class'=>'typeradio', 'value'=>$this->data['Investorrel']['linkkbn'])); ?>
				</td>
				<td class="td-file">
				
<?php $key = 'pdf'; ?>
				<?php echo $form->file($key); ?>
				</td>
				<td class="td-look">
				<div align="center">
<?php if (strlen($this->data['Investorrel']['pdf_tmp']) > 0) { ?>
				<?php echo $form->button('見る', array('type'=>'button', 'onclick'=>'var w=window.open(\''.Configure::read('Pdf.pdf_url').DS.Configure::read('Pdf.investorrel_dir').DS.$this->data['Investorrel']['pdf_tmp'].'\')')); ?>
<?php } ?>
				</div>
				</td>
				<td class="td-delete">
<?php if (strlen($this->data['Investorrel']['pdf_tmp']) > 0) { ?>
				<?php echo $form->submit('削除', array('type'=>'submit', 'name'=>'delete', 'id'=>'delete')); ?>
<?php } ?>
				</td>
				</tr>
				<tr>
					<td colspan="5" class="td-url">
<?php $key = 'url'; ?>
<?php echo $form->text($key); ?>
					</td>
				</tr>
				<tr>
					<td colspan="5" class="td-non">
					&nbsp;</td>
				</tr>
				</table>
				</td>
			</tr>
		</table>
		<p class="btn-area">
<?php if (!empty($this->data['Investorrel']['id'])) { ?>
			&nbsp;<?php echo $form->button('更新', array('type'=>'submit', 'class'=>'submit-btn', 'id'=>'regist')); ?>
<?php } else { ?>
			&nbsp;<?php echo $form->button('登録', array('type'=>'submit', 'class'=>'submit-btn', 'id'=>'regist')); ?>
<?php } ?>
		<?php echo $form->button('戻る', array('type'=>'button', 'class'=>'submit-btn', 'onclick'=>'location.href=\''.$this->webroot.'admin/investorrellists/paging/page:'.$prev_page_no.'\'; return false')); ?>

		</p>
	<?php echo $form->end(); ?>
</div>