<h1>ログイン</h1>
<div class="span-18 prefix-3 last">
	<?php echo $this->Session->flash(); ?>
	<?php echo $form->create('User', array('url'=>'/admin/logins/login', 'action' => 'login')); ?>
		<div class="span-18 content_area">
			<table class="centering">
				<tr>
					<th class="black login_th">管理者ID</th>
					<td class="login_td">
						<?php echo $form->text('userid', array('class'=>'login_input')); ?>
					</td>
				</tr>
				<tr>
					<th class="black login_th">パスワード</th>
					<td class="login_td">
						<?php echo $form->password('passwd', array('class'=>'login_input')); ?>
					</td>
				</tr>
			</table>
		</div>
		<div class="span-18">
			<p class="btn-area">
				<?php echo $form->button('ログイン', array('type'=>'submit')); ?>
			</p>
		</div>
	<?php echo $form->end(); ?>
</div>