
<?php echo $javascript->link('jquery.submitconfirm.js'); ?>
<?php if (!empty($this->data['Hiring']['id'])) { ?>
<h1>採用条件更新</h1>
<?php } else { ?>
<h1>採用条件登録</h1>
<?php } ?>
<div class="span-20 last">
	<?php echo $this->Session->flash(); ?>
	<?php echo $form->create('Hiring', array('url'=>'/admin/hiringentries/save', 'enctype' => 'multipart/form-data', 'id'=>'registForm')); ?>
		<?php echo $form->hidden('id'); ?>
		<table class="econosinfo">
			<tr>
<?php
// ラジオボタンの形式を変更
$tmp = $form->Html->tags['radio'];
$form->Html->tags['radio'] = '<div class="radiolist"><input type="radio" name="%1$s" id="%2$s" %3$s /><label for="%2$s">%4$s</label></div>';
?>
			<th>ステータス</th>
				<td class="radio-l2o">
<?php $key = 'status'; ?>
<?php if (isset($validErrors['Hiring'][$key])) { ?>
					<div class="valid-error">
						<p><?php echo $validErrors['Hiring'][$key]; ?></p>
					</div>
<?php } ?>
					<?php echo $form->radio($key, Constant::$status, array('legend'=>false, 'label'=>false, 'class'=>'typeradio', '$value'=>$this->data['Hiring']['status'])); ?>
				</td>
			</tr>

			<tr>
				<th>掲載期間（開始）（※）</th>
				<td class="ymd">
<?php $key = 'date_start'; ?>
<?php if (isset($validErrors['Hiring'][$key])) { ?>
					<div class="valid-error">
						<p><?php echo $validErrors['Hiring'][$key]; ?></p>
					</div>
<?php } ?>
<?php  if (isset($validErrors['Hiring'][$key])) { 
							echo $form->text($key, array('class'=>'ymd-text'));
		} else {
							echo $form->dateText($key, array('class'=>'ymd-text'));
		}
?>
					&nbsp;(半角数字8桁  例：20130401)
				</td>
			</tr>

			<tr>
				<th>掲載期間（終了）</th>
				<td class="ymd">
<?php $key = 'date_end'; ?>
<?php if (isset($validErrors['Hiring'][$key])) { ?>
					<div class="valid-error">
						<p><?php echo $validErrors['Hiring'][$key]; ?></p>
					</div>
<?php } ?>
<?php  if (isset($validErrors['Hiring'][$key])) { 
							echo $form->text($key, array('class'=>'ymd-text'));
		} else {
							echo $form->dateText($key, array('class'=>'ymd-text'));
		}
?>
					&nbsp;(半角数字8桁  例：20130601)
				</td>
			</tr>

			<tr>
				<th>求人タイトル（※）</th>
				<td>
<?php $key = 'title'; ?>
<?php if (isset($validErrors['Hiring'][$key])) { ?>
					<div class="valid-error">
						<p><?php echo $validErrors['Hiring'][$key]; ?></p>
					</div>
<?php } ?>
					<?php echo $form->text($key); ?>
				</td>
			</tr>

<?php for ($i = 0 ; $i < 15 ; $i++) { ?>
<?php $key = 'Jobcontent.'.$i.'.id'; ?>
<?php echo $form->hidden($key); ?>
<?php $key = 'Jobcontent.'.$i.'.hiring_id'; ?>
<?php echo $form->hidden($key); ?>
<?php $key = 'Jobcontent.'.$i.'.contents_index'; ?>
<?php echo $form->hidden($key, array('value'=>$i)); ?>

			<tr class="hiring">
				<th class="hiring_title">求人内容<?php echo $i+1 ; echo ($i==0) ? '（※）' : '' ?></th>
				<td class="hiring_title">
<?php if (isset($validErrors['Jobcontent'][$i]['title'])) { ?>
					<div class="valid-error">
						<p><?php echo $validErrors['Jobcontent'][$i]['title']; ?></p>
					</div>
<?php } ?>
<?php if (isset($validErrors['Jobcontent'][$i]['contents'])) { ?>
					<div class="valid-error">
						<p><?php echo $validErrors['Jobcontent'][$i]['contents']; ?></p>
					</div>
<?php } ?>
<?php $key = 'Jobcontent.'.$i.'.title'; ?>
<?php echo $form->input($key, array('type'=>'text', 'label'=>'項目名', 'div'=>false)); ?>
			</td>
		</tr>

		<tr>
			<th></th>
			<td class="hiring_content">
<?php $key = 'Jobcontent.'.$i.'.contents'; ?>
				<?php echo $form->input($key, array('type'=>'textarea', 'label'=>'内容', 'div'=>'hiring_content')); ?>
			</td>
		</tr>

<?php } ?>

		</table>
		<p class="btn-area">
<?php if (!empty($this->data['Hiring']['id'])) { ?>
			&nbsp;<?php echo $form->button('更新', array('type'=>'submit', 'class'=>'submit-btn', 'id'=>'regist')); ?>
<?php } else { ?>
			&nbsp;<?php echo $form->button('登録', array('type'=>'submit', 'class'=>'submit-btn', 'id'=>'regist')); ?>
<?php } ?>
		<?php echo $form->button('戻る', array('type'=>'button', 'class'=>'submit-btn', 'onclick'=>'location.href=\''.$this->webroot.'admin/hiringlists/index'.'\'; return false')); ?>

		</p>
	<?php echo $form->end(); ?>
</div>