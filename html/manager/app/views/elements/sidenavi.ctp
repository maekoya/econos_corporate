<?php
$active = '';
//echo $this->name.'<br />'.$this->action;
switch($this->name) {
    case 'Infolists':
        $active = 'info';
        break;
    case 'Hiringlists':
        $active = 'hiring';
        break;
        break;
    case 'Storesorts':
        $active = 'store';
        break;
    case 'Ecopointsorts':
        $active = 'ecopoint';
        break;
    case 'Investorrellists':
        $active = 'investorrel';
        break;
}
?>
				<div class="sidenavi">
					<div class="sidenavi-header">
						MENU
					</div>
					<ul>
						<li><a href="<?php echo $this->webroot; ?>admin/infolists/index" class="navi-button<?php echo ($active == 'info' ? ' active':'') ?>">ニュース管理</a></li>
						<li><a href="<?php echo $this->webroot; ?>admin/hiringlists/index" class="navi-button<?php echo ($active == 'hiring' ? ' active':'') ?>">採用条件管理</a></li>
						<li><a href="<?php echo $this->webroot; ?>admin/storesorts/index" class="navi-button<?php echo ($active == 'store' ? ' active':'') ?>">運営店舗管理</a></li>
						<li><a href="<?php echo $this->webroot; ?>admin/ecopointsorts/index" class="navi-button<?php echo ($active == 'ecopoint' ? ' active':'') ?>">エコポイント管理</a></li>
						<li><a href="<?php echo $this->webroot; ?>admin/investorrellists/index" class="navi-button<?php echo ($active == 'investorrel' ? ' active':'') ?>">IR管理</a></li>
						<li><a href="<?php echo $this->webroot; ?>admin/logins/logout" class="navi-button">ログアウト</a></li>
					</ul>
				</div>
