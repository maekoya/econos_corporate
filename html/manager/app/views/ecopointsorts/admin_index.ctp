
<?php echo $javascript->link('jquery.clickconfirm.js'); ?>
<h1>エコポイント管理</h1>
<div class="span-0 last">
<div class="econoslist-area span-0 last">
<?php echo $this->Session->flash(); ?>
<table class="econoslist_borderless">
	<tr>
		<td class="paging">
		</td>
		<td class="create_btn" align="right">
			<a href="<?php echo $this->webroot; ?>admin/ecopointentries/index/" class="control-button">新規情報追加</a>
		</td>
	</tr>
</table>
<?php $onDataCount = count($ecopoints); ?>
<?php $onData = ($onDataCount > 0); ?>
<table class="econoslist">
			<tr>
				<th class="th-num"></th>
				<th class="th-status">状態</th>
				<th class="th-goodscd">商品コード</th>
				<th class="th-goodsname">商品名</th>
				<th class="th-point">交換PT</th>
				<th colspan="2" class="th-displayorder">表示順</th>
				<th class="th-edit_del">操作</th>
				</tr>
<?php $row = 0; ?>
<?php foreach ($ecopoints as $ecopointData) { ?>
<?php   $row++; ?>
<?php   $bgColor = ($ecopointData['Ecopoint']['status'] == 'YUKO' ? 'bgColor01' : 'bgColor02'); ?>
			<tr class="<?php echo $bgColor; ?>">
				<td class="center"><?php echo h($row); ?></td>
				<td class="center"><?php echo fixStatus($ecopointData['Ecopoint']['status']); ?></td>
				<td class="right"><?php echo $ecopointData['Ecopoint']['goodscd']; ?></td>
				<td class="left"><?php echo $ecopointData['Ecopoint']['goodsname']; ?></td>
				<td class="right"><?php echo number_format($ecopointData['Ecopoint']['point']).' pt'; ?>&nbsp;</td>

<?php 
    $sortPermission = true;
?>
				<td class="center td-sort">
<?php if ($sortPermission && $row > 1) { ?>
					<a href="<?php echo $this->webroot; ?>admin/ecopointsorts/set/1u/<?php echo $ecopointData['Ecopoint']['id']; ?>">△</a>
<?php } else { ?>
					&nbsp;
<?php } ?>
				</td>
				<td class="center td-sort">
<?php if ($sortPermission && $row < $onDataCount) { ?>
					<a href="<?php echo $this->webroot; ?>admin/ecopointsorts/set/1d/<?php echo $ecopointData['Ecopoint']['id']; ?>">▽</a>
<?php } else { ?>
					&nbsp;
<?php } ?>
				</td>
				<td class="center" nowrap>
					<a href="<?php echo $this->webroot; ?>admin/ecopointentries/edit/<?php echo $ecopointData['Ecopoint']['id']; ?>" class="control-button">編集</a>
					<a href="<?php echo $this->webroot; ?>admin/ecopointsorts/delete/<?php echo $ecopointData['Ecopoint']['id']; ?>" class="control-button" id="record_del">削除</a>
				</td>
			</tr>
<?php } ?>

		</table>
	</div>
</div>