
<?php echo $javascript->link('jquery.clickconfirm.js'); ?>
<h1>ＩＲ管理</h1>
<div class="span-0 last">
<div class="econoslist-area span-0 last">
<?php echo $this->Session->flash(); ?>
	<table class="econoslist_borderless">
		<tr>
			<td class="paging">
<?php 
$paginator->options(array(
        'url' => array(
            'controller' => 'investorrellists',
            'action' => 'paging'
        )
));
?>
<p>
<?php if ($paginator->counter(array('format' => '%pages%')) > 1) { ?>
			<?php echo $paginator->first('<<'); ?>&nbsp;
			<?php echo $paginator->prev('<'); ?>&nbsp;
			<?php echo $paginator->numbers(); ?>&nbsp;
			<?php echo $paginator->next('>'); ?>&nbsp;
			<?php echo $paginator->last('>>'); ?>&nbsp;&nbsp;
			<?php echo $paginator->counter(array('format' => '（%page%ページ／%pages%ページ  件数：%count%件）')); ?>
<?php } else { ?>
			<?php echo $paginator->counter(array('format' => '（1ページ／%pages%ページ  件数：%count%件）')); ?>
<?php } ?>
</p>
		</td>
		<td class="create_btn" align="right">
			<a href="<?php echo $this->webroot; ?>admin/investorrelentries/index/" class="control-button">新規情報追加</a>
		</td>
		</tr>
	</table>
</div>
<div class="econoslist-area span-0 last">
		<table class="econoslist">
			<tr>
				<th class="th-num"></th>
				<th class="th-status">状態</th>
				<th class="th-date">登録日付</th>
				<th class="th-category">カテゴリ</th>
				<th class="th-investorrel_title">内容</th>
				<th class="th-edit_del">操作</th>
				</tr>
<?php $row = Configure::read('ListNumber.investorrellist') * ($paginator->current() - 1); ?>
<?php foreach ($investorrels as $investorrelData) { ?>
<?php   $row++; ?>
<?php   $bgColor = ($investorrelData['Investorrel']['status'] == 'YUKO' ? 'bgColor01' : 'bgColor02'); ?>
<tr class="<?php echo $bgColor; ?>">
				<td class="center"><?php echo h($row); ?></td>
				<td class="center"><?php echo fixStatus($investorrelData['Investorrel']['status']); ?></td>
				<td class="center"><?php echo $html->dateFormat($investorrelData['Investorrel']['created']); ?></td>
				<td class="center"><?php echo fixCategory(Constant::$category[$investorrelData['Investorrel']['category']]) ?></td>

				<td class="left">

<?php if ($investorrelData['Investorrel']['pdf'] == 1) { ?>
	<a href="javascript:void(0)" onclick="var w=window.open('<?php echo Configure::read('Pdf.pdf_url'). DS .Configure::read('Pdf.investorrel_dir') .DS . sprintf('%011d', $investorrelData['Investorrel']['id']) . '.pdf'; ?>');" ><?php echo nl2br(h($investorrelData['Investorrel']['title'])); ?><img src="<?php echo $this->webroot; ?>img/pdf-icon.png"></a>
<?php } else if ($investorrelData['Investorrel']['linkkbn'] == 'URL') { ?>
	<a href="javascript:void(0)" onclick="var w=window.open('<?php echo $investorrelData['Investorrel']['url'] ?>');" ><?php echo $investorrelData['Investorrel']['title'] ?><img src="<?php echo $this->webroot; ?>img/url-icon.png"></a>
<?php } else { ?>
	<?php echo $investorrelData['Investorrel']['title'] ?>
<?php } ?>
				</td>
				<td class="center" nowrap>
					<a href="<?php echo $this->webroot; ?>admin/investorrelentries/edit/<?php echo $investorrelData['Investorrel']['id']; ?>" class="control-button">編集</a>
					<a href="<?php echo $this->webroot; ?>admin/investorrellists/delete/<?php echo $investorrelData['Investorrel']['id']; ?>" class="control-button" id="record_del">削除</a>
				</td>
			</tr>
<?php } ?>

		</table>
		<p>
<?php if ($paginator->counter(array('format' => '%pages%')) > 1) { ?>
			<?php echo $paginator->first('<<'); ?>&nbsp;
			<?php echo $paginator->prev('<'); ?>&nbsp;
			<?php echo $paginator->numbers(); ?>&nbsp;
			<?php echo $paginator->next('>'); ?>&nbsp;
			<?php echo $paginator->last('>>'); ?>&nbsp;&nbsp;
			<?php echo $paginator->counter(array('format' => '（%page%ページ／%pages%ページ  件数：%count%件）')); ?>
<?php } else { ?>
			<?php echo $paginator->counter(array('format' => '（1ページ／%pages%ページ  件数：%count%件）')); ?>
<?php } ?>
		</p>
	</div>
</div>