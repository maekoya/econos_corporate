
<?php echo $javascript->link('jquery.submitconfirm.js'); ?>
<script type="text/javascript">
</script>
<?php if (!empty($this->data['Ecopoint']['id'])) { ?>
<h1>エコポイント更新</h1>
<?php } else { ?>
<h1>エコポイント登録</h1>
<?php } ?>
<div class="span-20 last">
	<?php echo $this->Session->flash(); ?>
	<?php echo $form->create('Ecopoint', array('url'=>'/admin/ecopointentries/save', 'enctype' => 'multipart/form-data', 'id'=>'registForm')); ?>
		<?php echo $form->hidden('id'); ?>
		<table class="econosinfo">
			<tr>
<?php
// ラジオボタンの形式を変更
$tmp = $form->Html->tags['radio'];
$form->Html->tags['radio'] = '<div class="radiolist"><input type="radio" name="%1$s" id="%2$s" %3$s /><label for="%2$s">%4$s</label></div>';
?>
			<th>ステータス</th>
				<td class="radio-l2o">
<?php $key = 'status'; ?>
<?php if (isset($validErrors['Ecopoint'][$key])) { ?>
					<div class="valid-error">
						<p><?php echo $validErrors['Ecopoint'][$key]; ?></p>
					</div>
<?php } ?>
					<?php echo $form->radio($key, Constant::$status, array('legend'=>false, 'label'=>false, 'class'=>'typeradio', '$value'=>$this->data['Ecopoint']['status'])); ?>
				</td>
			</tr>

			<tr>
				<th>商品コード（※）</th>
				<td class="goodscd">
<?php $key = 'goodscd'; ?>
<?php if (isset($validErrors['Ecopoint'][$key])) { ?>
					<div class="valid-error">
						<p><?php echo $validErrors['Ecopoint'][$key]; ?></p>
					</div>
<?php } ?>
				<?php echo $form->text($key); ?>
				</td>
			</tr>

			<tr>
				<th>商品名（※）</th>
				<td class="radio-l3o">
<?php $key = 'goodsname'; ?>
<?php if (isset($validErrors['Ecopoint'][$key])) { ?>
					<div class="valid-error">
						<p><?php echo $validErrors['Ecopoint'][$key]; ?></p>
					</div>
<?php } ?>
				<?php echo $form->text($key); ?>
				</td>
			</tr>

			<tr>
				<th>交換必要ポイント（※）</th>
				<td class="point">
<?php $key = 'point'; ?>
<?php if (isset($validErrors['Ecopoint'][$key])) { ?>
					<div class="valid-error">
						<p><?php echo $validErrors['Ecopoint'][$key]; ?></p>
					</div>
<?php } ?>
								<?php echo $form->text($key, array('onfocus'=>'deleteComma()', 'onblur'=>'insertComma()')); ?>&nbsp;pt
				</td>
			</tr>

			<tr>
				<th>説明（※）</th>
				<td>
<?php $key = 'explaination'; ?>
<?php if (isset($validErrors['Ecopoint'][$key])) { ?>
					<div class="valid-error">
						<p><?php echo $validErrors['Ecopoint'][$key]; ?></p>
					</div>
<?php } ?>
					<?php echo $form->textarea($key); ?>
				</td>
			</tr>

			<tr>
				<th>画像</th>
				<td class="radio-l3o">
<?php if (strlen($this->data['Ecopoint']['image_tmp']) > 0) { ?>
<img src="<?php echo Configure::read('Image.img_url').DS.Configure::read('Image.ecopoint_dir').DS.$this->data['Ecopoint']['image_tmp'] ?>"/><br>
<?php } ?>
				<?php $key = 'image'; ?>
<?php if (isset($validErrors['Ecopoint'][$key])) { ?>
					<div class="valid-error">
						<p><?php echo $validErrors['Ecopoint'][$key]; ?></p>
					</div>
<?php } ?>
<?php echo $form->file($key); ?>
					<?php echo $form->hidden('image_tmp'); ?>
<?php if (strlen($this->data['Ecopoint']['image_tmp']) > 0) { ?>

<?php echo $form->submit('削除', array('type'=>'submit', 'name'=>'delete', 'id'=>'delete')); ?>

<?php } ?>
				</td>
			</tr>
		</table>
		<p class="btn-area">
<?php if (!empty($this->data['Ecopoint']['id'])) { ?>
			&nbsp;<?php echo $form->button('更新', array('type'=>'submit', 'class'=>'submit-btn', 'id'=>'regist')); ?>
<?php } else { ?>
			&nbsp;<?php echo $form->button('登録', array('type'=>'submit', 'class'=>'submit-btn', 'id'=>'regist')); ?>
<?php } ?>
		<?php echo $form->button('戻る', array('type'=>'button', 'class'=>'submit-btn', 'onclick'=>'location.href=\''.$this->webroot.'admin/ecopointsorts/index'.'\'; return false')); ?>

		</p>
	<?php echo $form->end(); ?>
</div>