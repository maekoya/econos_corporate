
<?php echo $javascript->link('jquery.clickconfirm.js'); ?>
<h1>採用条件管理</h1>
<div class="span-0 last">
<div class="econoslist-area span-0 last">
<?php echo $this->Session->flash(); ?>
	<table class="econoslist_borderless">
		<tr>
			<td class="paging">
			</td>
			<td class="create_btn" align="right">
				<a href="<?php echo $this->webroot; ?>admin/hiringentries/index/" class="control-button">新規情報追加</a>
			</td>
			</tr>
	</table>
</div>
<div class="econoslist-area span-20 last">
<p>
		</p>
		<table class="econoslist">
			<tr>
				<th class="th-num"></th>
				<th class="th-status">状態</th>
				<th class="th-date_start_end">掲載期間</th>
				<th class="th-hiring_title">内容</th>
				<th class="th-edit_del">操作</th>
				</tr>
<?php $row = 0; ?>
<?php foreach ($hirings as $hiringData) { ?>
<?php   $row++; ?>
<?php   $bgColor = ($hiringData['Hiring']['status'] == 'YUKO' ? 'bgColor01' : 'bgColor02'); ?>
<tr class="<?php echo $bgColor; ?>">
				<td class="center"><?php echo h($row); ?></td>
				<td class="center"><?php echo fixStatus($hiringData['Hiring']['status']); ?></td>
				<td class="left"><?php echo $html->dateFormat($hiringData['Hiring']['date_start']); ?>～
				<?php echo $html->dateFormat($hiringData['Hiring']['date_end']); ?></td>
				<td class="left"><?php echo h($hiringData['Hiring']['title']); ?></td>
				<td class="center" nowrap>
					<a href="<?php echo $this->webroot; ?>admin/hiringentries/edit/<?php echo $hiringData['Hiring']['id']; ?>" class="control-button">編集</a>
					<a href="<?php echo $this->webroot; ?>admin/hiringlists/delete/<?php echo $hiringData['Hiring']['id']; ?>" class="control-button" id="record_del">削除</a>
				</td>
			</tr>
<?php } ?>

		</table>
		<p>
		</p>
	</div>
</div>