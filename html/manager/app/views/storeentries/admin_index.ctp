
<?php echo $javascript->link('jquery.submitconfirm.js'); ?>
<?php if (!empty($this->data['Store']['id'])) { ?>
<h1>運営店舗更新</h1>
<?php } else { ?>
<h1>運営店舗登録</h1>
<?php } ?>
<div class="span-20 last">
	<?php echo $this->Session->flash(); ?>
	<?php echo $form->create('Store', array('url'=>'/admin/storeentries/save', 'enctype' => 'multipart/form-data', 'id'=>'registForm')); ?>
		<?php echo $form->hidden('id'); ?>
		<table class="econosinfo">

			<tr>
<?php
// ラジオボタンの形式を変更
$tmp = $form->Html->tags['radio'];
$form->Html->tags['radio'] = '<div class="radiolist"><input type="radio" name="%1$s" id="%2$s" %3$s /><label for="%2$s">%4$s</label></div>';
?>
			<th>店舗種別（※）</th>
				<td>
<?php $key = 'storetype'; ?>
<?php if (isset($validErrors['Store'][$key])) { ?>
					<div class="valid-error">
						<p><?php echo $validErrors['Store'][$key]; ?></p>
					</div>
<?php } ?>
					<?php echo Constant::$storetype_s[$this->data['Store']['storetype']]; ?>
					<?php echo $form->hidden($key, array('value'=>$this->data['Store'][$key])); ?>
				</td>
			</tr>
		
			<tr>
			<th>ステータス（※）</th>
				<td class="radio-l2o">
<?php $key = 'status'; ?>
<?php if (isset($validErrors['Store'][$key])) { ?>
					<div class="valid-error">
						<p><?php echo $validErrors['Store'][$key]; ?></p>
					</div>
<?php } ?>
					<?php echo $form->radio($key, Constant::$status, array('legend'=>false, 'label'=>false, 'class'=>'typeradio', '$value'=>$this->data['Store']['status'])); ?>
				</td>
			</tr>

			<tr>
				<th>店舗名（※）</th>
				<td>
<?php $key = 'name'; ?>
<?php if (isset($validErrors['Store'][$key])) { ?>
					<div class="valid-error">
						<p><?php echo $validErrors['Store'][$key]; ?></p>
					</div>
<?php } ?>
					<?php echo $form->text($key); ?>
				</td>
			</tr>

			<tr>
				<th>URL（※）</th>
				<td>
<?php $key = 'url'; ?>
<?php if (isset($validErrors['Store'][$key])) { ?>
					<div class="valid-error">
						<p><?php echo $validErrors['Store'][$key]; ?></p>
					</div>
<?php } ?>
					<?php echo $form->text($key); ?>
				</td>
			</tr>

			<tr>
				<th>郵便番号（※）</th>
				<td class="postal">
<?php $key = 'postal'; ?>
<?php if (isset($validErrors['Store'][$key])) { ?>
					<div class="valid-error">
						<p><?php echo $validErrors['Store'][$key]; ?></p>
					</div>
<?php } ?>
					<?php echo $form->text($key, array('class'=>'p10')); ?>&nbsp;（半角数字 例：003-0824）
				</td>
			</tr>

			<tr>
				<th>住所（※）</th>
				<td>
<?php $key = 'address'; ?>
<?php if (isset($validErrors['Store'][$key])) { ?>
					<div class="valid-error">
						<p><?php echo $validErrors['Store'][$key]; ?></p>
					</div>
<?php } ?>
					<?php echo $form->text($key); ?>
				</td>
			</tr>

			<tr>
				<th>電話番号（※）</th>
				<td class="tel">
<?php $key = 'tel'; ?>
<?php if (isset($validErrors['Store'][$key])) { ?>
					<div class="valid-error">
						<p><?php echo $validErrors['Store'][$key]; ?></p>
					</div>
<?php } ?>
					<?php echo $form->text($key); ?>&nbsp;（半角数字 例：011-875-1996）
				</td>
			</tr>

			<tr>
				<th>FAX番号（※）</th>
				<td class="fax">
<?php $key = 'fax'; ?>
<?php if (isset($validErrors['Store'][$key])) { ?>
					<div class="valid-error">
						<p><?php echo $validErrors['Store'][$key]; ?></p>
					</div>
<?php } ?>
					<?php echo $form->text($key); ?>&nbsp;（半角数字 例：011-875-1997）
				</td>
			</tr>

		</table>
		<p class="btn-area">
<?php if (!empty($this->data['Store']['id'])) { ?>
			&nbsp;<?php echo $form->button('更新', array('type'=>'submit', 'class'=>'submit-btn', 'id'=>'regist')); ?>
<?php } else { ?>
			&nbsp;<?php echo $form->button('登録', array('type'=>'submit', 'class'=>'submit-btn', 'id'=>'regist')); ?>
<?php } ?>
		<?php echo $form->button('戻る', array('type'=>'button', 'class'=>'submit-btn', 'onclick'=>'location.href=\''.$this->webroot.'admin/storesorts/index/'.$this->data["Store"]["storetype"].'\'; return false')); ?>
		</p>
	<?php echo $form->end(); ?>
</div>