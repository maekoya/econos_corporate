
<?php echo $javascript->link('jquery.clickconfirm.js'); ?>
<h1>ニュース・インフォメーション管理</h1>
<div class="span-0 last">
<div class="econoslist-area span-0 last">
<?php echo $this->Session->flash(); ?>
	<table class="econoslist_borderless">
		<tr>
			<td class="paging">
<?php 
$paginator->options(array(
        'url' => array(
            'controller' => 'infolists',
            'action' => 'paging'
        )
));
?>
<p>
<?php if ($paginator->counter(array('format' => '%pages%')) > 1) { ?>
			<?php echo $paginator->first('<<'); ?>&nbsp;
			<?php echo $paginator->prev('<'); ?>&nbsp;
			<?php echo $paginator->numbers(); ?>&nbsp;
			<?php echo $paginator->next('>'); ?>&nbsp;
			<?php echo $paginator->last('>>'); ?>&nbsp;&nbsp;
			<?php echo $paginator->counter(array('format' => '（%page%ページ／%pages%ページ  件数：%count%件）')); ?>
<?php } else { ?>
			<?php echo $paginator->counter(array('format' => '（1ページ／%pages%ページ  件数：%count%件）')); ?>
<?php } ?>
</p>
		</td>
		<td class="create_btn" align="right">
				<a href="<?php echo $this->webroot; ?>admin/infoentries/index/" class="control-button">新規情報追加</a>
			</td>
			</tr>
	</table>
</div>
<div class="econoslist-area span-0 last">
		<table class="econoslist">
			<tr>
				<th class="th-num"></th>
				<th class="th-status">状態</th>
				<th class="th-date">日付</th>
				<th class="th-infokbn">区分</th>
				<th class="th-freeword">内容</th>
				<th class="th-edit_del">操作</th>
				</tr>
<?php $row = Configure::read('ListNumber.infolist') * ($paginator->current() - 1); ?>
<?php foreach ($infos as $infoData) { ?>
<?php   $row++; ?>
<?php   $bgColor = ($infoData['Info']['status'] == 'YUKO' ? 'bgColor01' : 'bgColor02'); ?>
<tr class="<?php echo $bgColor; ?>">
				<td class="center"><?php echo h($row); ?></td>
				<td class="center"><?php echo fixStatus($infoData['Info']['status']); ?></td>
				<td class="center"><?php echo $html->dateFormat($infoData['Info']['ymd']); ?></td>
				<td class="center"><?php echo fixInfoKbn(Constant::$infokbn[$infoData['Info']['infokbn']]) ?></td>
				<td class="left">
<?php if ($infoData['Info']['pdf'] == 1) { ?>
	<a href="javascript:void(0)" onclick="var w=window.open('<?php echo Configure::read('Pdf.pdf_url'). DS .Configure::read('Pdf.info_dir') .DS . sprintf('%011d', $infoData['Info']['id']) . '.pdf'; ?>');" ><?php echo nl2br(h($infoData['Info']['freeword'])); ?><img src="<?php echo $this->webroot; ?>img/pdf-icon.png"></a>
<?php } else if ($infoData['Info']['linkkbn'] == 'URL') { ?>
	<a href="javascript:void(0)" onclick="var w=window.open('<?php echo $infoData['Info']['url'] ?>');" ><?php echo nl2br(h($infoData['Info']['freeword'])); ?><img src="<?php echo $this->webroot; ?>img/url-icon.png"></a>
	<?php } else { ?>
	<?php echo $infoData['Info']['freeword'] ?>
<?php } ?>
				</td>
				<td class="center" nowrap>
					<a href="<?php echo $this->webroot; ?>admin/infoentries/edit/<?php echo $infoData['Info']['id']; ?>" class="control-button">編集</a>
					<a href="<?php echo $this->webroot; ?>admin/infolists/delete/<?php echo $infoData['Info']['id']; ?>" class="control-button" id="record_del">削除</a>
				</td>
			</tr>
<?php } ?>

		</table>
		<p>
<?php if ($paginator->counter(array('format' => '%pages%')) > 1) { ?>
			<?php echo $paginator->first('<<'); ?>&nbsp;
			<?php echo $paginator->prev('<'); ?>&nbsp;
			<?php echo $paginator->numbers(); ?>&nbsp;
			<?php echo $paginator->next('>'); ?>&nbsp;
			<?php echo $paginator->last('>>'); ?>&nbsp;&nbsp;
			<?php echo $paginator->counter(array('format' => '（%page%ページ／%pages%ページ  件数：%count%件）')); ?>
<?php } else { ?>
			<?php echo $paginator->counter(array('format' => '（1ページ／%pages%ページ  件数：%count%件）')); ?>
<?php } ?>
		</p>
	</div>
</div>