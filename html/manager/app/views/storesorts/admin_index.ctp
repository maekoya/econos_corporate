
<?php echo $javascript->link('jquery.clickconfirm.js'); ?>
<h1>運営店舗管理</h1>
<div class="span-0 last">
	<div class="hotelnavi-area span-0 last">
<?php echo $this->Session->flash(); ?>
<?php foreach (Constant::$storetype_s as $storetype => $storename_s) { ?>
<?php   $active = ($storetype == $this->data['Store']['storetype']) ? ' active' : '' ?>
		<div class="navi-btn">
			<a href="<?php echo $this->webroot; ?>admin/storesorts/index/<?php echo $storetype; ?>" class="navi-button<?php echo $active; ?>"><?php echo $storename_s; ?></a>
		</div>
<?php } ?>
</div>
<div class="econoslist-area span-20 last">
<table class="econoslist_borderless">
	<tr>
		<td class="paging">
		</td>
		<td class="create_btn" align="right">
<?php foreach (Constant::$storetype_s as $storetype => $storename_s) { ?>
<?php   $stype = ($storetype == $this->data['Store']['storetype']) ? $this->data['Store']['storetype'] : '' ?>
	<?php if (!empty($stype)) { ?> 
			<a href="<?php echo $this->webroot; ?>admin/storeentries/index/<?php echo $stype; ?>" class="control-button">新規店舗追加</a>
	<?php } ?>
<?php } ?>
		</td>
	</tr>
</table>
			
<?php $onDataCount = count($stores); ?>
<?php $onData = ($onDataCount > 0); ?>
<table class="econoslist">
			<tr>
				<th class="th-num"></th>
				<th class="th-status">状態</th>
				<th class="th-store">店舗</th>
				<th class="th-address">住所</th>
				<th class="th-telfax">電話/FAX</th>
				<th colspan="2" class="th-displayorder">表示順</th>
				<th class="th-del">操作</th>
				</tr>
<?php $row = 0; ?>
<?php foreach ($stores as $storeData) { ?>
<?php   $row++; ?>
<?php   $bgColor = ($storeData['Store']['status'] == 'YUKO' ? 'bgColor01' : 'bgColor02'); ?>
			<tr class="<?php echo $bgColor; ?>">
				<td class="center"><?php echo h($row); ?></td>
				<td class="center"><?php echo fixStatus($storeData['Store']['status']); ?></td>
				<td class="left">
					<a href="<?php echo $this->webroot; ?>admin/storeentries/edit/<?php echo $storeData['Store']['id']; ?>">
						<?php echo $storeData['Store']['name']; ?>
					</a>
				</td>
				<td class="left"><?php echo $storeData['Store']['address']; ?></td>
				<td class="center"><?php echo $storeData['Store']['tel'];?>/<?php echo $storeData['Store']['fax']; ?></td>
<?php 
    $sortPermission = true;
?>
				<td class="center td-sort">
<?php if ($sortPermission && $row > 1) { ?>
					<a href="<?php echo $this->webroot; ?>admin/storesorts/set/1u/<?php echo $storeData['Store']['id']; ?>">△</a>
<?php } else { ?>
					&nbsp;
<?php } ?>
				</td>
				<td class="center td-sort">
<?php if ($sortPermission && $row < $onDataCount) { ?>
					<a href="<?php echo $this->webroot; ?>admin/storesorts/set/1d/<?php echo $storeData['Store']['id']; ?>">▽</a>
<?php } else { ?>
					&nbsp;
<?php } ?>
				</td>
				<td class="center">
					<a href="<?php echo $this->webroot; ?>admin/storeentries/delete/<?php echo $storeData['Store']['id']; ?>" class="control-button" id="record_del">削除</a>
				</td>
			</tr>
<?php } ?>

		</table>
	</div>
</div>