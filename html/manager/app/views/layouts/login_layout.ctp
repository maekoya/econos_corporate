<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<?php echo $this->Html->charset(); ?>
		<title><?php echo $title_for_layout; ?></title>
		<?php echo $this->Html->meta('icon'); ?>
		<meta name="description" content="<?php echo C_DESCRIPTION; ?>">
		<meta name="author" content="<?php echo C_AUTHOR; ?>">
		<?php echo $this->element('js'); ?>
		<?php echo $this->element('css'); ?>
	</head>
	<body>

		<div id="header">
			<div class="container">
				<div class="span-24">
					<?php echo $this->element('header'); ?>
				</div>
			</div>
		</div>
		<div id="content">
			<div class="container">
				<div class="span-24">
					<?php echo $content_for_layout; ?>
				</div>
			</div>
		</div>
		<div id="footer">
			<div class="container">
				<div class="span-24">
					<?php echo $this->element('footer'); ?>
				</div>
			</div>
		</div>

	</body>
</html>
