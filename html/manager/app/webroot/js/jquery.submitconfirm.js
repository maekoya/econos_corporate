(function($) {
	var registForm = '#registForm';

	$(document).ready(function() {
		$('#delete').click(function() {
			if (confirm('データを削除してもよろしいですか？')) {
				return true;
			}
			return false;
		});
		$('#regist').click(function() {
			if (confirm('データを登録してもよろしいですか？')) {
				return true;
			}
			return false;
		});
	});
})(jQuery);
