<?php

class Ecopoint extends AppModel {
    var $useTable  = 'ecopoints';
    var $name      = 'Ecopoint';
    var $hasMany   = array(
    );
    var $fieldlist = array(
        'status',
        'goodscd',
        'goodsname',
        'point',
        'explaination',
        'image',
        'displayorder',
        'modified_user_id',
    );
	var $validate  = array(
		'status' => array(
			'notempty' => array(
				'rule'	   => array('notempty'),
				'message'  => 'ステータスが選択されていません。',
				'required' => true,
				'last'	   => true,
			),
		),
		'goodscd' => array(
				'notempty' => array(
					'rule'	   => array('notempty'),
					'message'  => '商品コードが入力されていません。',
					'required' => true,
					'last'	   => true,
				),
				'maxLength' => array(
					'rule'		 => array('maxLength', 11),
					'message'	 => '商品コードは11文字以内で入力してください。',
					'required'	 => true,
					'allowEmpty' => true,
					'last'		 => true,
				),
				'custom' => array(
					'rule'		 => array('custom', '/^[a-z\d]*$/i'),
					'message'	 => '商品コードは半角英数字で入力してください。',
					'required'	 => true,
					'allowEmpty' => true,
					'last'		 => true,
				),
		),
		'goodsname' => array(
				'notempty' => array(
					'rule'	   => array('notempty'),
					'message'  => '商品名が入力されていません。',
					'required' => true,
					'last'	   => true,
				),
				'maxLength' => array(
					'rule'		 => array('maxLength', 512),
					'message'	 => '商品コードは512文字以内で入力してください。',
					'required'	 => true,
					'allowEmpty' => true,
					'last'		 => true,
				),
			),
			'point' => array(
				'notempty' => array(
					'rule'	   => array('notempty'),
					'message'  => '交換必要ポイントが入力されていません。',
					'required' => true,
					'last'	   => true,
				),
				'maxLength' => array(
					'rule'		 => array('maxLength', 9),
					'message'	 => '交換必要ポイントは9文字以内で入力してください。',
					'required'	 => true,
					'allowEmpty' => true,
					'last'		 => true,
				),
				'validIntegerWithComma' => array(
					'rule'		 => array('validIntegerWithComma', 'point'),
					'message'	 => '交換必要ポイントは半角数字で入力してください。',
					'required'	 => true,
					'allowEmpty' => true,
					'last'		 => true,
				),
			),
		'explaination' => array(
				'notempty' => array(
					'rule'	   => array('notempty'),
					'message'  => '説明が入力されていません。',
					'required' => true,
					'last'	   => true,
				),
				'maxLength' => array(
					'rule'		 => array('maxLength', 20000),
					'message'	 => '説明は20000文字以内で入力してください。',
					'required'	 => true,
					'allowEmpty' => true,
					'last'		 => true,
				),
			),
		'image' => array(
					 'maxLength' => array(
					 		'rule'		 => array('maxLengthFile', 255),
					 		'message'	 => 'ファイル名は255文字以内で入力してください。',
					 		'allowEmpty' => true,
					 		'last'		 => true,
					 ),
					'isFiletype' => array(
							'rule'		 => array('isFiletype', array('image', array('jpg', 'png', 'gif'))),
							'message'	 => 'ファイル名はjpg, png またはgifを指定してください。',
							'allowEmpty' => true,
							'last'		 => true,
					),
				),
    );

    // ==================== コンストラクタ ====================
    /*
     * コンストラクタ
     * @return void
     */
    function Ecopoint() {
        parent::__construct();
    }

    // ==================== validate用function ====================
    /*
     * 数値チェック（半角数字のみ許可）
     * @param array $data 入力値
     * @param array $arg 未使用
     * @return boolean
     */
    function validIntegerWithComma($data, $arg) {
    	 $str = str_replace(',', '', $data[$arg]);
        if (preg_match('/^[0-9]+$/', $str)/* && !preg_match('/^0/', $data[$arg]) ←0スタートをfalseにしたいならば追加する */) {
            return true;
        }
        return false;
    }

    /*
     * 画像の文字数チェック
    * @param array $data 入力値
    * @param array $arg 未使用
    * @return boolean
    */
    function maxLengthFile($data, $arg) {
    	$chk =& Validation::getInstance();
    
    	$checkValue = $this->data['Ecopoint']['image']['name'];
    
    	return $chk->maxLength($checkValue, $arg);
    }
    /*
     * jpgが指定されているかチェック
    * @param array $data 入力値
    * @param array $arg 未使用
    * @return boolean
    */
    function isFiletype($data, $arg) {
    	$checkValue = $this->data['Ecopoint']['image']['name'];

    	if (empty($checkValue))
    		return true;

    	$exp = explode('.', $checkValue);
    	$ext = $exp[count($exp)-1];
    	$ext = strtolower($ext);
    	foreach ($arg[1] as $allowExt) {
    		$c = strtolower($allowExt);
    		if ($c == $ext) {
    			return true;
    		}
    	}
    	return false;
    }

    // ==================== 通常function ====================

    /*
     * 初期状態の取得
     * @return array 初期状態のEcopoint
     */
    function defaultEcopointData() {
        $ret = array(
            'Ecopoint' => array(
                'id'                 => '',          // なし
                'status'             => 'YUKO',      // 有効
                'goodscd'            => '',          // なし
                'goodsname'          => '',          // なし
                'point'              => null,        // なし
                'explaination'       => '',          // なし
                'image'              => '2',         // アップなし
                'image_tmp'          => '',          // なし
            )
        );
        return $ret;
    }

    /*
     * 対象IDの状態を編集
     * @return array Ecopoint
     */
    function editEcopointData($id) {
        $data = $this->findById($id);
        if ($data === false) {
            return false;
        }
        $ret = array(
            'Ecopoint' => array(
                'id'                 => $data['Ecopoint']['id'],
                'status'             => $data['Ecopoint']['status'],
                'goodscd'            => $data['Ecopoint']['goodscd'],
                'goodsname'          => $data['Ecopoint']['goodsname'],
                'point'              => $data['Ecopoint']['point'],
                'explaination'       => $data['Ecopoint']['explaination'],
                'image'              => $data['Ecopoint']['image'],
                'image_tmp'          => '',
            )
        );
       // 画像があるか
        if ($ret['Ecopoint']['image'] == 1) {
            $imgDir = Configure::read('Image.img_dir') . DS
        					. Configure::read('Image.ecopoint_dir') . DS;
            $imgFile = sprintf('%011d', $data['Ecopoint']['id']) . '.jpg';
            $imgPath = $imgDir . $imgFile;
            if (file_exists($imgPath)) {
                $ret['Ecopoint']['image_tmp'] = $imgFile;
            }
        }
        
        return $ret;
    }

    /*
     * 画像のチェック
    * @param array $data 入力値
    * @return string 検索条件
    */
    function uploadImageValidate($data, $key) {
    	$limitsize = Configure::read('Image.upload_limitsize');
    	$ret = array();
    	if (!isset($data['Ecopoint'][$key]) || !isset($data['Ecopoint'][$key]['error'])) {
    		$ret[$key] = '不正なアクセスです。';
    		return $ret;
    	}
    	if ($data['Ecopoint'][$key]['error'] == 4) {
    		return $ret;
    	}
    	if ($data['Ecopoint'][$key]['error'] != 0) {
    		switch ($data['Ecopoint'][$key]['error']) {
    			case 1:
    				$ret[$key] = 'アップロードされたファイルのサイズが、設定ファイルの値を超えています。';
    				return $ret;
    				break;
    			default:
    				$ret[$key] = 'アップロードに失敗しました。';
    				return $ret;
    				break;
    		}
    	}
    	if ($data['Ecopoint'][$key]['size'] > $limitsize) {
    		$p  = array('', 'k', 'M', 'T');
    		$sp = '';
    		$s  = $limitsize;
    		$n  = 0;
    		while ($s >= 1024) {
    			$s = (int)($s / 1024);
    			$n++;
    			if ($n >= 3) {
    				break;
    			}
    		}
    		$size      = $s . $p[$n] . 'バイト';
    		$ret[$key] = $size . '以下のファイルをアップロードしてください。';
    		return $ret;
    	}
    	$eit = exif_imagetype($data['Ecopoint'][$key]['tmp_name']);
    	if ($eit != IMAGETYPE_JPEG && $eit != IMAGETYPE_PNG && $eit != IMAGETYPE_GIF) {
    		$ret[$key] = 'jpg, png またはgifファイルをアップロードしてください。';
    		return $ret;
    	}
    	return $ret;
    }

    /*
     * DB登録用の配列を返却する
     * @param array $data 入力値
     * @return string 検索条件
     */
    function arrangeData($data) {
        $ecopointAry = array('Ecopoint'=>$data['Ecopoint']);

        // エコポイントのカンマ削除
        $ecopointAry['Ecopoint']['point'] = str_replace(",", "", $data['Ecopoint']['point']);

        // 画像のアップロードフラグをセットする
        if (isset($data['Ecopoint']['image']['error']) && $data['Ecopoint']['image']['error'] == 0) {
            $ecopointAry['Ecopoint']['image'] = 1;
        } elseif (isset($data['Ecopoint']['image']['error']) && $data['Ecopoint']['image']['error'] == 4 && 
        		          strlen($data['Ecopoint']['image_tmp']) > 0) {
            $ecopointAry['Ecopoint']['image'] = 1;
        } else {
            $ecopointAry['Ecopoint']['image'] = 2;
        }

        // 新規作成の場合、表示順の番号を与える
        if (empty($ecopointAry['Ecopoint']['id'])) {
        	$ret = $this->getMaxDisplayOrder() + 1;
        	$ecopointAry['Ecopoint']['displayorder'] = $ret;
        }
        
        return $ecopointAry;
    }

    /*
     * 画像パスを返却する
     * @param array $data 入力値
     * @return string 検索条件
     */
    function imagePath($data) {
        if (empty($data['Ecopoint']['id'])) {
            return false;
        }
        $imgDir    = Configure::read('Image.img_dir') . DS . Configure::read('Image.ecopoint_dir');
        $filename  = sprintf('%011d', $data['Ecopoint']['id']) . '.jpg';
        $sfilename = sprintf('%011d', $data['Ecopoint']['id']) . 's.jpg';

        // 画像ディレクトリが存在していない場合、作成する
        if (!file_exists($imgDir . DS)) {
            mkdir($imgDir . DS);
            chmod($imgDir . DS, 0777);
        }

        // 画像パスを返却
        return array(
            'filepath'  => $imgDir . DS . $filename,
            'sfilepath' => $imgDir . DS . $sfilename,
        );
    }

    /*
     * 詳細表示など用に指定IDからニュース・インフォメーション情報を取得
     * @param int $id ID
     * @return array ニュース・インフォメーション情報
     */
    function detailEcopoint($id) {
        return $this->findById($id);
    }

    /*
     * エコポイント並び替え用のデータ取得
    * @param array $data 入力値
    * @return string 検索条件
    */
    function ecopointsortsFind() {
    	$params = array(
    			'order'      => 'Ecopoint.displayorder'
    	);
    	return $this->find('all', $params);
    }
    
    /*
     * エコポイント並び替え
    * @param string $movetype 行動タイプ
    * @param int $id データID
    * @return bool
    */
    function orderUpdate($movetype, $id, $modified_user_id) {
    	switch($movetype) {
    		case '1u':
    			// 一つ上
    			return $this->orderUpdateToOneUp($id, $modified_user_id);
    			break;
    		case '1d':
    			// 一つ下
    			return $this->orderUpdateToOneDown($id, $modified_user_id);
    			break;
    	}
    }
    
    /*
     * エコポイント並び替え(一つ上)
    * @param int $id データID
    * @return bool
    */
    function orderUpdateToOneUp($id, $modified_user_id) {
    	// 引数のIDのデータを取得
    	$target = $this->findById($id);
    	if ($target === false) {
    		// 対象が見つからない
    		return false;
    	}

    	// 入れ替える対象のデータを取得
    	$cond = array(
    			'Ecopoint.displayorder' => $target['Ecopoint']['displayorder'] - 1,
    	);
    	$swap = $this->find('first', array('conditions'=>$cond));
    	if ($swap === false) {
    		// 対象が見つからない（引数データが一番上）
    		return true;
    	}

    	$modified = date('Y-m-d H:i:s');
    	// 引数データのorderをデクリメント
    	$target['Ecopoint']['displayorder']     = $target['Ecopoint']['displayorder'] - 1;
    	$target['Ecopoint']['modified_user_id'] = $modified_user_id;
    	$target['Ecopoint']['modified']         = null;
    	// 入れ替え対象データのorderをインクリメント
    	$swap['Ecopoint']['displayorder']     = $swap['Ecopoint']['displayorder'] + 1;
    	$swap['Ecopoint']['modified_user_id'] = $modified_user_id;
    	$swap['Ecopoint']['modified']         = null;
    
    	$this->create();
    	$ret1 = $this->save($target,false);
    	$this->create();
    	$ret2 = $this->save($swap,false);
    
    	return ($ret1 && $ret2);
    }

    /*
     * エコポイント並び替え(一つ下)
    * @param string $movetype 行動タイプ
    * @param int $id データID
    * @return bool
    */
    function orderUpdateToOneDown($id, $modified_user_id) {
    	// 引数のIDのデータを取得
    	$target = $this->findById($id);
    	if ($target === false) {
    		// 対象が見つからない
    		return false;
    	}

    	// 入れ替える対象のデータを取得
    	$cond = array(
    			'Ecopoint.displayorder' => $target['Ecopoint']['displayorder'] + 1,
    	);
    	$swap = $this->find('first', array('conditions'=>$cond));
    	if ($swap === false) {
    		// 対象が見つからない（引数データが一番上）
    		return true;
    	}

    	$modified = date('Y-m-d H:i:s');
    	// 引数データのorderをデクリメント
    	$target['Ecopoint']['displayorder']     = $target['Ecopoint']['displayorder'] + 1;
    	$target['Ecopoint']['modified_user_id'] = $modified_user_id;
    	$target['Ecopoint']['modified']         = null;
    	// 入れ替え対象データのorderをインクリメント
    	$swap['Ecopoint']['displayorder']     = $swap['Ecopoint']['displayorder'] - 1;
    	$swap['Ecopoint']['modified_user_id'] = $modified_user_id;
    	$swap['Ecopoint']['modified']         = null;
    
    	$this->create();
    	$ret1 = $this->save($target,false);
    	$this->create();
    	$ret2 = $this->save($swap,false);

    	return ($ret1 && $ret2);
    }

    /*
     * displayorderの最大番号を取得する
    * @return int 最大番号
    */
    function getMaxDisplayOrder() {
    
    	$displayorder = $this->getMaxDisplayOrderByKeys();
    
    	if ($displayorder === false) {
    		return 0;
    	} else {
    		return $displayorder['max_id'];
    	}
    }
    
    /*
     * 指定されたキーのデータを取得する（FOR UPDATE）
    * @param int $storetype   キー
    * @return int 最大番号
    */
    function getMaxDisplayOrderByKeys() {
    	$displayorder = $this->find('first', array(
    			"fields" => "MAX(Ecopoint.displayorder) as max_id"
    			)
    	);
    
    	if (count($displayorder) == 0) {
    		return false;
    	} else {
    		$displayorder = $displayorder[0];
    		return $displayorder;
    	}
    }
    /*
     * エコポイント削除後の表示順並び替え
    * @param int $id データID
    * @return bool
    */
    function resetDisplayOrder($id) {
    	$target = $this->findById($id);
		$displayorder = $target['Ecopoint']['displayorder'];
/*
    	$params = array(
    			'Ecopoint.displayorder > ' => $displayorder
    	);
    	$lists = $this->find('all', $params);
*/
    	$lists = $this->find('all');
 
    	foreach ($lists as $key=>$val) {
    		$target = $this->findById($val['Ecopoint']['id']);
    		$target_displayorder = $target['Ecopoint']['displayorder'];
    		if ($target_displayorder <= $displayorder)
    			continue;

    		$target['Ecopoint']['displayorder'] = $target['Ecopoint']['displayorder'] - 1;

    		$this->create();
    		$ret = $this->save($target,false);

    		if ($ret == false) {
    			return false;
    		}
    	}
    	
    	return true;
    }

}
?>