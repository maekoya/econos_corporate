<?php

class Hiring extends AppModel {
    var $useTable  = 'hirings';
    var $name      = 'Hiring';
    var $hasMany   = array(
    		'Jobcontent' => array(
    				'className' => 'Jobcontent',
    				'conditions' => '',
    				'order' => 'contents_index',
    				'foreignKey' => 'hiring_id',
    				'dependent' => true,
    				'exclusive' => false,
    			)
    	    );
    var $fieldlist = array(
        'status',
        'date_start',
        'date_end',
        'title',
        'modified_user_id',
    );
	var $validate  = array(
		'status' => array(
			'notempty' => array(
				'rule'	   => array('notempty'),
				'message'  => 'ステータスが選択されていません。',
				'required' => true,
				'last'	   => true,
			),
		),
		'date_start' => array(
				'minLength' => array(
					'rule'		 => array('minLength', 8),
					'message'	 => '掲載期間（開始）は8文字の半角数字で入力してください。',
					'required'	 => true,
					'allowEmpty' => false,
					'last'		 => true,
				),
				'maxLength' => array(
					'rule'		 => array('maxLength', 8),
					'message'	 => '掲載期間（開始）は8文字の半角数字で入力してください。',
					'required'	 => true,
					'allowEmpty' => true,
					'last'		 => true,
				),
				'validInteger' => array(
					'rule'		 => array('validInteger', 'date_start'),
					'message'	 => '掲載期間（開始）は8文字の半角数字で入力してください。',
					'required'	 => true,
					'allowEmpty' => true,
					'last'		 => true,
				),
				'isDate' => array(
					'rule'		 => array('isDate', array('date_start')),
					'message'	 => '掲載期間（開始）が不正です。正しい日付を入力してください。',
					'required'	 => true,
					'allowEmpty' => true,
					'last'		 => true,
				),
				'isDateBetween' => array(
					'rule'		 => array('isDateBetween', array('date_start')),
					'message'	 => '19700101～20380119までで入力してください。',
					'required'	 => true,
					'allowEmpty' => true,
					'last'		 => true,
				),
		),
		'date_end' => array(
				'minLength' => array(
					'rule'		 => array('minLength', 8),
					'message'	 => '掲載期間（終了）は8文字の半角数字で入力してください。',
					'required'	 => true,
					'allowEmpty' => true,
					'last'		 => true,
				),
				'maxLength' => array(
					'rule'		 => array('maxLength', 8),
					'message'	 => '掲載期間（終了）は8文字の半角数字で入力してください。',
					'required'	 => true,
					'allowEmpty' => true,
					'last'		 => true,
				),
				'validInteger' => array(
					'rule'		 => array('validInteger', 'date_end'),
					'message'	 => '掲載期間（終了）は8文字の半角数字で入力してください。',
					'required'	 => true,
					'allowEmpty' => true,
					'last'		 => true,
				),
				'isDate' => array(
					'rule'		 => array('isDate', array('date_end')),
					'message'	 => '掲載期間（終了）が不正です。正しい日付を入力してください。',
					'required'	 => true,
					'allowEmpty' => true,
					'last'		 => true,
				),
				'isCorrectStartEnd' => array(
					'rule'		 => array('isCorrectStartEnd', array('date_start', 'date_end')),
					'message'	 => '掲載期間（終了）が（開始）より前の日付です。',
					'required'	 => true,
					'allowEmpty' => true,
					'last'		 => true,
				),
				'isDateBetween' => array(
					'rule'		 => array('isDateBetween', array('date_end')),
					'message'	 => '19700101～20380119までで入力してください。',
					'required'	 => true,
					'allowEmpty' => true,
					'last'		 => true,
				),
		),
		'title' => array(
			'notempty' => array(
				'rule'	   => array('notempty'),
				'message'  => '求人タイトルが入力されていません。',
				'required' => true,
				'last'	   => true,
			),
		),
    );

    // ==================== コンストラクタ ====================
    /*
     * コンストラクタ
     * @return void
     */
    function Hiring() {
        parent::__construct();
    }

    // ==================== validate用function ====================
    /*
     * 正しい日付かチェック
    * @param array $data 入力値
    * @param array $arg 専用引数
    * @return boolean
    */
    function isDate($data, $arg) {
    	$val = $this->data['Hiring'][$arg[0]];
    	$y   = substr($val, 0, 4);
    	$m   = substr($val, 4, 2);
    	$d   = substr($val, 6, 2);
    	if (checkdate($m, $d, $y)) {
    		return true;
    	} else {
    		return false;
    	}
    }
    /*
     * 19700101～20380119までかチェック
    * @param array $data 入力値
    * @param array $arg 専用引数
    * @return boolean
    */
    function isDateBetween($data, $arg) {
    	$val = $this->data['Hiring'][$arg[0]];
    	$y   = substr($val, 0, 4);
    	$m   = substr($val, 4, 2);
    	$d   = substr($val, 6, 2);
    	if ($y < 1970 || $y > 2038)
    		return false;
    	if ($y == 2038) {
    		if ($m > 1)
    			return false;
    		if ($d > 19)
    			return false;
    	}
    	return true;
    }
    
    // ==================== validate用function ====================
    /*
     * 掲載期間（終了）が（開始）より前でないかチェック
    * @param array $data 入力値
    * @param array $arg 専用引数
    * @return boolean
    */
    function isCorrectStartEnd($data, $arg) {
    	$start = $this->data['Hiring'][$arg[0]];
    	$end = $this->data['Hiring'][$arg[1]];
    	 
    	if (empty($start) || empty($end))
    		return true;
//		if (isDate($data, array('start_date')) == false || isDate($data, array('end_date') == false ))
//			return true;

		($start > $end) ? $isCollect = false : $isCollect = true;
			return $isCollect;
    }
    
    /*
     * 数値チェック（半角数字のみ許可）
    * @param array $data 入力値
    * @param array $arg 未使用
    * @return boolean
    */
    function validInteger($data, $arg) {
    	if (preg_match('/^[0-9]+$/', $data[$arg])/* && !preg_match('/^0/', $data[$arg]) ←0スタートをfalseにしたいならば追加する */) {
    		return true;
    	}
    	return false;
    }

    // ==================== 通常function ====================

    /*
     * 初期状態の取得
     * @return array 初期状態のHiring
     */
    function defaultHiringData() {
        $ret = array(
            'Hiring' => array(
                'id'                 => '',          // なし
                'status'             => 'YUKO',      // 有効
                'date_start'         => '',          // なし
                'date_end'           => '',          // なし
                'title'              => '',          // 未入力
                'modified_user_id'   => '',          // なし
            )
        );
        return $ret;
    }

    /*
     * 対象IDの状態を編集
     * @return array Hiring
     */
    function editHiringData($id) {
        $data = $this->findById($id);
        if ($data === false) {
            return false;
        }
        return $data;
    }

    /*
     * DB登録用の配列を返却する
     * @param array $data 入力値
     * @return string 検索条件
     */
    function arrangeData($data) {
        $storeAry = array('Hiring'=>$data['Hiring'], 'Jobcontent'=>$data['Jobcontent']);

        return $storeAry;
    }

    /*
     * 採用条件のデータ取得
    * @param array $data 入力値
    * @return string 検索条件
    */
    function hiringlistsFind() {
    	$params = array(
    			'order'      => 'Hiring.id'
    	);
    	return $this->find('all', $params);
    }

    /*
     * 詳細表示など用に指定IDから採用条件を取得
     * @param int $id ID
     * @return array 採用条件
     */
    function detailHiring($id) {
        return $this->findById($id);
    }

}
?>