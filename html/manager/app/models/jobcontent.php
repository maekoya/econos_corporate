<?php

class Jobcontent extends AppModel {
    var $useTable  = 'jobcontents';
    var $name      = 'Jobcontent';
    var $hasMany   = array(
    		    );
    var $fieldlist = array(
        'hiring_id',
        'contents_index',
        'title',
        'contents',
        'modified_user_id',
    );

    // ==================== コンストラクタ ====================
    /*
     * コンストラクタ
     * @return void
     */
    function Jobcontent() {
        parent::__construct();
    }

    // ==================== validate用function ====================

    // ==================== 通常function ====================

    
}
?>