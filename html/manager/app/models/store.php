<?php

class Store extends AppModel {
    var $useTable  = 'stores';
    var $name      = 'Store';
    var $hasMany   = array(
    );
    var $fieldlist = array(
        'storetype',
        'status',
        'name',
        'url',
        'postal',
        'address',
        'tel',
        'fax',
        'displayorder',
    	 'modified_user_id',
    );

	var $validate  = array(
		'storetype' => array(
			'notempty' => array(
				'rule'	   => array('notempty'),
				'message'  => '店舗種別が選択されていません。',
				'required' => true,
				'last'	   => true,
			),
		),
		'status' => array(
			'notempty' => array(
				'rule'	   => array('notempty'),
				'message'  => 'ステータスが選択されていません。',
				'required' => true,
				'last'	   => true,
			),
		),
		'name' => array(
				'notempty' => array(
					'rule'	   => array('notempty'),
					'message'  => '店舗名が入力されていません。',
					'required' => true,
					'last'	   => true,
				),
				'maxLength' => array(
						'rule'		 => array('maxLength', 100),
						'message'	 => '店舗名は100文字以内で入力してください。',
						'required'	 => true,
						'allowEmpty' => true,
						'last'		 => true,
				),
			),
		'url' => array(
				'notempty' => array(
					'rule'	   => array('notempty'),
					'message'  => 'URLが入力されていません。',
					'required' => true,
					'last'	   => true,
				),
				'maxLength' => array(
						'rule'		 => array('maxLength', 20000),
						'message'	 => 'URLは20000文字以内で入力してください。',
						'required'	 => true,
						'allowEmpty' => true,
						'last'		 => true,
				),
			),
		'postal' => array(
				'notempty' => array(
					'rule'	   => array('notempty'),
					'message'  => '郵便番号が入力されていません。',
					'required' => true,
					'last'	   => true,
				),
				'maxLength' => array(
						'rule'		 => array('maxLength', 8),
						'message'	 => '郵便番号は8文字以内で入力してください。',
						'required'	 => true,
						'allowEmpty' => true,
						'last'		 => true,
				),
				'custom' => array(
					'rule' => array('custom',POSTAL_REGEXP),
					'message' => '半角数字または-（ハイフン）のみで入力してください',
				),
		),
		'address' => array(
				'notempty' => array(
					'rule'	   => array('notempty'),
					'message'  => '住所が入力されていません。',
					'required' => true,
					'last'	   => true,
				),
				'maxLength' => array(
						'rule'		 => array('maxLength', 256),
						'message'	 => '郵便番号は256文字以内で入力してください。',
						'required'	 => true,
						'allowEmpty' => true,
						'last'		 => true,
				),
			),
		'tel' => array(
				'notempty' => array(
					'rule'	   => array('notempty'),
					'message'  => '電話番号が入力されていません。',
					'required' => true,
					'last'	   => true,
				),
				'maxLength' => array(
						'rule'		 => array('maxLength', 14),
						'message'	 => '電話番号は14文字以内で入力してください。',
						'required'	 => true,
						'allowEmpty' => true,
						'last'		 => true,
				),
				'custom' => array(
					'rule' => array('custom',TEL_REGEXP),
					'message' => '半角数字または-（ハイフン）のみで入力してください',
				),
			),
		'fax' => array(
				'notempty' => array(
					'rule'	   => array('notempty'),
					'message'  => 'FAX番号が入力されていません。',
					'required' => true,
					'last'	   => true,
				),
				'maxLength' => array(
						'rule'		 => array('maxLength', 14),
						'message'	 => 'FAX番号は14文字以内で入力してください。',
						'required'	 => true,
						'allowEmpty' => true,
						'last'		 => true,
				),
				'custom' => array(
					'rule' => array('custom',TEL_REGEXP),
					'message' => '半角数字または-（ハイフン）のみで入力してください',
				),
		),
    );

    // ==================== コンストラクタ ====================
    /*
     * コンストラクタ
     * @return void
     */
    function Store() {
        parent::__construct();
    }

    // ==================== validate用function ====================
    /*
     * 正しい日付かチェック
    * @param array $data 入力値
    * @param array $arg 専用引数
    * @return boolean
    */
    function isDate($data, $arg) {
    	$val = $this->data['Info'][$arg[0]];
    	$y   = substr($val, 0, 4);
    	$m   = substr($val, 4, 2);
    	$d   = substr($val, 6, 2);
    	if (checkdate($m, $d, $y)) {
    		return true;
    	} else {
    		return false;
    	}
    }
    /*
     * リンク区分がarg[1]ならば必須チェック
     * @param array $data 入力値
     * @param array $arg 比較値
     * @return boolean
     */
    function recommendtypeNotEmpty($data, $arg) {
        if ($this->data['Info']['linkkbn'] == $arg[1]) {
            $checkValue = $this->data['Info'][$arg[0]];
            if (strlen($checkValue) > 0) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }
    /*
     * リンク区分がarg[1]ならばファイル名必須チェック
     * @param array $data 入力値
     * @param array $arg 比較値
     * @return boolean
     */
    function fileNotEmpty($data, $arg) {
        if ($this->data['Info']['linkkbn'] == $arg[1]) {
            $checkValue = $this->data['Info']['pdf']['name'];
            if (strlen($checkValue) > 0) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }
    /*
     * リンク区分がPDFならば文字数チェック
    * @param array $data 入力値
    * @param array $arg 未使用
    * @return boolean
    */
    function maxLengthFile($data, $arg) {
    	if ($this->data['Info']['linkkbn'] !== 'PDF')
    		return true;

    	$checkValue = $this->data['Info']['pdf']['name'];

    	return maxLength($checkValue, $arg);
    }
    /*
     * リンク区分がPDFならば文字数チェック
     * @param array $data 入力値
     * @param array $arg 未使用
     * @return boolean
     */
    function isFiletype($data, $arg) {
    	if ($this->data['Info']['linkkbn'] != $arg[1]) 
    		return true;

        $checkValue = $this->data['Info']['pdf']['name'];
        $exp = explode('.', $checkValue);
        $ext = $exp[count($exp)-1];
        $ext = strtolower($ext);
        foreach ($arg[1] as $allowExt) {
            $c = strtolower($allowExt);
            if ($c == $ext) {
                return true;
            }
        }
        return false;
    }
    /*
     * 数値チェック（半角数字のみ許可）
     * @param array $data 入力値
     * @param array $arg 未使用
     * @return boolean
     */
    function validInteger($data, $arg) {
        if (preg_match('/^[0-9]+$/', $data[$arg])/* && !preg_match('/^0/', $data[$arg]) ←0スタートをfalseにしたいならば追加する */) {
            return true;
        }
        return false;
    }

    // ==================== 通常function ====================

    /*
     * 初期状態の取得
     * @return array 初期状態のInfo
     */
    function defaultStoreData($storetype=null) {
    	 
        $ret = array(
            'Store' => array(
            	  'id'                 => '',          // なし
                'storetype'          => $storetype,  // BOOK OFF
            	  'status'             => 'YUKO',      // 有効
                'name'               => '',          // 掲載情報
                'url'                => '',          // なし
                'postal'             => '',          // 未入力
                'address'            => '',          // なし
                'tel'                => '',          // アップなし
                'fax'                => '',          // 未入力
                'displayorder'       => '',          // 未入力
                'modified_user_id'   => '',          // 未入力
            		
            )
        );
        return $ret;
    }

    /*
     * 対象IDの状態を編集
     * @return array Store
     */
    function editStoreData($id) {
        $data = $this->findById($id);
        if ($data === false) {
            return false;
        }
        $ret = array(
            'Store' => array(
                'id'                 => $data['Store']['id'],
                'storetype'          => $data['Store']['storetype'],
            	  'status'             => $data['Store']['status'],
                'name'               => $data['Store']['name'],
                'url'                => $data['Store']['url'],
                'postal'             => $data['Store']['postal'],
                'address'            => $data['Store']['address'],
                'tel'                => $data['Store']['tel'],
                'fax'                => $data['Store']['fax'],
                'displayorder'       => $data['Store']['displayorder'],
            )
        );

       return $ret;
    }


    /*
     * DB登録用の配列を返却する
     * @param array $data 入力値
     * @return string 検索条件
     */
    function arrangeData($data) {
        $storeAry = array('Store'=>$data['Store']);

        // 新規作成の場合、表示順の番号を与える
       if (empty($storeAry['Store']['id'])) {
           $storetype = $data['Store']['storetype'];
           $ret = $this->getMaxDisplayOrder($storetype) + 1;
           $storeAry['Store']['displayorder'] = $ret;
         }

        return $storeAry;
    }

    /*
     * 詳細表示など用に指定IDからニュース・インフォメーション情報を取得
     * @param int $id ID
     * @return array ニュース・インフォメーション情報
     */
    function detailStore($id) {
        return $this->findById($id);
    }

    /*
     * 検索条件の生成
    * @param array $data 入力値
    * @return string 検索条件
    */
    function createConditions($data) {
    	$cond = array();
    	if (isset($data['Store']['storetype']) && strlen($data['Store']['storetype']) > 0) {
    		$cond['Store.storetype'] = $data['Store']['storetype'];
    	}
    
    	return $cond;
    }
    /*
     * 運営店舗並び替え用のデータ取得
    * @param array $data 入力値
    * @return string 検索条件
    */
    function storesortsFind($cond) {
    	$params = array(
    			'conditions' => $cond,
    			'order'      => 'Store.displayorder'
    	);
    	return $this->find('all', $params);
    }
    
    /*
     * 運営店舗並び替え
    * @param string $movetype 行動タイプ
    * @param int $id データID
    * @return bool
    */
    function orderUpdate($movetype, $id, $modified_user_id) {
    	switch($movetype) {
    		case '1u':
    			// 一つ上
    			return $this->orderUpdateToOneUp($id, $modified_user_id);
    			break;
    		case '1d':
    			// 一つ下
    			return $this->orderUpdateToOneDown($id, $modified_user_id);
    			break;
    	}
    }
    
    /*
     * 運営店舗並び替え(一つ上)
    * @param int $id データID
    * @return bool
    */
    function orderUpdateToOneUp($id, $modified_user_id) {
    	// 引数のIDのデータを取得
    	$target = $this->findById($id);
    	if ($target === false) {
    		// 対象が見つからない
    		return false;
    	}

    	// 入れ替える対象のデータを取得
    	$cond = array(
				'Store.storetype'    => $target['Store']['storetype'],
    			'Store.displayorder' => $target['Store']['displayorder'] - 1,
    	);
    	$swap = $this->find('first', array('conditions'=>$cond));
    	if ($swap === false) {
    		// 対象が見つからない（引数データが一番上）
    		return true;
    	}
    
    	$modified = date('Y-m-d H:i:s');
    	// 引数データのorderをデクリメント
    	$target['Store']['displayorder']     = $target['Store']['displayorder'] - 1;
    	$target['Store']['modified_user_id'] = $modified_user_id;
    	$target['Store']['modified']         = null;
    	// 入れ替え対象データのorderをインクリメント
    	$swap['Store']['displayorder']     = $swap['Store']['displayorder'] + 1;
    	$swap['Store']['modified_user_id'] = $modified_user_id;
    	$swap['Store']['modified']         = null;
    
    	$this->create();
    	$ret1 = $this->save($target,false);
    	$this->create();
    	$ret2 = $this->save($swap,false);
    
    	return ($ret1 && $ret2);
    }
    
    /*
     * 運営店舗並び替え(一つ下)
    * @param string $movetype 行動タイプ
    * @param int $id データID
    * @return bool
    */
    function orderUpdateToOneDown($id, $modified_user_id) {
    	// 引数のIDのデータを取得
    	$target = $this->findById($id);
    	if ($target === false) {
    		// 対象が見つからない
    		return false;
    	}
    
    	// 入れ替える対象のデータを取得
    	$cond = array(
				'Store.storetype'    => $target['Store']['storetype'],
    			'Store.displayorder' => $target['Store']['displayorder'] + 1,
    	);
    	$swap = $this->find('first', array('conditions'=>$cond));
    	if ($swap === false) {
    		// 対象が見つからない（引数データが一番上）
    		return true;
    	}
    
    	$modified = date('Y-m-d H:i:s');
    	// 引数データのorderをデクリメント
    	$target['Store']['displayorder']     = $target['Store']['displayorder'] + 1;
    	$target['Store']['modified_user_id'] = $modified_user_id;
    	$target['Store']['modified']         = null;
    	// 入れ替え対象データのorderをインクリメント
    	$swap['Store']['displayorder']     = $swap['Store']['displayorder'] - 1;
    	$swap['Store']['modified_user_id'] = $modified_user_id;
    	$swap['Store']['modified']         = null;
    
    	$this->create();
    	$ret1 = $this->save($target,false);
    	$this->create();
    	$ret2 = $this->save($swap,false);
    
    	return ($ret1 && $ret2);
    }
    
    /*
     * 指定されたキーの最大番号を取得する
    * @param int $storetype   キー
    * @return int 最大番号
    */
    function getMaxDisplayOrder($storetype) {
    	if (empty($storetype)) {
    		return false;
    	}

    	$displayorder = $this->getMaxDisplayOrderByKeys($storetype);

    	if ($displayorder === false) {
    		return 0;
    	} else {
    		return $displayorder['max_id'];
    		/*
    		$max = $displayorder['max_id'];
    		if ($max==0) {
    			$max = $this->Store->getLastInsertID;
    		} else {
    			return $max;
    		}
    		*/
    	}
    }

    /*
      * 指定されたキーのデータを取得する（FOR UPDATE）
     * @param int $storetype   キー
     * @return int 最大番号
     */
    function getMaxDisplayOrderByKeys($storetype) {
        	if (empty($storetype)) {
    		return false;
    	}
    	 $displayorder = $this->find('first', array(
    			"fields" => "MAX(Store.displayorder) as max_id", 
    			"conditions" => array('storetype' => $storetype))
    	 );

        if (count($displayorder) == 0) {
            return false;
        } else {
            $displayorder = $displayorder[0];
            return $displayorder;
        }
    }

    /*
     * エコポイント削除後の表示順並び替え
    * @param int $id データID
    * @return bool
    */
    function resetDisplayOrder($id) {
    	$target = $this->findById($id);
    	$storetype = $target['Store']['storetype'];
    	$displayorder = $target['Store']['displayorder'];
/*
    	$params = array(
    			'Store.storetype' => $storetype
    	);
    	$lists = $this->find('all', $params);
*/
    	$lists = $this->find('all');

    	foreach ($lists as $key=>$val) {
    		$target = $this->findById($val['Store']['id']);
    		
    		$target_storetype = $target['Store']['storetype'];
    		if ($target_storetype != $storetype)
    			continue;

    		$target_displayorder = $target['Store']['displayorder'];
    		if ($target_displayorder <= $displayorder)
    			continue;
    
    		$target['Store']['displayorder'] = $target['Store']['displayorder'] - 1;
    
    		$this->create();
    		$ret = $this->save($target,false);
    
    		if ($ret == false) {
    			return false;
    		}
    	}

    	return true;
    }

}
?>