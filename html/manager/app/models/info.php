<?php

class Info extends AppModel {
    var $useTable  = 'infos';
    var $name      = 'Info';
    var $hasMany   = array(
    );
    var $fieldlist = array(
        'status',
        'infokbn',
        'ymd',
        'freeword',
        'linkkbn',
        'pdf',
        'url',
        'modified_user_id',
    );
	var $validate  = array(
		'status' => array(
			'notempty' => array(
				'rule'	   => array('notempty'),
				'message'  => 'ステータスが選択されていません。',
				'required' => true,
				'last'	   => true,
			),
		),
		'ymd' => array(
				'minLength' => array(
					'rule'		 => array('minLength', 8),
					'message'	 => '日付は8文字の半角数字で入力してください。',
					'required'	 => true,
					'allowEmpty' => false,
					'last'		 => true,
				),
				'maxLength' => array(
					'rule'		 => array('maxLength', 8),
					'message'	 => '日付は8文字の半角数字で入力してください。',
					'required'	 => true,
					'allowEmpty' => true,
					'last'		 => true,
				),
				'validInteger' => array(
					'rule'		 => array('validInteger', 'ymd'),
					'message'	 => '日付は8文字の半角数字で入力してください。',
					'required'	 => true,
					'allowEmpty' => true,
					'last'		 => true,
				),
				'isDate' => array(
					'rule'		 => array('isDate', array('ymd')),
					'message'	 => '日付が不正です。正しい日付を入力してください。',
					'required'	 => true,
					'allowEmpty' => true,
					'last'		 => true,
				),
				'isDateBetween' => array(
					'rule'		 => array('isDateBetween', array('ymd')),
					'message'	 => '19700101～20380119までで入力してください。',
					'required'	 => true,
					'allowEmpty' => true,
					'last'		 => true,
				),
		),
		'freeword' => array(
				'maxLength' => array(
						'rule'		 => array('maxLength', 20000),
						'message'	 => '内容は20000文字以内で入力してください。',
						'required'	 => true,
						'allowEmpty' => false,
						'last'		 => true,
				),
			),
		'pdf' => array(
				'fileNotEmpty' => array(
						'rule'	  => array('fileNotEmpty', array('pdf', 'PDF')),
						'message' => 'ファイル名が入力されていません。',
						'last'	  => true,
				),
				'maxLength' => array(
						'rule'		 => array('maxLengthFile', 255),
						'message'	 => 'ファイル名は255文字以内で入力してください。',
						'allowEmpty' => true,
						'last'		 => true,
				),
				'isFiletype' => array(
						'rule'		 => array('isFiletype', array('linkkbn', array('pdf'))),
						'message'	 => 'ファイル名はpdfを指定してください。',
						'allowEmpty' => true,
						'last'		 => true,
				),
		),
		'url' => array(
				'recommendtypeNotEmpty' => array(
						'rule'	  => array('recommendtypeNotEmpty', array('url', 'URL')),
						'message' => 'URLが入力されていません。',
						'last'	  => true,
				),
				'maxLength' => array(
						'rule'		 => array('maxLength', 20000),
						'message'	 => 'URLは20000文字以内で入力してください。',
						'allowEmpty' => true,
						'last'		 => true,
				),
		),
    );

    // ==================== コンストラクタ ====================
    /*
     * コンストラクタ
     * @return void
     */
    function Info() {
        parent::__construct();
    }

    // ==================== validate用function ====================
    /*
     * 正しい日付かチェック
    * @param array $data 入力値
    * @param array $arg 専用引数
    * @return boolean
    */
    function isDate($data, $arg) {
    	$val = $this->data['Info'][$arg[0]];
    	$y   = substr($val, 0, 4);
    	$m   = substr($val, 4, 2);
    	$d   = substr($val, 6, 2);
    	if (checkdate($m, $d, $y)) {
    		return true;
    	} else {
    		return false;
    	}
    }
    /*
     * 19700101～20380119までかチェック
    * @param array $data 入力値
    * @param array $arg 専用引数
    * @return boolean
    */
    function isDateBetween($data, $arg) {
    	$val = $this->data['Info'][$arg[0]];
    	$y   = substr($val, 0, 4);
    	$m   = substr($val, 4, 2);
    	$d   = substr($val, 6, 2);
    	if ($y < 1970 || $y > 2038)
    		return false;
    	if ($y == 2038) {
    		if ($m > 1)
    			return false;
    		if ($d > 19)
    			return false;
    	}
    	return true;
    }
    /*
     * リンク区分がarg[1]ならば必須チェック
     * @param array $data 入力値
     * @param array $arg 比較値
     * @return boolean
     */
    function recommendtypeNotEmpty($data, $arg) {
        if ($this->data['Info']['linkkbn'] == $arg[1]) {
            $checkValue = $this->data['Info'][$arg[0]];
            if (strlen($checkValue) > 0) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }
    /*
     * リンク区分がarg[1]ならばファイル名必須チェック
     * @param array $data 入力値
     * @param array $arg 比較値
     * @return boolean
     */
    function fileNotEmpty($data, $arg) {

        if ($this->data['Info']['linkkbn'] == $arg[1]) {

        	// ファイル登録済であればチェックOK
        	if (!empty($this->data['Info']['pdf_tmp']))
        		return true;

            $checkValue = $this->data['Info']['pdf']['name'];
            if (strlen($checkValue) > 0) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }
    /*
     * リンク区分がPDFならば文字数チェック
    * @param array $data 入力値
    * @param array $arg 未使用
    * @return boolean
    */
    function maxLengthFile($data, $arg) {
    	$chk =& Validation::getInstance();

    	if ($this->data['Info']['linkkbn'] !== 'PDF')
    		return true;

    	$checkValue = $this->data['Info']['pdf']['name'];

    	return $chk->maxLength($checkValue, $arg);
    }
    /*
     * リンク区分がPDFならば文字数チェック
     * @param array $data 入力値
     * @param array $arg 未使用
     * @return boolean
     */
    function isFiletype($data, $arg) {
    	if ($this->data['Info']['linkkbn'] != 'PDF') 
    		return true;

        $checkValue = $this->data['Info']['pdf']['name'];

        // ファイル登録済でファイルが指定していなければチェックOK
        if (!empty($this->data['Info']['pdf_tmp']) && empty($checkValue))
        	return true;

        $exp = explode('.', $checkValue);
        $ext = $exp[count($exp)-1];
        $ext = strtolower($ext);
        foreach ($arg[1] as $allowExt) {
            $c = strtolower($allowExt);
            if ($c == $ext) {
                return true;
            }
        }
        return false;
    }
    /*
     * 数値チェック（半角数字のみ許可）
     * @param array $data 入力値
     * @param array $arg 未使用
     * @return boolean
     */
    function validInteger($data, $arg) {
        if (preg_match('/^[0-9]+$/', $data[$arg])/* && !preg_match('/^0/', $data[$arg]) ←0スタートをfalseにしたいならば追加する */) {
            return true;
        }
        return false;
    }

    // ==================== 通常function ====================

    /*
     * 初期状態の取得
     * @return array 初期状態のInfo
     */
    function defaultInfoData() {
        $ret = array(
            'Info' => array(
                'id'                 => '',          // なし
                'status'             => 'YUKO',      // 有効
                'infokbn'            => 'KEISAI',    // 掲載情報
                'ymd'                => null,        // なし
                'freeword'           => '',          // 未入力
                'linkkbn'            => 'PDF',       // PDF
                'pdf'                => '2',         // アップなし
                'url'                => '',          // 未入力
                'pdf_tmp'            => '',          // なし
            )
        );
        return $ret;
    }

    /*
     * 対象IDの状態を編集
     * @return array Info
     */
    function editInfoData($id) {
        $data = $this->findById($id);
        if ($data === false) {
            return false;
        }
        $ret = array(
            'Info' => array(
                'id'                 => $data['Info']['id'],
                'status'             => $data['Info']['status'],
                'infokbn'            => $data['Info']['infokbn'],
                'ymd'                => $data['Info']['ymd'],
                'freeword'           => $data['Info']['freeword'],
                'linkkbn'            => $data['Info']['linkkbn'],
                'pdf'                => $data['Info']['pdf'],
                'url'                => $data['Info']['url'],
                'pdf_tmp'            => '',
            )
        );
       // PDFがあるか
       if ($ret['Info']['pdf'] == 1) {
        	$pdfDir = Configure::read('Pdf.pdf_dir') . DS
        					. Configure::read('Pdf.info_dir') . DS;
        	$pdfFile = sprintf('%011d', $data['Info']['id']) . '.pdf';
        	$pdfPath = $pdfDir . $pdfFile;
        	if (file_exists($pdfPath)) {
        		$ret['Info']['pdf_tmp'] = $pdfFile;
        	}
        }
        return $ret;
    }

    /*
     * PDFのチェック
    * @param array $data 入力値
    * @return string 検索条件
    */
    function uploadPdfValidate($data, $key) {
        $limitsize = Configure::read('Pdf.upload_limitsize');
        $ret = array();
        if (!isset($data['Info'][$key]) || !isset($data['Info'][$key]['error'])) {
            $ret[$key] = '不正なアクセスです。';
            return $ret;
        }
        if ($data['Info'][$key]['error'] == 4) {
            return $ret;
        }
        if ($data['Info'][$key]['error'] != 0) {
     		switch ($data['Info'][$key]['error']) {
    			case 1:
    				$ret[$key] = 'アップロードされたファイルのサイズが、設定ファイルの値を超えています。';
    				return $ret;
    				break;
    			default:
    				$ret[$key] = 'アップロードに失敗しました。';
    				return $ret;
    				break;
    		}
         }
        if ($data['Info'][$key]['size'] > $limitsize) {
            $p  = array('', 'k', 'M', 'T');
            $sp = '';
            $s  = $limitsize;
            $n  = 0;
            while ($s >= 1024) {
                $s = (int)($s / 1024);
                $n++;
                if ($n >= 3) {
                    break;
                }
            }
            $size      = $s . $p[$n] . 'バイト';
            $ret[$key] = $size . '以下のファイルをアップロードしてください。';
            return $ret;
        }

        return $ret;

    }

    /*
     * DB登録用の配列を返却する
     * @param array $data 入力値
     * @return string 検索条件
     */
    function arrangeData($data) {
        $storeAry = array('Info'=>$data['Info']);

        // PDFのアップロードフラグをセットする
        if (isset($data['Info']['pdf']['error']) && $data['Info']['pdf']['error'] == 0) {
            $storeAry['Info']['pdf'] = 1;
        } elseif (isset($data['Info']['pdf']['error']) && $data['Info']['pdf']['error'] == 4 && 
        		          strlen($data['Info']['pdf_tmp']) > 0) {
            $storeAry['Info']['pdf'] = 1;
        } else {
            $storeAry['Info']['pdf'] = 2;
        }

        return $storeAry;
    }

    /*
     * PDFパスを返却する
    * @param array $data 入力値
    * @return string 検索条件
    */
    function pdfPath($data) {
    	if (empty($data['Info']['id'])) {
    		return false;
    	}
    	$pdfDir    = Configure::read('Pdf.pdf_dir') . DS . Configure::read('Pdf.info_dir');
    	$filename  = sprintf('%011d', $data['Info']['id']) . '.pdf';
    
    	// PDFディレクトリが存在していない場合、作成する
    	if (!file_exists($pdfDir. DS)) {
    		mkdir($pdfDir. DS);
    		chmod($pdfDir. DS, 0777);
    	}
    
    	// PDFパスを返却
    	return array(
    			'filepath'  => $pdfDir . DS . $filename,
    	);
    }

    /*
     * 詳細表示など用に指定IDからニュース・インフォメーション情報を取得
     * @param int $id ID
     * @return array ニュース・インフォメーション情報
     */
    function detailInfo($id) {
        return $this->findById($id);
    }

}
?>