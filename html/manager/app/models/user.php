<?php
App::import('Component', 'Auth');

class User extends AppModel {
    var $useTable  = 'users';
    var $name      = 'User';
    var $hasMany   = array(
    );
    var $fieldlist = array(
        'userid',
        'passwd',
        'status',
        'adminkbn',
        'hotelcd',
        'username',
        'memo',
    );
    var $validate  = array(
    );

    // ==================== validate用function ====================

    // ==================== 通常function ====================
    /*
     * AuthComponentでパスワードを作る
     * @param string $string 入力値
     * @return string パスワード
     */
    function makePassword($string) {
        $AC = new AuthComponent();
        return $AC->password($string);
    }
}
?>