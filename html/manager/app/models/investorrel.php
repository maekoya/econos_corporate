<?php

class Investorrel extends AppModel {
    var $useTable  = 'investorrels';
    var $name      = 'Investorrel';
    var $hasMany   = array(
    );
    var $fieldlist = array(
        'status',
        'category',
        'title',
        'linkkbn',
        'pdf',
        'url',
        'modified_user_id',
    );
	var $validate  = array(
		'status' => array(
			'notempty' => array(
				'rule'	   => array('notempty'),
				'message'  => 'ステータスが選択されていません。',
				'required' => true,
				'last'	   => true,
			),
		),
		'category' => array(
			'notempty' => array(
				'rule'	   => array('notempty'),
				'message'  => 'カテゴリが選択されていません。',
				'required' => true,
				'last'	   => true,
			),
		),
		'title' => array(
				'notempty' => array(
						'rule'	   => array('notempty'),
						'message'  => 'タイトルが入力されていません。',
						'required' => true,
						'last'	   => true,
				),
				'maxLength' => array(
					'rule'		 => array('maxLength', 100),
					'message'	 => 'タイトルは100文字以内で入力してください。',
					'required'	 => true,
					'allowEmpty' => true,
					'last'		 => true,
				),
		),
		'pdf' => array(
				'fileNotEmpty' => array(
						'rule'	  => array('fileNotEmpty', array('pdf', 'PDF')),
						'message' => 'ファイル名が入力されていません。',
						'last'	  => true,
				),
				'maxLength' => array(
						'rule'		 => array('maxLengthFile', 255),
						'message'	 => 'ファイル名は255文字以内で入力してください。',
						'allowEmpty' => true,
						'last'		 => true,
				),
				'isFiletype' => array(
						'rule'		 => array('isFiletype', array('linkkbn', array('pdf'))),
						'message'	 => 'ファイル名はpdfを指定してください。',
						'allowEmpty' => true,
						'last'		 => true,
				),
		),
		'url' => array(
				'recommendtypeNotEmpty' => array(
						'rule'	  => array('recommendtypeNotEmpty', array('url', 'URL')),
						'message' => 'URLが入力されていません。',
						'last'	  => true,
				),
				'maxLength' => array(
						'rule'		 => array('maxLength', 20000),
						'message'	 => 'URLは20000文字以内で入力してください。',
						'allowEmpty' => true,
						'last'		 => true,
				),
		),
    );

    // ==================== コンストラクタ ====================
    /*
     * コンストラクタ
     * @return void
     */
    function Investorrel() {
        parent::__construct();
    }

    // ==================== validate用function ====================
    /*
     * リンク区分がarg[1]ならば必須チェック
     * @param array $data 入力値
     * @param array $arg 比較値
     * @return boolean
     */
    function recommendtypeNotEmpty($data, $arg) {
        if ($this->data['Investorrel']['linkkbn'] == $arg[1]) {
            $checkValue = $this->data['Investorrel'][$arg[0]];
            if (strlen($checkValue) > 0) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }
    /*
     * リンク区分がarg[1]ならばファイル名必須チェック
     * @param array $data 入力値
     * @param array $arg 比較値
     * @return boolean
     */
    function fileNotEmpty($data, $arg) {
        if ($this->data['Investorrel']['linkkbn'] == $arg[1]) {
            $checkValue = $this->data['Investorrel']['pdf']['name'];

            
            // ファイル登録済であればチェックOK
            if (!empty($this->data['Investorrel']['pdf_tmp']))
            	return true;

            if (strlen($checkValue) > 0) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }
    /*
     * リンク区分がPDFならば文字数チェック
    * @param array $data 入力値
    * @param array $arg 未使用
    * @return boolean
    */
    function maxLengthFile($data, $arg) {
    	$chk =& Validation::getInstance();
    	
    	if ($this->data['Investorrel']['linkkbn'] !== 'PDF')
    		return true;

    	$checkValue = $this->data['Investorrel']['pdf']['name'];

    	return $chk->maxLength($checkValue, $arg);
    }
    /*
     * リンク区分がPDFならば文字数チェック
     * @param array $data 入力値
     * @param array $arg 未使用
     * @return boolean
     */
    function isFiletype($data, $arg) {
    	if ($this->data['Investorrel']['linkkbn'] != 'PDF') 
    		return true;

        $checkValue = $this->data['Investorrel']['pdf']['name'];

         // ファイル登録済でファイルが指定していなければチェックOK
        if (!empty($this->data['Investorrel']['pdf_tmp']) && empty($checkValue))
        	return true;

        $exp = explode('.', $checkValue);
        $ext = $exp[count($exp)-1];
        $ext = strtolower($ext);
        foreach ($arg[1] as $allowExt) {
            $c = strtolower($allowExt);
            if ($c == $ext) {
                return true;
            }
        }
        return false;
    }

    // ==================== 通常function ====================

    /*
     * 初期状態の取得
     * @return array 初期状態のInvestorrel
     */
    function defaultInvestorrelData() {
        $ret = array(
            'Investorrel' => array(
                'id'                 => '',          // なし
                'status'             => 'YUKO',      // 有効
                'category'           => '01KEIEI',   // 業務・財務情報
                'title'              => '',          // なし
                'linkkbn'            => 'PDF',       // PDF
                'pdf'                => '2',         // アップなし
                'url'                => '',          // なし
                'pdf_tmp'            => '',          // なし
            )
        );
        return $ret;
    }

    /*
     * 対象IDの状態を編集
     * @return array Investorrel
     */
    function editInvestorrelData($id) {
        $data = $this->findById($id);
        if ($data === false) {
            return false;
        }
        $ret = array(
            'Investorrel' => array(
                'id'                 => $data['Investorrel']['id'],
                'status'             => $data['Investorrel']['status'],
                'category'           => $data['Investorrel']['category'],
                'title'              => $data['Investorrel']['title'],
                'linkkbn'            => $data['Investorrel']['linkkbn'],
                'pdf'                => $data['Investorrel']['pdf'],
                'url'                => $data['Investorrel']['url'],
                'pdf_tmp'            => '',
            )
        );
       // PDFがあるか
       if ($ret['Investorrel']['pdf'] == 1) {
        	$pdfDir = Configure::read('Pdf.pdf_dir') . DS
        					. Configure::read('Pdf.investorrel_dir') . DS;
        	$pdfFile = sprintf('%011d', $data['Investorrel']['id']) . '.pdf';
        	$pdfPath = $pdfDir . $pdfFile;
        	if (file_exists($pdfPath)) {
        		$ret['Investorrel']['pdf_tmp'] = $pdfFile;
        	}
        }
        return $ret;
    }

    /*
     * PDFのチェック
    * @param array $data 入力値
    * @return string 検索条件
    */
    function uploadPdfValidate($data, $key) {
        $limitsize = Configure::read('Pdf.upload_limitsize');
        $ret = array();
        if (!isset($data['Investorrel'][$key]) || !isset($data['Investorrel'][$key]['error'])) {
            $ret[$key] = '不正なアクセスです。';
            return $ret;
        }
        if ($data['Investorrel'][$key]['error'] == 4) {
            return $ret;
        }
        if ($data['Investorrel'][$key]['error'] != 0) {
         	switch ($data['Investorrel'][$key]['error']) {
    			case 1:
    				$ret[$key] = 'アップロードされたファイルのサイズが、設定ファイルの値を超えています。';
    				return $ret;
    				break;
    			default:
    				$ret[$key] = 'アップロードに失敗しました。';
    				return $ret;
    				break;
    		}
         }
        if ($data['Investorrel'][$key]['size'] > $limitsize) {
            $p  = array('', 'k', 'M', 'T');
            $sp = '';
            $s  = $limitsize;
            $n  = 0;
            while ($s >= 1024) {
                $s = (int)($s / 1024);
                $n++;
                if ($n >= 3) {
                    break;
                }
            }
            $size      = $s . $p[$n] . 'バイト';
            $ret[$key] = $size . '以下のファイルをアップロードしてください。';
            return $ret;
        }
        return $ret;
    }

    /*
     * DB登録用の配列を返却する
     * @param array $data 入力値
     * @return string 検索条件
     */
    function arrangeData($data) {
        $storeAry = array('Investorrel'=>$data['Investorrel']);

        // PDFのアップロードフラグをセットする
        if (isset($data['Investorrel']['pdf']['error']) && $data['Investorrel']['pdf']['error'] == 0) {
            $storeAry['Investorrel']['pdf'] = 1;
        } elseif (isset($data['Investorrel']['pdf']['error']) && $data['Investorrel']['pdf']['error'] == 4 && 
        		          strlen($data['Investorrel']['pdf_tmp']) > 0) {
            $storeAry['Investorrel']['pdf'] = 1;
        } else {
            $storeAry['Investorrel']['pdf'] = 2;
        }

        return $storeAry;
    }

    /*
     * PDFパスを返却する
    * @param array $data 入力値
    * @return string 検索条件
    */
    function pdfPath($data) {
    	if (empty($data['Investorrel']['id'])) {
    		return false;
    	}
    	$pdfDir    = Configure::read('Pdf.pdf_dir') . DS . Configure::read('Pdf.investorrel_dir');
    	$filename  = sprintf('%011d', $data['Investorrel']['id']) . '.pdf';

    	// PDFディレクトリが存在していない場合、作成する
    	if (!file_exists($pdfDir. DS)) {
    		mkdir($pdfDir. DS);
    		chmod($pdfDir. DS, 0777);
    	}

    	// PDFパスを返却
    	return array(
    			'filepath'  => $pdfDir . DS . $filename,
    	);
    }

    /*
     * 詳細表示など用に指定IDからニュース・インフォメーション情報を取得
     * @param int $id ID
     * @return array ニュース・インフォメーション情報
     */
    function detailInvestorrel($id) {
        return $this->findById($id);
    }

}
?>