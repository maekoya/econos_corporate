<?php

// PDF系
define('DS', "/");
define('Pdf_pdf_url', '/econos_files/pdf');
define('Pdf_pdf_dir', dirname(dirname(dirname(dirname(__FILE__)))).DS.'econos_files'.DS.'pdf');

// PDF系 IR管理のPDFファイルフォルダ
define('Pdf_investorrel_dir', 'investorrel');
// PDF系 ニュース・インフォメーション管理のPDFファイルフォルダ
define('Pdf_info_dir', 'info');

// 画像系
define('Image_img_url', '/econos_files/img');
define('Image_img_dir', dirname(dirname(dirname(dirname(__FILE__)))).DS.'econos_files'.DS.'img');

// 画像系 エコポイントの画像ファイルフォルダ
define('Image_ecopoint_dir', 'ecopoint');

//ホームに表示するニュース・インフォメーションの行数
define('DEFAULT_SHOW_ROWS', 3);