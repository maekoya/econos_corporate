<?php

//Query実行Logに保存する[日時 SCRIPT_FILENAME SQL]
function Exec_Sql($sql,$pg_name=null) {

	//現在日時
//	$line = "[" . date("Y-m-d H:i:s") . "]";
	//プログラム名
//	$line .= " (" . $pg_name . ")";
	//SQL
//	$line .= " " . $sql;
	//改行
//	$line .= "\n";

	//LOGファイル名を指定
//	$file_name = "/www/manager/log/query_" . date("Ymd") . ".log";
	//$file_name = "./query_log/query.log";
	//LOGファイルの書き込み
//	$fp = fopen("$file_name", "a");
//	fwrite($fp, $line);
//	fclose($fp);
	//SQLを実行
	$result = mysql_query($sql);
	//エラーハンドリング
	if (!$result) {
		echo "SQL ERROR<br />\n";
		//echo $pg_name . "<br />\n";
		echo $sql;

//		$fp = fopen("$file_name", "a");
//		$err_line = "[ERROR] " . $line;
//		fwrite($fp, $err_line);
//		fclose($fp);

		exit;
	}else{
		return $result;
	}

}

/**
    Exec_Sql(クエリのみ。更新系はサポートしない)の検索結果を 配列に詰めて返す。
*/
function Exec_Sql_ToA($sql, $pg_name=null){
    $result = Exec_Sql($sql,$pg_name);
    $ret = array();
    while ( $row = mysql_fetch_assoc($result)){
        $ret[]=$row;
    }
    return $ret;
}

/**
    Exec_Sql(クエリのみ。更新系はサポートしない)の検索結果の1行分を返す。
    配列の配列ではなく、1行分の連想配列を剥き出しにして返す。
    0件ならば空配列を返す。
*/
function Exec_Sql_One($sql, $pg_name=null){
    $result = Exec_Sql($sql,$pg_name);
    if ($row = mysql_fetch_assoc($result)) return $row;
    else return array();
}

function isExistInfos($sql_infos_by_year, $target_year){

	$sql_infos = sprintf($sql_infos_by_year, $target_year, $target_year+1);
	$row_infos = Exec_Sql_ToA($sql_infos);

	return (count($row_infos)>0) ? true : false ;
}

/**
    日付フォーマット  ex.) 2013.01.01
*/
function dateformat($date){
	return date('Y.m.d', strtotime($date));
}

/**
    日付フォーマット  ex.) 2013.01.01
*/
function getLink($val, $dir){
	$link = '';
	if (($val['linkkbn'] == 'PDF') && ($val['pdf'] == 1)) {
		$link = Pdf_pdf_url . DS. $dir . DS. sprintf('%011d', $val['id']) . '.pdf';
	} else if ($val['linkkbn'] == 'URL') {
		$link = $val['url'];
	}
	return $link;
}

/**
    画像イメージの保存先
*/
function getImageLink($val){
	$link = '';
	if ($val['image'] == 1) {
		$link = Image_img_url . DS. Image_ecopoint_dir . DS. sprintf('%011d', $val['id']) . '.jpg';
	}
	return $link;
}

/**
    PDF/URLアイコンの表示
*/
function getLinkImage($val){
	$image = '';
	if ($val['linkkbn'] == 'PDF' && ($val['pdf'] == 1)) {
		$image = 'images/common/icon_pdf.gif';
	} else if ($val['linkkbn'] == 'URL') {
		$image = 'images/common/icon_blank.gif';
	}
	return $image;
}

/**
    ニュース・インフォメーションのPDFリンク先
 */
function getInfoLink($val){
	return getLink($val, Pdf_info_dir);
}

/**
    IRのPDFリンク先
 */
function getIRLink($val){
	return getLink($val, Pdf_investorrel_dir);
}

/**
    ニュース・インフォメーションのPDF/リンクアイコン
 */
function getIconByInfokbn($val){

	$image_dir = "images".DS."common".DS;

	switch ($val['infokbn']) {
	case 'KEISAI':
		$icon = $image_dir."cat_keisai.gif";
		break;
	case 'KAISHA':
		$icon = $image_dir."cat_kaisya.gif";
		break;
	case 'IR':
		$icon = $image_dir."cat_ir.gif";
		break;
	default:
		$icon = '';
		break;
	}
	return $icon;
}

/**
    PDF/URLアイコンの幅
 */
function getImageWidth($val){
	echo $val['linkkbn'] == 'PDF' ? 11 : 12;
}

/**
    PDF/URLアイコンの高さ
 */
function getImageHeight($val){
	echo $val['linkkbn'] == 'PDF' ? 12 : 10;
}

/**
 * HTMLの特殊文字をエスケープして改行の前にbrタグを追加し、結果を出力します。
 */
function hbr($str)
{
	echo nl2br(htmlspecialchars($str, ENT_QUOTES, 'UTF-8'));
}

?>
